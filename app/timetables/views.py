from django.http import HttpResponse
from django.views.generic import View
from django.core.urlresolvers import resolve
from timetables.utils import NavigationTree
from timetables.utils.requirejs import JS_MAIN_ATTR_NAME
from timetables.model.models import User

def todo(request, *args, **kwargs):
    '''
    This View indicates that the view and template have not been implemented. Please implement.
    @param request:
    '''
    return HttpResponse("""
<h1>View Not Implemented</h1>
<p>You need to update the urls.py file which matches the path: <code>{0}</code>.
You'll need to change the view from 'todo' to an appropriate one.</p>
<h2>The request was</h2>
<pre>
{1}
</pre>""".format(request.path, request), content_type="text/html")

class BaseView(View):
    def __init__(self, navtree):
        self.navtree = navtree

    @staticmethod
    def url_name(request):
        match = resolve(request.path)
        return match.url_name

    def js_main_module(self):
        return None

    def context(self, request):
        context = {}
        context["bootstraps"] = {}
        context["url_name"] = self.url_name(request)
        context["breadcrumbs"], context["navigation"] = (
                NavigationTree.build_navigations(request, self.navtree))
        context['user'] = User.as_context(request.user)

        module = self.js_main_module()
        if module:
            context[JS_MAIN_ATTR_NAME] = module

        return context
