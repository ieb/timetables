'''
Created on May 10, 2012

@author: ieb
'''
from timetables.utils.staticdata import create_yaml_loader_relative_to_file
from timetables.model.models import Revision, Organisation, EventGroup, \
    Term, OrganisationRole, Permission, \
    TermInfo, EventType

load_yaml = create_yaml_loader_relative_to_file(__file__)


class SuperAdmin(object):
    '''
    SuperAdmin view object to use 
    replace testdata.X() with SuperAdmin(request).X() and that will re-wire the view to the DB.
    Also, since the mentods produce a Map they also prosuce json feeds with a 
    HttpResponse(json.dumps(SuperAdmin(request).X()),indent=4,content_type="application/json; charset=utf-8")
    '''

    def __init__(self, request):
        self.request = request

    def _getUser(self, data):
        if self.request.user.is_anonymous():
            return { 'is_logged_in' : 'No'} # this should be false, not No
        return {
                'is_logged_in' : 'Yes',
                'name': '%s %s' % (self.request.user.first_name, self.request.user.last_name),
                'username' : self.request.user.username
            }

    def _getRecentChanges(self, data):
        return [ {
              'revision' : rev["version"],
              'data_updated' : rev["created"],
              'author' : rev["user__username"],
              'comment' : rev["comment"]
                  }
                 for rev in Revision.objects.all().order_by('created').values("version", "created", "user__username", "comment")]
        # TODO, needs a filter of some form

    def _getFaculties(self, data):
        return [ o['name'] for o in Organisation.objects.all().values('name')[:50]] # TODO, needs a filter of some form

    def _getTimeTables(self, data):
        '''
timetables:
  - faculty: History :Tripos.departmenttripos.department.name
    level: Part IA   :Tripos.level.name
    courses:   :Tripos.eventgroup_set
      - name: Animal Biology :Tripos.eventgroup_set.name
        status: published  :Tripos.eventgroup_set.status
      - name: Chemistry
        status: unpublished
      - name: Computer Science
        status: published
      - name: Evolution
        status: unpublished
  - faculty: Politics
    level: Part IA
    courses:
      - name: The Good Ones
        status: published
      - name: The Bad Ones
        status: unpublished
      - name: Commies
        status: published
      - name: Dictators
        status: unpublished
                '''
        d = []
        # filter required
        '''
        for f in Tripos.objects.all().values("id", "department__name", "level__name"):
            courses = []
            for c in EventGroup.objects.filter(tripos=f['id']): #FIXME to prevent multi queries
                courses.append({
                    'name' : c.name,
                    'status' : c.getPublicationStatus()
                })
            d.append(
                     {
                      'faculty' : f["department__name"],
                      'level' : f["level__name"],
                      'courses' : courses
                      }
                     )
        '''

        return d

    def _getTermDates(self, data):
        '''
        termdates:
  - year: 2011/12
    michealmas_start: 4
    michealmas_finish: 2
    lent_start: 17
    lent_finish: 16
    easter_day: 6 april
    easter_start: 24
    easter_finish: 15
  - year: 2012/13
    michealmas_start: 2
    michealmas_finish: 30
    michealmas_finish_month: nov
    lent_start: 15
    lent_finish: 15
    easter_day: 31 march
    easter_start: 23
    easter_finish: 14
        '''
        d = []
        c = None
        lastYear = None
        # FIXME: Filter required, possibly
        for t in Term.objects.all().order_by("academic_year__starting_year", "start").values("academic_year__starting_year", "term_info__name", "start", "end"):
            if t["academic_year__starting_year"] != lastYear:
                if c is not None:
                    d.append(c)
                c = {"year": t["academic_year__starting_year"]}
            if t["start"] == t["end"]:
                c.update({
                    "%s" % t["term_info__name"] : t["start"]
                    })
            else:
                c.update({
                    "%s_start" % t["term_info__name"] : t["start"],
                    "%s_end" % t["term_info__name"] : t["end"],
                    })
            lastYear = t["academic_year__starting_year"]
        if c is not None:
            d.append(c)
        return d

    def _getAdministrators(self, data):
        '''
        administrators:
  - name: Admin Administratorson
    mail: admin@caret.cam.ac.uk
    faculty: History
    permission: faculty
  - name: Admin Administratorson
    mail: admin@caret.cam.ac.uk
    faculty: Politics
    permission: faculty
  - name: Admin Administratorson
    mail: admin@caret.cam.ac.uk
    faculty: History
    permission: key faculty
  - name: Admin Administratorson
    mail: admin@caret.cam.ac.uk
    faculty: Dancing
    permission: super
        '''
        d = []
        for r in OrganisationRole.objects.filter(
                                role__type='A').values(
                                "role__id",
                                "user__first_name",
                                "user__last_name",
                                "user__email",
                                "organisation__name"):
            d.append({
                    "name" : "%s %s" % (r["user__first_name"], r["user__last_name"]),
                    "faculty" : r["organisation__name"],
                    "permission": " ".join([ "%s" % p["name"] for p in Permission.objects.filter(rolepermission__role=r["role__id"]).values("name")])
                    })

        return d

    def _getEventTypes(self, data):
        '''
          types:
    - Lecture
    - Lab
    - Class
    - Practical
        '''
        return [ "%s" % e['name'] for e in EventType.objects.all().values("name")]


    def _getTerms(self, data):
        # might require a filter on organization
        return [ "%s" % t["name"] for t in TermInfo.objects.filter(day=False).values("name") ]


    def _getLevels(self, data):
        return [ "DummyLevel1", "DummyLevel2", "DummyLevel3" ]

    def _getSubjects(self, data):
        # might require acces via Tripos to filter by organization, for the moment I will access direcrtly
        return [ "DummySubject1", "DummySubject2", "DummySubject3" ]


    def page_overview_data(self):
        # Load the UI data and customize it with data for the request.
        # that way we only need to change what's not static.
        data = load_yaml("uidata/overview.yaml")
        data.update({
            'user' : self._getUser(data),
            'recent_changes' : None, #self._getRecentChanges(data),
            'faculties' : self._getFaculties(data),
            'timetables' : self._getTimeTables(data),
            'termdates' : self._getTermDates(data)
            })
        return data

    def administrators(self):
        data = load_yaml("uidata/administrators.yaml")
        data.update({
            'user' : self._getUser(data),
            'administrators' : self._getAdministrators(data),
            })
        return data

    def administrators_add(self):
        data = load_yaml("uidata/administrators-add.yaml")
        data.update({
            'user' : self._getUser(data),
            'faculties' : self._getFaculties(data),
            'organisation' : {
                    'types' : self._getEventTypes(data),
                    'terms' : self._getTerms(data),
                    'classifications' : {
                                'levels' : self._getLevels(data),
                                'subjects' : self._getSubjects(data)
                            }
                }
            })
        return data

    def faculties(self):
        data = load_yaml("uidata/faculties.yaml")
        data.update({
            'user' : self._getUser(data),
            'administrators' : self._getAdministrators(data),
            'faculties' : self._getFaculties(data),
            })
        return data

    def faculties_add(self):
        data = load_yaml("uidata/faculties-add.yaml")
        data.update({
            'user' : self._getUser(data),
            'administrators' : self._getAdministrators(data),
            'faculties' : self._getFaculties(data),
            })
        return data

    def faculties_edit(self):
        data = load_yaml("uidata/faculties-edit.yaml")
        data.update({
            'user' : self._getUser(data),
            'administrators' : self._getAdministrators(data),
            'faculties' : self._getFaculties(data),
            'organisation' : {
                    'types' : self._getEventTypes(data),
                    'terms' : self._getTerms(data),
                    'classifications' : {
                                'levels' : self._getLevels(data),
                                'subjects' : self._getSubjects(data)
                            }
                }
            })
        return data

    def timetables(self):
        data = load_yaml("uidata/timetables.yaml")
        data.update({
            'user' : self._getUser(data),
            'faculties' : self._getFaculties(data),
            'timetables' : self._getTimeTables(data),
            })
        return data
