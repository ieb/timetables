from django.conf.urls.defaults import patterns, url

from timetables.superadministrator import views
from timetables.superadministrator.views import overview
from timetables.superadministrator.views import administrators
from timetables.superadministrator.views import faculties

USERNAME_REGEX = "[\w_@\+\.-]+"

urlpatterns = patterns('',
    # 0.0 - SUPER ADMINISTRATOR OVERVIEW
    url(r'^$',
            overview.Overview.as_view(),
            name="superadmin overview"),

    # 1.0 - ADMINISTRATORS
    url(r'^administrators/$',
            administrators.Administrators.as_view(),
            name="superadmin administrators"),
    # 1.1 - ADD NEW ADMINISTRATOR
    url(r'^administrators/add/$',
            administrators.AddAdministrators.as_view(),
            name="superadmin add administrator"),
    # 1.2 - EDIT ADMINISTRATOR
    url(r'^administrators/edit/(%s)/$' % USERNAME_REGEX,
            administrators.EditAdministrators.as_view(),
            name="superadmin edit administrator"),

    # 2.0 - FACULTIES
    url(r'^faculties/$',
            faculties.Faculties.as_view(),
            name="superadmin faculties"),
    # 2.1 - ADD NEW FACULTY
    # -- 2.1.1 - ADD NEW FACULTY - ADD NEW LEVEL
    # -- 2.1.2 - ADD NEW FACULTY - ADD NEW CLASSIFICATION
    url(r'^faculties/f/group/$',
            faculties.faculty_group_fragment,
            name="superadmin faculties group fragment"),
    url(r'^faculties/f/classifier/$',
            faculties.faculty_classifier_fragment,
            name="superadmin faculties classifier fragment"),

    url(r'^faculties/add/$',
            faculties.AddFaculty.as_view(),
            name="superadmin add faculties"),
    url(r'^faculties/exists/$',
            faculties.faculty_exists),

    # 2.2 - EDIT FACULTY
    url(r'^faculties/edit/(\d+)/$',
            faculties.EditFaculty.as_view(),
            name="superadmin edit faculties"),

    # 4.0 - TIMETABLES
    url(r'^timetables/$',
            views.Timetables.as_view(),
            name="superadmin timetables"),

)
