from django.views.decorators.http import require_GET
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.contrib.auth.decorators import login_required, user_passes_test
from django.utils.decorators import method_decorator
from django.http import (HttpResponseForbidden, HttpResponse, HttpResponseRedirect,
        HttpResponseBadRequest, Http404)
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404
from django import forms

from timetables.utils import TimetablesPaginator
from timetables.utils.ints import int_safe

from timetables.superadministrator.service import SuperAdmin
from timetables.utils.requirejs import js_main_module
from timetables.utils import NavigationTree
from timetables.views import BaseView
from timetables.model import models
from timetables.model.models import User, Organisation, Permission, NotEditable


import json

import logging
from timetables.utils.compat import OrderedDict
from django.utils.datastructures import SortedDict
import operator
from timetables.utils.Json import JSON_CONTENT_TYPE, JSON_INDENT
from timetables.utils.xact import xact
from timetables.utils.http import HttpParameters
log = logging.getLogger(__name__)
del logging

navtree = NavigationTree.from_json(
    { "name": "Overview", "view_name": "superadmin overview", "children":[
        { "name": "Administrators", "view_name": "superadmin administrators",
            "children": [
                { "name":  "Add New Administrators",
                        "view_name": "superadmin add administrator" },
            ]
        },
        { "name": "Faculties", "view_name": "superadmin faculties",
            "children": [
                { "name": "Add New Faculty",
                        "view_name": "superadmin add faculties" },
            ]
        },
        { "name": "Timetables", "view_name": "superadmin timetables" }
    ]}
)

def validate_user(user):
    if not User.is_superadmin(user):
        log.info("Rejected non superadmin user: %s" % user)
        raise PermissionDenied
    return True

class SuperadminBaseView(BaseView):
    def __init__(self):
        super(SuperadminBaseView, self).__init__(navtree)

    # Require a logged in user for all superadmin views. If the login check
    # fails then the user is redirected to the login screen. If the
    # validate_user test fails then a 403 response is generated, as the user IS
    # logged in but simply lacks the required permissions.
    @method_decorator(login_required)
    @method_decorator(user_passes_test(validate_user))
    def dispatch(self, *args, **kwargs):
        return super(SuperadminBaseView, self).dispatch(*args, **kwargs)

    def context(self, request, based_on=None):
        context = (based_on or {}).copy()
        context.update(super(SuperadminBaseView, self).context(request))

        return context

class Timetables(SuperadminBaseView):

    results_per_page = 20

    def js_main_module(self):
        return "superadmin/timetables"

    def list_organisations(self, request):
        return faculty_search(request)
        # should additionally filter on classifier groups of subject and level - others are not displayed on page

    def context(self, request):
        context = super(Timetables, self).context(request)

        context["page_title"] = "Timetables"

        # construct the data required for the table
        pages = TimetablesPaginator(self.list_organisations(request),
                Timetables.results_per_page)
        page = pages.page_safe(request.GET.get(HttpParameters.PAGE, 1))

        context["organisations"] = get_timetable_status(page.object_list.prefetch_related("year", "groupusage_set",
                                                    "groupusage_set__group", "groupusage_set__classifiers",
                                                    "groupusage_set__classifiers__group"))

        context["search_faculty"] = get_faculty_search_string(request, "Search Faculties")
        
        if 'tab' in  request.GET:
            context['select_tab'] = request.GET['tab']

        return context

    def get(self, request):
        context = self.context(request)

        return render(request, "superadministrator/4.0-Timetables.html",
                               context)


"""
Utility function - could be moved somewhere more sensible
Used by Timetables.list_organisations and Faculties view
Gets organisations filtered using any faculty search
"""
def faculty_search(request):
    search_faculty = get_faculty_search_string(request)

    orgs = Organisation.objects.all()
    if search_faculty:
        orgs = orgs.filter(name__icontains=search_faculty)

    return orgs


"""
Utility function - could be moved somewhere more sensible
Used by Timetables.list_organisations and Faculties view
Gets the string entered into the faculty search box
"""
def get_faculty_search_string(request, blank_value=None):
    if request.method == "GET":
        query = request.GET.get(HttpParameters.SEARCH_QUERY, "").strip()
        if query != "":
            return query
    return blank_value

"""
Utilty function - could be moved somewhere more sensible
Used by Timetables.context and Overview view
"""
def get_timetable_status(orgs):
    organisations = {}
    for org in orgs:
        org_id = org.id

        log.debug("Checking organization %s " % org.id)

        # get basic organisation details. Only need to do this once
        if not org_id in organisations:
            organisations[org_id] = {}
            organisations[org_id]["id"] = org.id
            organisations[org_id]["name"] = org.name
            organisations[org_id]["code"] = org.code
            organisations[org_id]["year"] = org.year.starting_year
            organisations[org_id]["level_counts"] = {} # count of total number of subjects found at this level
            organisations[org_id]["published"] = SortedDict()
            organisations[org_id]["unpublished"] = SortedDict()

        # get all GroupUsage instances for this organisation - not sure this is the most efficient way to do things. Might be better to get all GroupUsage in one go (trade off against pagination)
        # investigate pre-loading?
        for gu in org.groupusage_set.all():
            published = gu.group.getPublicationStatus()
            levels = filter(lambda x: x.group.family == 'L', gu.classifiers.all()) # theoretically could have multiple levels / subjects per GroupUsage instance
            subjects = filter(lambda x: x.group.family == 'S', gu.classifiers.all())

            if published == 'Published':
                p = organisations[org_id]["published"]
            else:
                p = organisations[org_id]["unpublished"]

            for l in levels:
                l_value = l.value
                if not l_value in organisations[org_id]["level_counts"]:
                    organisations[org_id]["level_counts"][l_value] = 0
                if l_value not in p:
                    p[l_value] = SortedDict()
                # Uncomment this to test subjects in the view
                #for s in ["Maths","Physics","Chemistry"]:
                #    p[l_value].append(s)
                #   organisations[org_id]["level_counts"][l_value] += 1

                for s in subjects:
                    if s.value not in p[l_value]:
                        p[l_value][s.value] = s
                        organisations[org_id]["level_counts"][l_value] += 1

        # Sort the subjects into order
        for org_id, org in organisations.items():
            for p_id in ["published", "unpublished"]:
                for l_value, l in org[p_id].items():
                    subjects = SortedDict()
                    for s, v in sorted(l.items()):
                        subjects[s] = v
                    org[p_id][l_value] = subjects


    return organisations
