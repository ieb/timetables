"""
This module contains the views for the faculty related screens.
"""

import logging
log = logging.getLogger(__name__)
del logging

import json

from django.views.decorators.http import require_GET
from django.utils.decorators import method_decorator
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponseBadRequest, \
    HttpResponse
from django.core.urlresolvers import reverse

from timetables.utils.http import HttpParameters, ContextKeys
from timetables.superadministrator.views import (SuperadminBaseView,
        faculty_search, get_faculty_search_string)
from timetables.utils import TimetablesPaginator
from timetables.model.models import (User, AcademicYear, Organisation,
        Permission, ClassifierGroup, Classifier, ValidationError,
    Classification)
from timetables.utils.compat import OrderedDict
from timetables.utils.http import Request
from timetables.utils.xact import xact
from timetables.utils.Json import JSON_CONTENT_TYPE, JSON_INDENT
from timetables.utils.ints import int_safe

class Faculties(SuperadminBaseView):
    PAGE_STATE_KEYS = (
                    ( HttpParameters.SEARCH_QUERY, ContextKeys.SEARCH_QUERY),
                    )

    # The number of Faculties to show per page. This can be increased when we're
    # not debugging.
    results_per_page = 20

    def js_main_module(self):
        return None # FIXME: write js module for this page

    def list_faculties(self, request):
        queryset = faculty_search(request)

        # Order the queryset. There's only one field to sort on (name) so no
        # need to check which column to sort on
        ordering = ("-name" if request.GET.get(HttpParameters.SORT_ORDER) ==
                HttpParameters.DESC else "name")
        queryset = queryset.order_by(ordering)
        return queryset

    @staticmethod
    def orgs_mapped_to_key_admins(organisations):
        if organisations is None or len(organisations) == 0:
            return {}
        key_admin_perm = Permission.objects.admin_key_admin()
        key_admins = (User.objects.filter(
                organisationrole__organisation__in=organisations,
                organisationrole__role__permissions=key_admin_perm)
                .prefetch_related("organisationrole_set__organisation"))
        org_admins = OrderedDict((org, list()) for org in organisations)
        for admin in key_admins:
            # This shouldn't make a query due to the prefetch...
            for org_role in admin.organisationrole_set.all():
                if org_role.organisation in org_admins:
                    org_admins[org_role.organisation].append(admin)
        return org_admins

    def context(self, request, based_on=None):
        context = super(Faculties, self).context(request, based_on=based_on)

        pages = TimetablesPaginator(self.list_faculties(request),
                Faculties.results_per_page)
        page = pages.page_safe(request.GET.get(HttpParameters.PAGE, 1))

        context["page"] = page
        context["org_admins"] = self.orgs_mapped_to_key_admins(page.object_list)
        context["page_title"] = "Faculties"

        # pass sort query and order to the template:
        if HttpParameters.SEARCH_QUERY in request.GET:
            context[ContextKeys.SEARCH_QUERY] = \
                    request.GET[HttpParameters.SEARCH_QUERY]
        if HttpParameters.SORT in request.GET:
            context[ContextKeys.SORT] = "name" # can only ever sort on name
        if HttpParameters.SORT_ORDER in request.GET:
            context[ContextKeys.SORT_ORDER] = \
                    request.GET[HttpParameters.SORT_ORDER]

        # All query parameters that influence paging, these will be saved by
        context["page_state_keys"] = Faculties.PAGE_STATE_KEYS

        # for consistency this should use HttpParameters.SEARCH_QUERY and ContextKey.SEARCH_QUERY provided it doesnt need to be scoped.
        context["search_faculty"] = get_faculty_search_string(request, "Search Faculties")

        return context

    def get(self, request):
        context = self.context(request)
        return render(request, "superadministrator/2.0-Faculties.html",
                               context)

class FacultyFormProcessor(object):
    """
    A mixin to provide the model updating and form data processing for the edit
    and add faculty views.
    """

    def validate_org_not_archived(self, org):
        if org.year.is_archived:
            raise ValidationError("Organisation is in an archived year and so "
                    "cannot be edited: %s" % org)

    def validate_group_types(self, group_data):
        # Ignore groups to be deleted
        undeleted_groups = [g for g in group_data if not g["deleted"]]

        level_count = len([g for g in undeleted_groups if g["family"] == "L"])
        subject_count = len([g for g in undeleted_groups if g["family"] == "S"])

        msg = None
        if not level_count == 1:
            msg = "Expected 1 level classifier group but got: %d" % level_count
        if not subject_count == 1:
            msg = ("Expected 1 subject classifier group but got: %d" %
                    subject_count)

        if msg:
            log.error(msg)
            raise ValidationError(msg)

    def validate_classifier_count(self, classifiers_data):
        undeleted_classifiers = [c for c in classifiers_data
                if not c["deleted"]]

        if len(undeleted_classifiers) == 0:
            msg = "At least one classifier must be provided"
            log.error(msg)
            raise ValidationError(msg)

    @method_decorator(xact)
    def save(self, org, org_data):
        """
        Updates org with the representation given in org_data
        
        Args:
            org: An Organisation() model instance to save into.
            org_data: A dict of data representing an Organisation and it's
                classifiers.
        Returns:
            The updated Organisation instance.
        """
        self.validate_org_not_archived(org)
        self.validate_group_types(org_data["groups"])

        year = AcademicYear.objects.get(id=org_data["year_id"],
                is_archived=False)

        org.year = year
        org.name = org_data["name"]
        org.code = org_data["code"]

        if org_data["deleted"] and org.id is not None:
            # FIXME: should we explicitly remove classifiers etc or do 
            # (or should) they get killed by fk constraints/triggers/events?
            org.delete()
            return org
        else:
            org.save()

        classifiers = dict()
        for group_data in org_data["groups"]:
            self.save_group(org, group_data, classifiers)
        for group_data in org_data["groups"]:
            self.update_parents(group_data, classifiers)
        return org

    def save_group(self, org, group_data, classifiers):
        if group_data["id"] is None:
            group = ClassifierGroup()
        else:
            # Going through the org ensures that the specified ID is owned
            # by the org
            group = org.classifiergroup_set.get(id=group_data["id"])
        if group_data["deleted"]:
            if group.id is not None:
                group.delete()
        else:
            group.name = group_data["name"]
            group.family = group_data["family"]
            group.organisation = org
            group.save()

        self.validate_classifier_count(group_data["classifiers"])
        
        for classifier_data in group_data["classifiers"]:
            classifiers[classifier_data['id']] = self.save_classifier(org, group, classifier_data)
            
    def update_parents(self, group_data, classifiers):
        '''
        Update the classifier parents, taking account of newly created classifiers.
        :param group_data:
        :param classifiers:
        '''
        log.error("Classifiers %s " % classifiers)
        for classifier_data in group_data["classifiers"]:
            classifier = classifiers[classifier_data['id']]
            if 'parent' in classifier_data and classifier_data['parent'] is not None:
                if classifier_data['parent'] in classifiers:
                    classifier.parent = classifiers[classifier_data['parent']]
                else:
                    log.error("Classifier outside this set %s " % classifier_data['parent'])
                    classifier.parent = Classifier.objects.get(id=classifier_data['parent'])
                classifier.save()
            

    def save_classifier(self, org, group, classifier_data):
        name = classifier_data["value"]
        website = classifier_data["website"]
        description = classifier_data["description"]
        contact_person = classifier_data["contact_person"]
        deleted = classifier_data["deleted"]
        try:
            classifier_id = int(classifier_data["id"])
        except ValueError:
            classifier_id = None
            
        if classifier_id is None:
            classifier = Classifier.objects.create(
                    value=name,
                    group=group
                    # parent will be setup later                    
                    )
        else:
            classifier = group.classifier_set.get(id=classifier_id)
            if deleted:
                classifier.delete()
            else:
                classifier.value = name
                classifier.save()
            
        try:
            classification  = Classification.objects.get(classifier=classifier)
            if deleted:
                classification.delete()
            else:
                classification.name = name
                classification.website = website
                classification.description = description
                classification.contact_person = contact_person
                classification.save()
        except Classification.DoesNotExist:
            if not deleted:
                classification = Classification.objects.create(
                            name = name,
                            website = website,
                            description = description,
                            contact_person = contact_person,
                            owner=org,
                            classifier=classifier)
            
        return classifier

    def load_form_data(self, request):
        groups_tree = Request.build_tree(request, "groups", "groups",
                    ["classifiers"], {
                        "groups": ["id", "deleted", "family", "name"],
                        "classifiers": ["id", "deleted", "value", "parent", "contact_person", "website", "description"]
                    })

        # Convert form strings to python data types and convert the dicts 
        # of nested elements into lists
        groups = [{
            "deleted": g["deleted"] == "true",
            "id": None if not g["id"] else int(g["id"]),
            "family": g["family"],
            "name": g["name"].strip(),
            "classifiers": [{
                    "deleted": c["deleted"] == "true",
                    "id": None if not c["id"] else c["id"],
                    "parent": None if not c["parent"] else c["parent"],
                    "contact_person": "" if not c["contact_person"] else c["contact_person"],
                    "website": "" if not c["website"] else c["website"],
                    "description": "" if not c["description"] else c["description"],
                    "value": c["value"].strip()
                } for _, c in g["classifiers_"].items()]

        } for _, g in groups_tree.items()]

        organisation = {
            "deleted": request.POST.get("delete_faculty") == "on",
            "name": request.POST["name"].strip(),
            "code": request.POST["code"].strip(),
            "year_id": int(request.POST["year"]),
            "groups": groups
        }

        return organisation

class BaseFacultyForm(SuperadminBaseView):
    def __init__(self, *args, **kwargs):
        super(BaseFacultyForm, self).__init__(*args, **kwargs)

        self.form_processor = FacultyFormProcessor()

    def js_main_module(self):
        return "superadmin/faculties-add"

class AddFaculty(BaseFacultyForm):


    def get_organisation(self):
        return Organisation(year=AcademicYear.objects.active_years()[0])

    def get_groups(self):
        return [ClassifierGroup(name="Levels", family="L"),
                ClassifierGroup(name="Subjects", family="S")]

    def context(self, request):
        context = super(AddFaculty, self).context(request);

        context["page_title"] = "Add New Faculty"
        context["years"] = AcademicYear.objects.active_years()
        context["organisation"] = self.get_organisation()
        context["groups"] = self.get_groups()
        return context

    def get(self, request):
        context = self.context(request)
        return render(request, "superadministrator/2.1-Faculties-Add.html",
                context)

    @method_decorator(xact)
    def post(self, request):
        try:
            organisation_data = self.form_processor.load_form_data(request)
        except:
            log.exception("Error loading POST data")
            # Should only occur if someone manually screws with the POST
            return HttpResponseBadRequest("Form data was invalid.")

        try:
            self.form_processor.save(self.get_organisation(), organisation_data)
        except ValidationError:
            log.exception("Invalid POST data passed to save().")
            return HttpResponseBadRequest("Form data was invalid")
        return HttpResponseRedirect(reverse("superadmin faculties"))


class EditFaculty(BaseFacultyForm):

    def js_main_module(self):
        return "superadmin/faculties-add"

    def context(self, request, organisation_id):
        context = super(EditFaculty, self).context(request);

        context["page_title"] = "Edit Faculty"
        context["years"] = AcademicYear.objects.active_years()
        context["organisation"] = self.get_organisation(organisation_id)
        context["groups"] = self.get_groups(organisation_id)
        log.error( self.get_groups(organisation_id) )

        org_admins = User.objects.admins(organisation=context["organisation"])
        context["key_admins"] = Faculties.orgs_mapped_to_key_admins([context["organisation"]])[context["organisation"]]
        context["administrators"] = org_admins


        context["url_params"] = {
                "view": "superadmin edit faculties",
                "params": [organisation_id]}
        return context

    def get_organisation(self, organisation_id):
        return Organisation.objects.get(id=organisation_id)

    # Return the groups sorted by group family. It's possible we might want to
    # allow a more sophisticated ordering scheme in the future, but this is
    # probably the best we an do given we don't know what custom classifier groups
    # would be used for or where they should sit in the heirarchy.
    def get_groups(self, organisation_id):
        return ClassifierGroup.objects.filter(organisation__id=organisation_id).order_by('family')

    def get(self, request, organisation_id):
        context = self.context(request, organisation_id)

        return render(request, "superadministrator/2.2-Faculties-Edit.html",
                context)

    @method_decorator(xact)
    def post(self, request, organisation_id):
        try:
            organisation_data = self.form_processor.load_form_data(request)
        except:
            log.exception("Error loading POST data")
            # Should only occur if someone manually screws with the POST
            return HttpResponseBadRequest("Form data was invalid.")

        try:
            self.form_processor.save(self.get_organisation(organisation_id),
                    organisation_data)
        except ValidationError:
            log.exception("Invalid POST data passed to save().")
            return HttpResponseBadRequest("Form data was invalid")
        return HttpResponseRedirect(reverse("superadmin faculties"))


def faculty_group_fragment(request):
    index = int_safe(request.GET.get("index"), default=0)

    return render(request, "superadministrator/fragments/classifier-group.html",
            {"group": ClassifierGroup(family="C"), "group_index": index})

def faculty_classifier_fragment(request):
    group_index = int_safe(request.GET.get("group_index"), default=0)
    classifier_index = int_safe(request.GET.get("classifier_index"), default=0)

    return render(request, "superadministrator/fragments/classifier.html",
            {"classifier": Classifier(), "classifier_index": classifier_index,
                    "group_index": group_index})


@require_GET
def faculty_exists(request):
    year = int_safe(request.GET.get("year", None))
    name = request.GET.get("name", None)
    code = request.GET.get("code", None)
    if year is not None and name is not None and code is None:
        exists = Organisation.objects.filter(
                year__starting_year=year, name=name).exists()
    elif year is not None and code is not None and name is None:
        exists = Organisation.objects.filter(
                year__starting_year=year, code=code).exists()
    else:
        return HttpResponseBadRequest("Query params should be (name, year) or"
                " (code, year).")

    if name is not None:
        fields = {"name": name, "year": year}
    else:
        fields = {"code": code, "year": year}
    response_json = {
        "fields": fields,
        "exists": exists
    }
    return HttpResponse(json.dumps(response_json, indent=JSON_INDENT),
            content_type=JSON_CONTENT_TYPE)
