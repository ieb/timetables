"""
View code for the Superadministrator Overview page.
"""
from django.shortcuts import render

from timetables.model.models import Term, Organisation, User
from timetables.superadministrator.service import SuperAdmin
from timetables.utils.datetimes import (
        CommonDateComponent, weekday_date_accessor_short,
        dayofmonth_date_accessor_short, month_date_accessor_short)
from timetables.utils.compat import OrderedDict
from timetables.utils.requirejs import js_main_module
from timetables.superadministrator.views import (SuperadminBaseView,
        get_timetable_status)

from itertools import groupby
from operator import add

class DateColumn(object):
    def __init__(self, dates, accessors, min_common_frequency=0.5,
            unsharable_accessors=None):
        """
        
        Args:
            dates: A list of dates to build the column from.
            accessors: A list of accessors for the date objects in dates.
                These are instances of utils.datetimes.DateComponentAccessor.
            min_common_frequency: The proportion of date values which must be
                the same for the value to be promoted to the column header. 
                Default: 0.5
            unsharable_accessors: A set of accessors whose values should never
                be considered common across the cells of the column.
        """
        self._common_components = dict(
                [(acc, CommonDateComponent(dates, acc)) for acc in accessors])
        self._accessors = accessors
        self._cells = [DateColumn.DateCell(self, date) for date in dates]
        self._min_common_frequency = min_common_frequency
        self._unsharable_accessors = unsharable_accessors or set()

    def accessors(self):
        return self._accessors

    def common_component(self, accessor):
        assert accessor in self._accessors, "accessor must be one of ours"
        assert accessor in self._common_components, (
            "our accessors are always in _common_components")
        return self._common_components[accessor]

    def is_common_value(self, date, accessor):
        # Some accessors are marked as never being sharable (common across 
        # cells)
        if accessor in self._unsharable_accessors:
            return False

        value = accessor.get_value(date)
        common_component = self.common_component(accessor)
        # A date value is common (can be shared by placing in the column header)
        # if the most common value has a frequency greater than the threshold
        # and the value is the common value.
        return (self.is_common_component_sufficiently_frequent(common_component)
                and common_component.value == value)

    def is_common_component_sufficiently_frequent(self, common_component):
        return common_component.frequency >= self._min_common_frequency

    def common_values(self):
        common_components = filter(
                self.is_common_component_sufficiently_frequent,
                self._common_components.itervalues())
        return [(comp.value, comp.component_accessor)
                for comp in common_components]

    def common_names(self):
        return [accessor.name_value(value) for (value, accessor)
                in self.common_values()]

    def cells(self):
        """Gets a list of date cells this column contains."""
        return self._cells

    class DateCell(object):
        def __init__(self, datecolumn, date):
            """
            Represents a single cell in a DateColumn.
            
            Args:
                datecolumn: A DateColumn instance that this cell is part of
                date: A datetime.date or datetime.datetime instance.
            """
            self._datecolumn = datecolumn
            self._date = date

        def all_values(self):
            accessors = self._datecolumn.accessors()
            return [(acc.get_value(self._date), acc) for acc in accessors]

        def unique_values(self):
            """Gets a list of the date values for this cell which are different
            from the most common values of the parent datecolumn."""
            accessors = self._datecolumn.accessors()
            return [(acc.get_value(self._date), acc) for acc in accessors
                    if not self._datecolumn.is_common_value(self._date, acc)]

        def unique_names(self):
            return [accessor.name_value(value) for (value, accessor)
                    in self.unique_values()]

        def __unicode__(self):
            return " ".join(self.unique_names())

        def __repr__(self):
            return self.__unicode__()


class TermDatesTable(object):
    """Represents the data backing a Term dates table.
    """
    def __init__(self, terms, date_accessors, unsharable_accessors=None):
        """"""
        self._date_accessors = date_accessors
        # assumption: len(terms) is small (< 200)
        all_terms = sorted(terms)

        # Group terms by name into a mapping of name => [term, term ...]
        by_name = groupby(all_terms, lambda term: term.term_info.name)
        self._terms = OrderedDict()
        for name, terms in by_name:
            self._terms[name] = TermDatesTable.TermColumns(
                    list(terms), self._date_accessors, unsharable_accessors)

        # Check the years are the same across terms
        # self._terms contains a list of term dates for each term keyed by term name.
        # self._terms['lent'] contains all known lent term dates.
        # This section of code transposes that array and ensures that each column contains
        # terms from the same academic year. If it does not, (because a terms has not been)
        # defined it will fail with a value error and break all pages. Therefore
        # it is vital when adding a  year that all terms are defined. (IMHO we need to fix this).
        years = []
        for year_terms in zip(*[termcolumns.terms()
                for termcolumns in self._terms.itervalues()]):
            first_year = year_terms[0].academic_year
            for term in year_terms[1:]:
                if term.academic_year != first_year:
                    raise ValueError("All Terms must be defined for all years, "
                            "if that is not done then this error will happen."
                            "Terms were not correctly grouped by year."
                            " First term's year: %s, subsequent year: %s" % (
                            first_year, term.academic_year))
            years.append(first_year)
        self._years = years

    def years(self):
        return self._years

    @staticmethod
    def from_std_terms_in_active_years(date_accessors, **kwargs):
        standard_active_terms = Term.objects.default_active_terms()
        return TermDatesTable(standard_active_terms, date_accessors, **kwargs)

    def column_groups(self):
        """Get the available column groups as a list of 3-tuples:
        (name, columns_holder, holder_width)."""
        return [(name, self._terms[name], len(self._terms[name].columns()))
                for name in self._terms]

    def individual_columns(self):
        "Gets a list of all DateColumns in order."
        paired_columns = [termcolumn.columns()
                for termcolumn in self._terms.values()]
        # Flatten nested lists
        return reduce(add, paired_columns, [])

    def rows(self):
        """Gets a list of the rows in the table.
        
        Each entry is a two-tuple containing the year the row represents, and
        a further tuple containing the DateCell from each column for the year.
        i.e.:
            [(AcademicYear, (DateCell, DateCell, DateCell)),
             (AcademicYear, (DateCell, DateCell, DateCell)),
             (AcademicYear, (DateCell, DateCell, DateCell))]
        """
        # Zip up the nth year with a tuple of each date cell of the nth row 
        return zip(
                self.years(),
                # Create a list containing tuples containing the nth cell of 
                # each column.
                zip(*[datecolumn.cells()
                        for datecolumn in self.individual_columns()]))

    class TermColumns(object):
        def __init__(self, terms, accessors, unshared_accessors):
            assert len(set([t.term_info.name for t in terms])) == 1, ("All"
                    " terms must have the same name: %s" % terms)
            self._terms = terms
            self._accessors = accessors
            self._unshared_accessors = unshared_accessors

            # Build the start date column
            self._columns = [self._build_column([t.start for t in terms])]
            # Term objects can also represent single days. These only have one
            # date column.
            if not terms[0].term_info.day:
                self._columns.append(self._build_column([t.end for t in terms]))

        def terms(self):
            return self._terms

        def _build_column(self, dates):
            return DateColumn(dates, self._accessors,
                    unsharable_accessors=self._unshared_accessors)

        def columns(self):
            return self._columns

# The accessors in this list define the date components to show in each table
# cell. Also, the most common value of each of these is shown in the table 
# header.
TABLE_CELL_DATE_COMPONENTS = [
        weekday_date_accessor_short,
        dayofmonth_date_accessor_short,
        month_date_accessor_short
]
UNSHARABLE_DATE_COMPONENTS = set([
        dayofmonth_date_accessor_short
])

def term_dates_table():
    return TermDatesTable.from_std_terms_in_active_years(
            TABLE_CELL_DATE_COMPONENTS,
            unsharable_accessors=UNSHARABLE_DATE_COMPONENTS)

class Overview(SuperadminBaseView):

    def js_main_module(self):
        return "superadmin/overview"

    def context(self, request):
        context = super(Overview, self).context(request)

        context["termdatetable"] = term_dates_table()

        context["organisations_by_years"] = (
                Organisation.objects.grouped_into_years())

        # construct the data required timetable status table
        organisations = Organisation.objects.all().prefetch_related("year", "groupusage_set",
                                                    "groupusage_set__group", "groupusage_set__classifiers",
                                                    "groupusage_set__classifiers__group")
        context["organisations"] = get_timetable_status(organisations)

        # Pass organisation data to javascript as a JSON bootstrap module.
        context["bootstraps"].update({
                "organisationsByYear":
                        Organisation.objects.organisation_years_bootstrap()})

        return context

    def get(self, request):
        context = self.context(request)

        return render(request,
            "superadministrator/0.0-Super-Administrator-Overview.html", context)
