from django import template
from django.core.urlresolvers import reverse
import json
import logging
from django.conf import settings
from urllib import urlencode
from timetables.utils.Json import JSON_INDENT
from timetables.utils.http import ContextKeys, HttpParameters

LOG = logging.getLogger(__name__)

register = template.Library()

@register.simple_tag
def url_dynamic(url_description):
    """
    A template tag which can be used in place of the built-in {% url ... %} tag when the
    url parameters are not fixed at the time of writing the template. For example when
    writing a template to output the URL to an arbitrary Django view.
    
    Instead of taking a variable number of arguments, url_dynamic takes a single dict
    containing "view" and "params" keys which is used to parameterise a call to
    urlresolvers.reverse(). The "params" key's value can be either a list of values, or
    dict for populating named patterns. For backwards compatibility with the normal url
    tag, a single string argument is treated as a view name.
    
    Usage:
    
    {% url_dynamic foodata.view %}
    
    Where foodata.view is:
    {
        "view": "my view name",
        "params": {"someurlparam": 4, "anotherparam": 5}
    }
    """
    # If the param was a string, treat it as the view name
    if isinstance(url_description, basestring):
        return reverse(url_description)
    elif isinstance(url_description, dict) and url_description.has_key('view'):
        # Otherwise treat it as a dict, with view, args and kwargs params which are used
        # to parameterise the reverse() call.
        params = url_description.get('params', [])
        return _reverse_with_params(url_description["view"], params)

    raise template.TemplateSyntaxError("url_dynamic tag requires one argument which is"
                                       " either a string view name, or a dict with at"
                                       " least a 'view' key mapping to the view name,"
                                       " and possibly 'args' and or 'kwargs', containing"
                                       "a list of url parameters or dict of named"
                                       " parameters respectively. Got: {0}"
                                       .format(url_description))

def _reverse_with_params(name, params):
    if isinstance(params, list):
        return reverse(name, args=params)
    elif isinstance(params, dict):
        return reverse(name, kwargs=params)
    raise ValueError("url_dynamic params should be either a list or dict. Got: {0}"
                     .format(params))

@register.filter
def humanise(string):
    """Makes python_style_underscore_strings more human readable by replacing
    underscores with spaces and uppercasing the first character of each word.
    
    Examples:
        humanise("python_style_underscore_strings") 
            => Python Style Underscore Strings
    """
    no_underscores = string.replace("_", " ")
    words = no_underscores.split(" ")
    uppercased_words = map(_capitalise_first, words)
    return " ".join(uppercased_words)

def _capitalise_first(word):
    return word[:1].upper() + word[1:]


@register.filter(name="json")
def json_encode(object):
    """JSON encodes the input value, returning JSON string."""

    try:
        json_string = json.dumps(object, indent=JSON_INDENT)
    except:
        LOG.exception("|json filter: Error JSON encoding object: %s" % object);
        json_string = "null /* error encoding JSON */"

    # In order to prevent HTML/JS injection attacks when including user 
    # controlled JSON content in <script> tags, we need to escape closing tag 
    # sequences in JSON strings, otherwise an attacker could insert a literal 
    # "</script> HTML HERE" sequence in a JSON string. 
    # See: http://www.w3.org/TR/html4/appendix/notes.html#h-B.3.2.1

    # Escape the sequence </ with the equivalent unicode code points.
    return json_string.replace("</", "\u003C\u002F")


@register.tag
def setting (parser, token):
    try:
        _, option, defaultValue = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag requires a name and a default eg {%% setting \"SETTING\" \"a default\" %%} " % token.contents[0]
    return SettingNode(option, defaultValue)

class SettingNode (template.Node):
    def __init__ (self, option, defaultValue):
        self.option = option
        self.defaultValue = defaultValue

    def render (self, context):
        # if FAILURE then FAIL silently
        try:
            return str(settings.__getattr__(self.option))
        except:
            return self.defaultValue




@register.tag
def sort_column_class(parser, token):
    '''
    Tag that generates column sort classes based on a standard
    :param parser:
    :param token:
    '''
    try:
        _, this_cname = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag requires this_column_name  eg {%% sort_column_class \"stitle\"  %%} " % token.contents[0]
    return SortColumnClassNode(this_cname[1:-1])


class SortColumnClassNode(template.Node):
    def __init__(self, this_cname):
        self.this_cname = this_cname
        self.sort = template.Variable(ContextKeys.SORT)
        self.sort_order = template.Variable(ContextKeys.SORT_ORDER)

    def render (self, context):
        try:
            sort = self.sort.resolve(context)
        except template.VariableDoesNotExist:
            sort = None
        try:
            sort_order = self.sort_order.resolve(context)
        except template.VariableDoesNotExist:
            sort_order = None
        if self.this_cname == sort:
            if sort_order == HttpParameters.ASC:
                return "ic-asc-arrow"
            else:
                return "ic-desc-arrow"
        return ""

@register.tag
def save_paging_state(parser, token):
    '''
    Tag that generates column sort classes based on a standard
    :param parser:
    :param token:
    '''
    parts = token.split_contents()
    if len(parts) == 2:
        return SavePageingAndSortingNode(parts[1][1:-1].lower())
    elif len(parts) > 2:
        return SavePageingAndSortingNode(parts[1][1:-1].lower(), excluded_terms=[ t[1:-1] for t in parts[2:]])
    else:
        raise template.TemplateSyntaxError, "%r tag requires metbod (GET or POST)  eg {%% save_paging_state \"GET\" [ excluded params ] %%} " % token.contents[0]


@register.tag
def save_page_state(parser, token):
    '''
    Tag that generates column sort classes based on a standard
    :param parser:
    :param token:
    '''
    parts = token.split_contents()
    if len(parts) == 2:
        return SaveSortNode(parts[1][1:-1].lower())
    elif len(parts) > 2:
        return SaveSortNode(parts[1][1:-1].lower(), excluded_terms=[ t[1:-1] for t in parts[2:]])
    else:
        raise template.TemplateSyntaxError, "%r tag requires metbod (GET or POST)  eg {%% save_paging_and_sorting \"GET\"  %%} " % token.contents[0]

@register.tag
def save_context(parser, token):
    '''
    Saves list of context terms specified by param_name, context_name pairs
    :param parser:
    :param token:
    '''
    pairs = token.split_contents()
    method = pairs[1].lower()[1:-1]
    terms = []
    for i in range(2, len(pairs), 2):
        terms.append((pairs[i][1:-1], pairs[i + 1]))

    return SaveContextList(method, terms)

@register.tag
def sort_column_href (parser, token):
    try:
        _, this_cname = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag requires column_name  eg {%% sort_column_href \"stitle\"  %%} " % token.contents[0]
    return SortColumnHrefNode(this_cname[1:-1])

@register.tag
def sort_and_page_href(parser, token):
    return SortingAndPagingHrefNode()

class SaveContextList(template.Node):
    '''
    Generates form tags or a url to represent the state of the static terms and dynamic context_terms passed in
    '''
    def __init__(self, method, terms, context_terms=None, excluded_terms=[]):
        self._variables = [(name, template.Variable(t)) for name, t in terms]
        self._context_terms = context_terms
        self._excluded_terms = excluded_terms
        self.as_url = method == "get"

    def render(self, context):
        params = self._get_params(context)
        if self.as_url:
            if len(params) == 0:
                return "1=1"
            else:
                return urlencode(params, doseq=True)
        else:
            tags = []
            for n, vv in params.iteritems():
                if n not in self._excluded_terms:
                    if isinstance(vv, list):
                        for v in vv:
                            tags.append('<input type="hidden" name="%s" value="%s" />' % (n, v))
                    else:
                        tags.append('<input type="hidden" name="%s" value="%s" />' % (n, vv))
            return "".join(tags)

    def _get_params(self, context):
        variables = dict(self._variables)
        if self._context_terms is not None:
            try:
                ct = template.Variable(self._context_terms)
                for n, v in ct.resolve(context):
                        variables[n] = template.Variable(v)
            except template.VariableDoesNotExist:
                pass
        params = {}
        for n, v in variables.iteritems():
            try:
                vv = v.resolve(context)
                if vv is not None:
                    params[n] = vv
            except template.VariableDoesNotExist:
                pass

        return params



class SaveSortNode(SaveContextList):
    '''
    Creates a href or form elements containing the page state params no paging
    '''
    def __init__(self, method, excluded_terms=[]):
            super(SaveSortNode, self).__init__(method,
                        ((HttpParameters.SORT, ContextKeys.SORT),
                         (HttpParameters.SORT_ORDER, ContextKeys.SORT_ORDER),
                         ),
                        "page_state_keys",
                        excluded_terms=excluded_terms)

class SavePageingAndSortingNode(SaveContextList):
    '''
    Creates a href or form elements containing the page state params and paging
    '''
    def __init__(self, method, excluded_terms=[]):
            super(SavePageingAndSortingNode, self).__init__(method,
                        ((HttpParameters.SORT, ContextKeys.SORT),
                         (HttpParameters.SORT_ORDER, ContextKeys.SORT_ORDER),
                         (HttpParameters.PAGE, ContextKeys.CURRENT_PAGE),
                         ),
                        "page_state_keys",
                        excluded_terms=excluded_terms)


class SortingAndPagingHrefNode(SaveContextList):
    '''
    Creates a href containing the page state params and appropriate column headings.
    '''
    def __init__(self,excluded_terms=[]):
        super(SortingAndPagingHrefNode, self).__init__("get",
                        ((HttpParameters.SORT, ContextKeys.SORT),
                         (HttpParameters.SORT_ORDER, ContextKeys.SORT_ORDER),
                         (HttpParameters.PAGE, ContextKeys.CURRENT_PAGE),
                         (HttpParameters.SEARCH_QUERY, ContextKeys.SEARCH_QUERY),
                         ),
                         excluded_terms=excluded_terms)


class SortColumnHrefNode(SaveContextList):
    '''
    Creates a href containing the page state params and appropriate column headings.
    '''
    def __init__(self, this_cname, excluded_terms=[]):
        super(SortColumnHrefNode, self).__init__("get",
                        ((HttpParameters.SORT, ContextKeys.SORT),
                         (HttpParameters.SORT_ORDER, ContextKeys.SORT_ORDER),
                         (HttpParameters.PAGE, ContextKeys.CURRENT_PAGE),
                         ),
                        "page_state_keys",
                        excluded_terms=excluded_terms)
        self.sort = template.Variable(ContextKeys.SORT)
        self.sort_order = template.Variable(ContextKeys.SORT_ORDER)
        self.this_cname = this_cname

    def _get_params(self, context):
        params = super(SortColumnHrefNode, self)._get_params(context)
        params.update({
            HttpParameters.SORT : self.this_cname,
            HttpParameters.SORT_ORDER : HttpParameters.ASC
            })

        try:
            sort = self.sort.resolve(context)
        except template.VariableDoesNotExist:
            sort = None
        try:
            sort_order = self.sort_order.resolve(context)
        except template.VariableDoesNotExist:
            sort = None
        if self.this_cname == sort:
            if sort_order == HttpParameters.ASC:
                params[HttpParameters.SORT_ORDER] = HttpParameters.DESC
        return params




@register.filter
def dict_key_value(dictionary, key_name):
    # heavily borrowed from https://code.djangoproject.com/ticket/3371; not sure why this isn't a built-in filter :|
    try:
        value = dictionary[key_name]
    except KeyError:
        value = "unknown"

    return value


@register.filter
def trim(s):
    return s.strip()
