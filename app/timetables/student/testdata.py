from timetables.utils.staticdata import create_yaml_loader_relative_to_file

load_yaml = create_yaml_loader_relative_to_file(__file__)

def page_home_not_loggedin_data():
    # Use the same data as the logged-in home page, but strip the user data so
    # that nobody is logged in.
    template_context = page_home_loggedin_data()
    template_context['user'] = {'is_logged_in': False}
    return template_context

def page_home_loggedin_data():
    return load_yaml("uidata/home-loggedin.yaml")

def page_browse_events_loggedin_data():
    return load_yaml("uidata/browse-events.yaml")

def page_browse_events_not_loggedin_data():
    template_context = page_browse_events_loggedin_data()
    template_context['user'] = {'is_logged_in': False}
    return template_context

def page_browse_events_loggedin_as_admin():
    template_context = page_browse_events_loggedin_data()
    template_context['user']['isAdmin'] = True
    return template_context

def page_search_events_loggedin_data():
    return load_yaml("uidata/search-events.yaml")

def page_search_events_not_loggedin_data():
    template_context = page_search_events_loggedin_data()
    template_context['user'] = {'is_logged_in': False}
    return template_context

def page_search_events_loggedin_as_admin():
    template_context = page_search_events_loggedin_data()
    template_context['user']['isAdmin'] = True
    return template_context

def page_lecture_list_data():
    return load_yaml("uidata/lecture-list.yaml")

def page_timetable_monthview_data():
    return load_yaml("uidata/timetable-monthview.yaml")

def page_timetable_termview_data():
    return load_yaml("uidata/timetable-termview.yaml")

def page_timetable_weekview_data():
    return load_yaml("uidata/timetable-weekview.yaml")
    
def page_timetable_listview_data():
    return load_yaml("uidata/timetable-listview.yaml")