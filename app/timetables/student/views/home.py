from timetables.student.views import StudentBaseView, pt_get_ids

from timetables.model.models import AcademicYear, Organisation, User

from timetables.superadministrator.views.overview import term_dates_table


"""
Homepage (logged in or out)
"""
class HomeView(StudentBaseView):

    def __init__(self):
        super(HomeView, self).__init__()
        self.template = "student/1.X-Home.html"

    def context(self, request, **kwargs):
        context = super(HomeView, self).context(request)
        context["termdatetable"] = term_dates_table()
        context["user"] = User.as_context(request.user)
        
        # get the student's personal timetable for the current user and selected year
        pt = pt_get_ids( request, context["page_year"] )
        context["bootstraps"].update({"personal_timetable": pt})
        
        return context