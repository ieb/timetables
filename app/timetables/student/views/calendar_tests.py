from django.test import TestCase
from timetables.student.views.calendar import MonthListCalendar

from datetime import datetime, timedelta

class MonthListCalendarTest(TestCase):
    
    def test_month_range(self):
        start, end = MonthListCalendar("foo", 2012, 6)._month_range()
        self.assertEqual(datetime(2012, 6, 1), start)
        self.assertEqual(datetime(2012, 7, 1) - timedelta(microseconds=1), end)

    def test_load_events(self):
        events = MonthListCalendar("hwtb2", 2012, 10)._load_events()
        self.assertTrue(len(events) == 0)
