'''
Created on Jul 31, 2012

@author: ieb
'''
import unittest
from timetables.utils.v1.grouptemplate import GroupTemplate
from timetables.utils.v1 import pparser
import logging
from timetables.utils.v1.year import Year
import datetime

log = logging.getLogger(__name__)
del logging

# Note, this is Not a Django unit test

class Test(unittest.TestCase):


    def setUp(self):
        
        pass


    def tearDown(self):
        pass



    def test_basic_operation(self):
        g = GroupTemplate("TuThSa12")
        p = pparser.fullparse("Mi1 Tu12 x4",g)
        g.add_patterns(p)
        log.error(p.format(group = g))
        log.error(g.get_patterns())
        
    def test_generate_events(self):
        g = GroupTemplate("TuThSa12")
        p = pparser.fullparse("Mi1 Tu12 x4",g)
        year = Year([datetime.date(2012,10,2),datetime.date(2013,1,15),datetime.date(2013,4,23)])
        for start, end in year.atoms_to_isos(p.patterns()):
            log.error("Start: %s, End: %s " % (start, end))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()