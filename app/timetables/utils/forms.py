from django import forms
from django.forms import formsets

class FormStateExtractor(object):
    """Represents a loader stage in a ProgressiveForm.
    
    StateExtractors are responsible for examining a form's cleaned_data to 
    extract the required additional keyword params to pass back to the form's
    constructor so that the form can instantiate the required fields to validate
    the next stage's data.
    """
    def __init__(self):
        self.extraction_run = False

    def run_extraction(self, form):
        assert hasattr(form, "cleaned_data"), ("form must have a "
                "cleaned_data attribute when check_form() is called. To ensure "
                "this, check_form() should be called from the Form.clean() "
                "method")

        if self.extraction_run:
            raise RuntimeError("run_extraction() has already been called on "
                    "this FormStateExtractor instance.")

        self._extracted_params = self.extract_params(form)
        self._is_satisfied = not self._extracted_params is None
        self.extraction_run = True

    def extract_params(self, form):
        """Returns a dict containing the additional kwargs to be passed to the
        form's constructor in order for the next stage (if any) to be run. 
        """
        raise RuntimeError("Stage.extract_params(self, form) should be"
                " overriden in Stage subclass.")

    def _ensure_check_form_called(self):
        if not self.extraction_run:
            raise RuntimeError("is_satisfied() called without check_form() "
                    "being called.")

    @property
    def is_satisfied(self):
        self._ensure_check_form_called()
        return self._is_satisfied

    @property
    def extracted_params(self):
        self._ensure_check_form_called()
        return self._extracted_params

class FunctionFormStateExtractor(FormStateExtractor):
    """
    A FormStateExtractor whose constructor takes a function to be called from
    extract_params() instead of overriding extract_params() in a subclass.
    """
    def __init__(self, extractor_function):
        super(FunctionFormStateExtractor, self).__init__()
        self._extractor_function = extractor_function

    @property
    def extractor_function(self):
        return self._extractor_function

    def extract_params(self, form):
        return self.extractor_function(form)

def extractor(func):
    return FunctionFormStateExtractor(func)

class ProgressiveForm(forms.Form):
    """A class to be mixed into a django Form subclass in order to provide
    progressive loading.
    """

    def _ensure_correct_construction(self):
        if not hasattr(self, "_extractor"):
            raise RuntimeError("form has no _extractor attribute. It appears "
                    "that the form was constructed manually. ProgressiveForm "
                    "subclasses must be created via "
                    "ProgressiveForm.progressively_load()")

    def full_clean(self):
        self._was_clean_called = False
        super(ProgressiveForm, self).full_clean()

    def clean(self):
        super(ProgressiveForm, self).clean()

        self._ensure_correct_construction()
        if self._extractor is not None:
            self._extractor.run_extraction(self)
        self._was_clean_called = True
        return self.cleaned_data

    def was_clean_called(self):
        return getattr(self, "_was_clean_called")

    def is_valid(self):
        self._ensure_correct_construction()
        return (super(ProgressiveForm, self).is_valid() and
                # If we're on a stage then there must still be outstanding data
                self._extractor is None)

    @classmethod
    def state_extractors(cls):
        return []

    @classmethod
    def progressively_load(cls, *args, **kwargs):

        def new_form(extractor=None):
            form = cls(*args, **kwargs)
            form._extractor = extractor
            return form

        for extractor in cls.state_extractors():
            form = new_form(extractor=extractor)
            form.full_clean()

            if not form.was_clean_called() or not extractor.is_satisfied:
                return form
            kwargs.update(extractor.extracted_params)

        return new_form(extractor=None)


class TimetablesFormset(formsets.BaseFormSet):

    # I can't see why Django doesn't provide a method to do this, I must be 
    # missing something, but I can't see how to ignore extra forms in a 
    # formset... :/
    def _form_should_be_cared_about(self, form):
        is_valid = hasattr(form, "cleaned_data")
        not_empty = (not form.empty_permitted or
                            (form.empty_permitted and form.has_changed()))
        not_deleted = not form.cleaned_data.get("DELETE")
        return is_valid and not_empty and not_deleted

    @property
    def valid_forms(self):
        """
        Yields the formset's valid forms which are not extras or being deleted.
        """
        return [form for form in self.forms if
                self._form_should_be_cared_about(form)]
