'''
Created on Aug 20, 2012

@author: ieb
'''
from django.shortcuts import render
from timetables.administrator.views import AdminBaseView
from timetables.model.models import Classifier, Permission
from timetables.model.backend import PermissionSubject
from django.http import HttpResponseForbidden

class AddAdministrators(AdminBaseView):

    '''
    Renders the AddAdministration page in an empty state.
    Tried to use the page from Superadmin but it is too intertwined with the rest of Superadmin to be re-usable.
    '''
    def js_main_module(self):
        return "admin/administrators-add"


    def context(self, request, based_on=None):
        context = super(AddAdministrators, self).context(request, based_on=based_on)

        context["page_title"] = "Add Administrator"

        context["organisation"] = {
                    "classifications" : {
                        "subjects" : Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='S', \
                                                                             group__organisation=self.org)).order_by("value"),
                        "levels" : Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='L', \
                                                                             group__organisation=self.org)).order_by("value"),

                            }
                    }

        return context

    def get(self, request, year_start, org_code):
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE_ADMINISTRATORS, PermissionSubject(self.org)):
            return HttpResponseForbidden()
        context = self.context(request)
        return render(request, "administrator/4.1-Administrators_Add.html",
                      context)
