'''
Created on Aug 20, 2012

@author: ieb
'''
from timetables.administrator.views import AdminBaseView
from django.shortcuts import render
from timetables.model.models import EventSeries, Event, EventGroup, TermInfo,\
    Term, User
from timetables.utils.v1 import publish, generate

class ViewSeriesEventsFragment(AdminBaseView):
    '''
    Get an event series as a fragment
    '''

    def js_main_module(self):
        return None


    def context(self, request, event_series_id, based_on=None):
        
        context = super(ViewSeriesEventsFragment, self).context(request, based_on=based_on)
            
        if event_series_id == "new" or "p" in request.GET:
            if event_series_id != "new":
                try:
                    event_series = EventSeries.objects.get(id=event_series_id)
                    if event_series.date_time_pattern == request.GET['p']:
                        # this cuts the number of queries down from a few 1000 to < 20
                        context['events'] = Event.objects.filter(owning_series__id=event_series_id). \
                                select_related("owning_series","default_timeslot"). \
                                prefetch_related("owning_series__associated_staff")
                        return context
                except ValueError:
                    pass
                except EventSeries.DoesNotExist:
                    pass
            if "g" in request.GET:
                group_template = request.GET['g']
            else:
                group_template = ""
            event_series = EventSeries(date_time_pattern=request.GET['p'])
            if "term" in request.GET:
                term_name = request.GET['m']
                try:
                    term = Term.objects.get(id=request.GET['m'])
                    term_name = TermInfo.TERM_SHORT_NAMES[term.name]
                except:
                    pass
            else:
                term_name = "Mi"
            title = request.GET['t'] or ""
            location = request.GET['l'] or ""
            staff = [User(username=x) for x in (request.GET['s'] or "").split(";")]
            events = generate(event_series, group_template, term_name, self.org, title=title, location=location, staff=staff) 
            context['events'] = events
        else:
            # this cuts the number of queries down from a few 1000 to < 20
            context['events'] = Event.objects.filter(owning_series__id=event_series_id). \
                    select_related("owning_series","default_timeslot"). \
                    prefetch_related("owning_series__associated_staff")
        return context

    def get(self, request, year_start, org_code, event_series_id):
        self.locate_organisation(request, year_start, org_code)
        context = self.context(request, event_series_id)

        return render(request, "administrator/fragments/events-fragment.html",
                context)
