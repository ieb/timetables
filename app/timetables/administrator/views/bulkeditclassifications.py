'''
Created on Aug 1, 2012

@author: ieb
'''
from django.utils.decorators import method_decorator
from timetables.utils.xact import xact
from django.http import HttpResponseBadRequest, HttpResponseNotAllowed,\
    HttpResponseForbidden
from timetables.administrator.views.allevents import AllEvents
from timetables.model.models import Permission
from timetables.model.backend import PermissionSubject


class BulkEditClassifications(AllEvents):
    '''
    Save changes to Event Classifications in bulk.
    For the moment this is disabled as it makes no sense.
    '''
    
    def js_main_module(self):
        return None

    def get(self, request, year_start, org_code):
        return HttpResponseNotAllowed("POST")

    @method_decorator(xact)
    def post(self, request, year_start, org_code):
        # POST contains selected-events a comma seperated list of EventIds
        # multiple level ids
        # multiple subject ids
        # It should clear all the classifications on each event, and and set
        # new classificatiosn to match the requested for all event ids.
        # However, this is not possible since Events dont have classifications, only 
        # Event groups, so we will do it for the Event groups, but that also doesnt work 
        # Since Events are children of EventSerise and may be children of many EventSerise, so 
        # What is set ?
        
        self.locate_organisation(request, year_start, org_code)
        if request.user.has_perm(Permission.WRITE, PermissionSubject(self.org)):
            return HttpResponseBadRequest("Not implemented pending resolution of model/design problem")
        else:
            return HttpResponseForbidden()
