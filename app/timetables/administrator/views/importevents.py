'''
Created on Sep 18, 2012

@author: ieb
'''
from timetables.administrator.views import AdminBaseView, BadRequestData
from django.shortcuts import render
from timetables.model.models import Classifier, EventGroup, Term,\
    EventType, Location, EventSeries, GroupUsage, User
import csv
import tempfile
from django.db import models
import hashlib
from django.utils.timezone import now
import base64
from timetables.utils.http import Request
from timetables.utils.v1 import publish
import os
from django.core.urlresolvers import reverse
from django.utils.http import urlencode
from django.utils.decorators import method_decorator
from timetables.utils.xact import xact

    
class ImportEvents(AdminBaseView):
    '''
    Import events from CSV
    '''

    UPDATE_REQUIRED_FIELDS = (
            'id',
            'title',
            'location',
            'date pattern',
            'lecturers'
            )

    CREATE_REQUIRED_FIELDS = (
                'title',
                'lecturers',
                'date pattern',
                'location',
                'term',
                'event type',
                'default location',
                'group title',
                'default pattern',
                'group description'
                )

    def js_main_module(self):
        return "admin/events-import"
    
    
    def context(self, request, based_on=None):
        context = super(ImportEvents, self).context(request, based_on=based_on)

        context["page_title"] = "Import Events"
        context["organisation"] = {
                    "classifications" : {
                        "subjects" : Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='S', \
                                                                             group__organisation=self.org)).order_by("value"),
                        "levels" : Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='L', \
                                                                             group__organisation=self.org)).order_by("value"),

                            }
                    }
        
        if 'fb_s' in request.GET:
            context['imported_series'] = request.GET['fb_s']
        if 'fb_u' in request.GET:
            context['imported_events'] = request.GET['fb_u']
        if 'fb_d' in request.GET:
            context['deleted_events'] = request.GET['fb_d']

        return context

    def get(self, request, year_start, org_code):
        self.locate_organisation(request, year_start, org_code)
        context = self.context(request)

        return render(request, "administrator/1.3-Import-Events.html", context)
    
    
    @method_decorator(xact)
    def post(self, request, year_start, org_code):
        csvfile = tempfile.NamedTemporaryFile("w+b", delete=False)
        csvfilename = csvfile.name
        try:
            f = request.FILES['csv-event-file']
            for chunk in f.chunks():
                csvfile.write(chunk)
            csvfile.close()
            # can borrow comes after the file in the POST.
            can_borrow = ("1" == (request.POST['can_borrow'] or "1"))
            
            classifications = Classifier.objects.filter(
                                models.Q(id__in=request.POST.getlist("org_levels"),group__family='L',group__organisation=self.org)|
                                models.Q(id__in=request.POST.getlist("org_subjects"),group__family='S',group__organisation=self.org))
    
    
            csvfile = open(csvfile.name,"r+b")
            reader = csv.DictReader(csvfile)
            total_events = 0
            total_events_deleted = 0
            rownum=1
            groups = set()
            for event_series_row in reader:
                rownum = rownum + 1
                if 'id' in event_series_row and event_series_row['id'] is not None:
                    # Update the event series.

                    self._check_requried(rownum, ImportEvents.UPDATE_REQUIRED_FIELDS, event_series_row)
                    event_series = EventSeries.objects.get(id=event_series_row['id'])
                    if event_series.owning_group.id not in groups:
                        event_series.owning_group.save_version(request.user,"Imported Events")
                        groups.add(event_series.owning_group.id)
                    
                    added, deleted = self._update_series(event_series, event_series_row, can_borrow)
                    total_events = total_events + added
                    total_events_deleted = total_events_deleted + deleted
                else:
                    self._check_requried(rownum, ImportEvents.CREATE_REQUIRED_FIELDS, event_series_row)
                    # Create a new event group and series
                    try:
                        term = Term.objects.get(term_info__name=event_series_row['term'],
                                                academic_year=self.year)
                    except Term.DoesNotExist:
                        raise BadRequestData(400,"Term '%s' at row %s does not exist " % (event_series_row['term'],rownum))
                    event_type, _ = EventType.objects.get_or_create(name=event_series_row['event type'])
                    default_location, _ = Location.objects.get_or_create(name=event_series_row['default location'])
                    if 'code' not in event_series_row:
                        digest = hashlib.sha1()
                        digest.update(str(now()))
                        c = base64.urlsafe_b64encode(digest.digest())
                        event_series_row['code'] = c[:10]
                    try:
                        event_group = EventGroup.objects.get(name=event_series_row['group title'],
                                                           owning_organisation=self.org,
                                                           term=term,
                                                           code=event_series_row['code'],
                                                           is_deleted=False)
                    except EventGroup.DoesNotExist:
                        event_group = EventGroup.objects.create(
                                                               name=event_series_row['group title'],
                                                               owning_organisation=self.org,
                                                               term=term,
                                                               publication_status='U',
                                                               code=event_series_row['code'],
                                                               event_type=event_type,
                                                               default_location=default_location,
                                                               default_pattern=Request.get_default(event_series_row, 'default pattern'),
                                                               description = Request.get_default(event_series_row, 'group description'),
                                                               is_deleted=False)
                    self._sync_group_usage(event_group,classifications)
                    total_events = total_events + self._create_series(event_group, event_series_row, can_borrow)
                    
            target_url = "%s?%s" % (reverse("admin import events", args=(self.year.url_id(), str(self.org.url_id()))), 
                                    urlencode({ 'fb_u' : total_events, 
                                               'fb_s' : rownum-1, 
                                               'fb_d' : total_events_deleted }))
            return self.prepare_response(request, target_url, rownum-1)
        finally:
            try:
                csvfile.close()
            except:
                pass
            try:
                os.remove(csvfilename)
            except:
                pass
            
    def _check_requried(self, r, keys, d):
        for k in keys:
            if k not in d:
                raise BadRequestData(400, "Missing Column %s at record %s " % ( k, r))

    def _sync_group_usage(self, event_group, classifications):
        group_usage, _ = GroupUsage.objects.get_or_create(
                     organisation=self.org,
                     group=event_group
                    )
        # remove any classifiers that are no longer present
        for c in group_usage.classifiers.all():
            if c not in classifications:
                group_usage.classifiers.remove(c)

        for c in classifications:
            group_usage.classifiers.add(c)
        # save classifiers, assuming that if already present its a noop
        group_usage.save()


    def _get_location(self, d, k, default):
        '''
        Get the location specified in d[k] using default if not available or not specified
        :param d:
        :param k:
        :param default:
        '''
        try:
            if d[k] != "":
                location, _ = Location.objects.get_or_create(name=d[k])
                return location
        except:
            pass
        return default

    def _create_series(self, event_group, event_series_row, can_borrow):
        '''
        Create a new event series, with data from group and event series associating it with event_group
        and using the location if none has been specified in series
        :param group:
        :param series:
        :param event_group:
        :param location:
        '''
        series_location = self._get_location(event_series_row, 'location', event_group.default_location)
        date = Request.get_default(event_series_row, 'date pattern', event_group.default_pattern)
        if date is None or date.strip() == "":
            raise BadRequestData(code=400,message="Every series must have a date pattern or the group must have a default.")
        event_series = EventSeries.objects.create(
                    owning_group=event_group,
                    title=event_series_row['title'],
                    date_time_pattern=date,
                    can_borrow = can_borrow,
                    location=series_location)
        self._add_lecturers(event_series_row,'lecturers', event_series )
        event_series.save()
        n_intervals, n_events, n_deleted = publish(event_series)
        return n_events

    
    def _update_series(self, event_series, event_series_row, can_borrow):
        '''
        Update the event_series which is a seriese of event_group with data from series
        :param group:
        :param series:
        :param event_group:
        :param event_series:
        :param location:
        '''
        # This is an update to an existing event series
        event_group = event_series.owning_group
        series_location = self._get_location(event_series_row, 'location', event_group.default_location)
        date = Request.get_default(event_series_row, 'date pattern', event_group.default_pattern)
        self._add_lecturers(event_series_row,'lecturers', event_series )
        
        # Set the properties.
        event_series.title=event_series_row['title']
        event_series.can_borrow = can_borrow
        event_series.date_time_pattern=date
        event_series.location=series_location
        event_series.save()
        n_intervals, n_events, n_deleted = publish(event_series)
        return n_events, n_deleted


    def _add_lecturers(self, d, k, event_series):
        '''
        Add lecturers to event_series from d[k] which is a , seperate list.
        :param d:
        :param k:
        :param event_series:
        '''
        if k not in d:
            return
        lecturers = d[k].split(',')
        for lec in lecturers:
            u, _ = User.objects.get_or_create(username=lec[:29])
            event_series.associated_staff.add(u)
