'''
Created on Aug 7, 2012

@author: ieb
'''
from timetables.administrator.views import AdminBaseView
from django.shortcuts import render
from timetables.model.models import Classifier, EventType, Term, EventGroup,\
    Revision, ClassifierGroup, Permission
from timetables.administrator.views.saveevent import SaveEvent
from django.conf import settings
from django.http import Http404, HttpResponseForbidden

import logging
from django.utils.timezone import now
from timetables.model.backend import PermissionSubject
log = logging.getLogger(__name__)
del(logging)

try:
    TEST_DEFAULT_DATA = settings.TEST_DEFAULT_DATA
except:
    TEST_DEFAULT_DATA = False

class EventBaseView(AdminBaseView):

    def load_default_data(self, context):
        '''
        Loads default data into the context, wihch may be QA/TEST data
        :param context:
        '''
        if TEST_DEFAULT_DATA:
            for v in SaveEvent.GROUP_FORM_NAMES:
                v = v.replace("-","_")
                context["default_group_%s" % (v)] = "%s %s" %(v,now())
            for v in SaveEvent.SERIES_FORM_NAMES:
                v = v.replace("-","_")
                context["default_series_%s" % (v)] = "%s %s" %(v,now())
            for v in SaveEvent.EVENT_FORM_NAMES:
                v = v.replace("-","_")
                context["default_event_%s" % (v)] = "%s %s" %(v,now())
        else:
            for v in SaveEvent.GROUP_FORM_NAMES:
                v = v.replace("-","_")
                context["default_group_%s" % (v)] = ""
            for v in SaveEvent.SERIES_FORM_NAMES:
                v = v.replace("-","_")
                context["default_series_%s" % (v)] = ""
            for v in SaveEvent.EVENT_FORM_NAMES:
                v = v.replace("-","_")
                context["default_event_%s" % (v)] = ""

    def context(self, request, based_on=None):
        context = super(EventBaseView, self).context(request, based_on=based_on)
        self.load_default_data(context)
        return context



class AddEvents(EventBaseView):
    '''
    Renders the AddEvents page in an empty state with 1 serise waiting to be filled.
    '''

    def js_main_module(self):
        return "admin/events-add"

    def context(self, request, based_on=None):
        context = super(AddEvents, self).context(request, based_on=based_on)

        context["page_title"] = "Add Events"
        
        if 'tab' in request.GET and request.GET['tab'] == 'url':
            context['external_url_tab'] = True
        else:
            context['events_tab'] = True


        context["organisation"] = {
                    "classifications" : {
                        "subjects" : Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='S', \
                                                                             group__organisation=self.org)).order_by("value"),
                        "levels" : Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='L', \
                                                                             group__organisation=self.org)).order_by("value"),

                            },
                    "types" : EventType.objects.all().order_by("name"),
                    "terms" : Term.objects.filter(term_info__day=False,
                                                  academic_year=self.year,
                                                  term_info__term_type='S').order_by("term_info__name").select_related("terminfo"),
                    }
        context["subject_name"] = ClassifierGroup.objects.get(organisation_id=self.org,family='S').name

        # the ID of this new group
        context["group_id"] = "n"
        context["new_group"] = True
        # the ID of the embedded series
        context["series_add_id"] = "n_0"
        context["new_series"] = True


        return context


    def get(self, request, year_start, org_code):
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE, PermissionSubject(self.org)):
            return HttpResponseForbidden()
        context = self.context(request)
        return render(request, "administrator/1.1-Add-New-Events.html", context)

class EditEvents(EventBaseView):
    '''
    Renders the EditEvents page With a single group
    We could make this extend AddEvents
    '''

    def js_main_module(self):
        return "admin/events-edit"


    def context(self, request, event_group_id, based_on=None):
        context = super(EditEvents, self).context(request, based_on=based_on)

        context["page_title"] = "Edit Events"


        context["organisation"] = {
                    "classifications" : {
                        "subjects" : Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='S', \
                                                                             group__organisation=self.org)).order_by("value"),
                        "levels" : Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='L', \
                                                                             group__organisation=self.org)).order_by("value"),

                            },
                    "types" : EventType.objects.all().order_by("name"),
                    "terms" : Term.objects.filter(term_info__day=False,
                                                  academic_year=self.year,
                                                  term_info__term_type='S').order_by("term_info__name").select_related("terminfo"),
                    }

        context["subject_name"] = ClassifierGroup.objects.get(organisation_id=self.org,family='S').name

        try:
            event_group = EventGroup.objects.get(id=event_group_id,is_deleted=False)
        except EventGroup.DoesNotExist:
            raise Http404()
        if event_group.event_type.name == "--external--":
            context["external_url"] = event_group.owned_series.all()[0].external_url

        context['subjects_selected'] = Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='S', \
                                                                             group__organisation=self.org,
                                                                             groupusage__group=event_group)).order_by("value")
        context['levels_selected'] = Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='L', \
                                                                             group__organisation=self.org,
                                                                             groupusage__group=event_group)).order_by("value")
        log.error("Subjects Seletected  %s " % context['subjects_selected'] )
        log.error("Levels Seletected  %s " % context['levels_selected'] )
        # save the group.
        context['group'] = event_group
        # the ID of this new group
        context["group_id"] = event_group.id
        context["new_group"] = False
        # the ID of the embedded series
        context["series_add_id"] = "n_0"
        context["new_series"] = True
        context['recent_changes'] = Revision.objects.filter(grouprevision__group_version__current_version=event_group).order_by("-id")


        return context


    def get(self, request, year_start, org_code, event_group_id):
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE, PermissionSubject(self.org)):
            return HttpResponseForbidden()
        context = self.context(request, event_group_id)
        return render(request, "administrator/1.2-Edit-Events.html", context)

    
class AddEventGroupFragment(EventBaseView):
    '''
    Renders a AddEventGroup Fragment with 1 Event Series waiting to be filled. x
    '''
    
    def js_main_module(self):
        return None

    def context(self, request, group_id, based_on=None):
        context = super(AddEventGroupFragment, self).context(request, based_on=based_on)

        context["page_title"] = "Add Events"


        context["organisation"] = {
                    "types" : EventType.objects.all().order_by("name"),
                    "terms" : Term.objects.filter(term_info__day=False,
                                                  academic_year=self.year,
                                                  term_info__term_type='S').order_by("term_info__name").select_related("terminfo"),
                    }

        context["group_id"] = group_id
        context["new_group"] = True
        context["series_add_id"] = "%s_0" % group_id
        context["new_series"] = True


        return context


    def get(self, request, year_start, org_code, group_id):
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE, PermissionSubject(self.org)):
            return HttpResponseForbidden()
        context = self.context(request, group_id)
        return render(request, "administrator/fragments/editor-group.html", context)

class EditEventGroupFragment(AddEventGroupFragment):
    pass

class AddEventSeriesFragment(EventBaseView):
    '''
    Generate a add event serise fragment binding it to the group specified in the query parameter gid and the
    serise specified in the parameter sid
    '''
    
    def js_main_module(self):
        return "admin/events-add"

    def context(self, request, group_id, series_id, based_on=None):
        context = super(AddEventSeriesFragment, self).context(request, based_on=based_on)


        context["group_id"] = group_id
        context["series_add_id"] = "%s_%s" % (group_id, series_id)
        context["new_series"] = True


        return context

    def get(self, request, year_start, org_code, group_id, serise_id):
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE, PermissionSubject(self.org)):
            return HttpResponseForbidden()
        context = self.context(request,  group_id, serise_id)
        return render(request, "administrator/fragments/editor-add-series.html", context)


class EditEventSeriesFragment(AddEventSeriesFragment):
    pass
