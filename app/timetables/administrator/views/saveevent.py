'''
Created on Aug 8, 2012

@author: ieb
'''
from timetables.administrator.views import AdminBaseView, BadRequestData
from django.utils.decorators import method_decorator
from timetables.utils.xact import xact
from django.http import HttpResponseForbidden
from timetables.model.models import Event, User, Location, EventGroup,\
    Classifier, EventType, Term, GroupUsage, EventSeries, TermInfo, Permission
from django.utils import simplejson as json
from timetables.utils.http import Request
import hashlib
import base64
from timetables.utils.v1 import publish

import logging
import traceback
from django.utils.timezone import now
from django.core.urlresolvers import reverse
from django.db import models
from timetables.model.backend import PermissionSubject
from django.utils.http import urlencode
log = logging.getLogger(__name__)
del logging

class SaveEvent(AdminBaseView):
    '''
    Save events.
    '''

    GROUP_FORM_NAMES = (
                "deleted",
                "new",
                "title",
                "desc",
                "code",
                "type",
                "term",
                "default-location",
                "default-date-time-pattern",
                )

    SERIES_FORM_NAMES = (
                "series_deleted",
                "series_new",
                "series_title",
                "series_lecturer",
                "series_location",
                "series_date",
                )
    EVENT_FORM_NAMES = (
                "event-new",
                "event-title",
                "event-lecturer",
                "event-location",
                "event-date",
                )




    def js_main_module(self):
        return None


    def _get_location(self, d, k, default):
        '''
        Get the location specified in d[k] using default if not available or not specified
        :param d:
        :param k:
        :param default:
        '''
        try:
            if d[k] != "":
                location, _ = Location.objects.get_or_create(name=d[k])
                return location
        except:
            pass
        return default

    def _add_lecturers(self, d, k, event_series):
        '''
        Add lecturers to event_series from d[k] which is a , seperate list.
        :param d:
        :param k:
        :param event_series:
        '''
        if k not in d:
            return
        lecturers = d[k].split(',')
        for lec in lecturers:
            log.error("Adding user %s " % lec)
            u, _ = User.objects.get_or_create(username=lec[:29])
            event_series.associated_staff.add(u)

    def _create_series(self, group, series, event_group, location, can_borrow):
        '''
        Create a new event series, with data from group and event series associating it with event_group
        and using the location if none has been specified in series
        :param group:
        :param series:
        :param event_group:
        :param location:
        '''
        series_location = self._get_location(series, 'series_location', location)
        date = Request.get_default(series, 'series_date', Request.get_default(group,'default-date-time-pattern'))
        if date is None or date.strip() == "":
            raise BadRequestData(code=400,message="Every series must have a date pattern or the group must have a default.")
        event_series = EventSeries.objects.create(
                    owning_group=event_group,
                    title=series['series_title'],
                    date_time_pattern=date,
                    can_borrow=can_borrow,
                    location=series_location)
        self._add_lecturers(series,'series_lecturer', event_series )
        event_series.save()
        n_intervals, n_events, n_deleted = publish(event_series)
        log.error("Saved Event Series %s New TimeIntervals:%s New Events:%s " % (event_series.id, n_intervals, n_events))
        return n_events


    def _update_series(self, group, series, event_group, event_series, location, can_borrow):
        '''
        Update the event_series which is a seriese of event_group with data from series
        :param group:
        :param series:
        :param event_group:
        :param event_series:
        :param location:
        '''
        # This is an update to an existing event series
        series_location = self._get_location(series, 'series_location', location)
        date = Request.get_default(series, 'series_date', Request.get_default(group,'default-date-time-pattern'))
        self._add_lecturers(series,'series_lecturer', event_series )
        
        # Set the properties.
        event_series.title=series['series_title']
        event_series.date_time_pattern=date
        event_series.location=series_location
        event_series.can_borrow = can_borrow
        event_series.save()
        n_intervals, n_events, n_deleted = publish(event_series)
        log.error("Updated Event Series %s New TimeIntervals:%s New Events:%s " % (event_series.id, n_intervals, n_events))
        return n_events, n_deleted

    def _sync_group_usage(self, event_group, classifications):
        group_usage, _ = GroupUsage.objects.get_or_create(
                     organisation=self.org,
                     group=event_group
                    )
        # remove any classifiers that are no longer present
        for c in group_usage.classifiers.all():
            if c not in classifications:
                group_usage.classifiers.remove(c)

        for c in classifications:
            group_usage.classifiers.add(c)
        # save classifiers, assuming that if already present its a noop
        group_usage.save()



    @method_decorator(xact)
    def post(self, request, year_start, org_code):
        '''
        # The post either contains multiple event groups containing event series.
        # if group_type is events, then its a set of event groups.
        # if group_type is external_url then its one external url.
        # Post contains multiple groups containing multiple serise
        # The root of the tree of values is group which contains a list of group ids
        # all the values associated with each group take the form <form_param_name>_<groupid>
        # each group has a series. The series IDs are stored in series_<groupid>
        # all values assocaited with a series in a group take the form <form_param_name>_<seriesid>
        # each seriesID is unique over all groups.
        
        :param request:
        :param year_start:
        :param org_code:
        '''
        target_url = None
        total_events = 0
        total_events_deleted = 0
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE, PermissionSubject(self.org)):
            return HttpResponseForbidden()

        type = Request.get_default(request.POST, "group_type", "events")
        # can borrow comes after the file in the POST.
        can_borrow = ("1" == (Request.get_default(request.POST, "can_borrow", "1")))
        log.error("Subjects is %s " % request.POST.getlist("org_subjects"))
        log.error("Levels is %s " % request.POST.getlist("org_levels"))
        
        classifications = Classifier.objects.filter(
                                    models.Q(id__in=request.POST.getlist("org_levels"),group__family='L',group__organisation=self.org)|
                                    models.Q(id__in=request.POST.getlist("org_subjects"),group__family='S',group__organisation=self.org))
        if type == "external_url":

            group_id = Request.get_default(request.POST,"external_url_group","")
            external_url = Request.get_default(request.POST,"external_url","")
            external_url_desc = Request.get_default(request.POST, "external_url_desc", "")
            digest = hashlib.sha1()
            digest.update(str(now()))
            c = base64.urlsafe_b64encode(digest.digest())
            log.error("Code is %s " % c)
            code = c[:10]
            # The model is not totally suitable for this usage, but it will have to do.
            # We can't use Classifier because its not versioned and the UI requires versioning.
            # We will also need these objects to behave just like event groups from a search point of view
            # Hence (without rewriting the classifier section of the schema) creating an Event Group with a single
            # Event Series that point contains the external url is the way to go. We will have to allow nulls on
            # some of the fields to make this possible.
            if Request.check(request.POST, "external_url_new"):
                external_location, _ = Location.objects.get_or_create(name="--external--")
                external_event_type, _ = EventType.objects.get_or_create(name="--external--")
                try:
                    external_term = Term.objects.get(academic_year=self.year,term_info__name="all")
                except Term.DoesNotExist:
                    start = None
                    end = None
                    for t in Term.objects.filter(academic_year=self.year):
                        if start is None or start < t.start:
                            start = t.start
                        if end is None or end > t.end:
                            end = t.end
                    externa_term_info, _ = TermInfo.objects.get_or_create(name="all", term_type=TermInfo.OTHER_TERM)
                    external_term, _ = Term.objects.get_or_create(term_info=externa_term_info,academic_year=self.year, start=start, end=end)
                event_group = EventGroup.objects.create(
                            name="--external--",
                            publication_status='U',
                            owning_organisation=self.org,
                            code=code,
                            default_pattern="",
                            term=external_term,
                            default_location=external_location,
                            event_type=external_event_type,
                            is_deleted=False,
                            description=external_url_desc)
                self._sync_group_usage(event_group, classifications)
                event_series = EventSeries.objects.create(
                    title="--external--",
                    date_time_pattern="",
                    location=external_location,
                    owning_group=event_group,
                    can_borrow=False,
                    external_url=external_url)
                target_url = reverse("admin events", args=(self.year.url_id(), str(self.org.url_id())))

            elif Request.check(request.POST, "external_url_deleted"):
                event_group = EventGroup.objects.get(id=group_id, owning_organisation=self.org)
                # Save the version before changing it.
                event_group.save_version(request.user, request.POST['comment'])

                event_group.is_deleted=True
                event_group.save()
                EventSeries.objects.filter(owning_group__owning_organisation=self.org,owning_group=group_id).update(is_deleted=True)
                # Delete any Events just in case.
                Event.objects.filter(owning_series__owning_group__owning_organisation=self.org,owning_series__owning_group=group_id).delete()
                log.error("Deleted Event Group ID %s " % (group_id))
            else:
                
                event_group = EventGroup.objects.get(id=group_id, owning_organisation=self.org)
                event_group.save_version(request.user, request.POST['comment'])
                event_group.update(description=external_url_desc)
                EventSeries.objects.get(owning_group=event_group).update(external_url=external_url)
        else: # event groups
            log.error(json.dumps(request.POST,indent=4))
            groups = Request.build_tree(request,'group', 'group', ('series','event'),
                                { 'group': SaveEvent.GROUP_FORM_NAMES,
                                   'series': SaveEvent.SERIES_FORM_NAMES,
                                   'event': SaveEvent.EVENT_FORM_NAMES})
            log.error(json.dumps(groups,indent=4))
            self.locate_organisation(request, year_start, org_code)

            for group_id,group in groups.iteritems():
                if Request.check(group,'new'):
                    if Request.check(group,'delete'):
                        continue
                    if 'code' not in group or group['code'] is None:
                        digest = hashlib.sha1()
                        digest.update(str(now()))
                        c = base64.urlsafe_b64encode(digest.digest())
                        log.error("Code is %s " % c)
                        group['code'] = c[:10]
                    location, _ = Location.objects.get_or_create(name=group['default-location'])
                    event_group = EventGroup.objects.create(
                            name=group['title'],
                            publication_status='U',
                            owning_organisation=self.org,
                            code=group['code'],
                            term=Term.objects.get(id=group['term'],academic_year=self.year),
                            default_pattern=group['default-date-time-pattern'],
                            default_location=location,
                            event_type=EventType.objects.get(id=group['type']),
                            is_deleted=False,
                            description=group['desc'])
                    self._sync_group_usage(event_group,classifications)
                    # We can assume all series are new because the group is new
                    for series_id, series in group['series_'].iteritems():
                        if Request.check(series,'series_new') and not Request.check(series,'series_deleted'):
                            total_events = total_events + self._create_series(group, series, event_group, location, can_borrow)

                    log.error("Created Event Group ID %s " % (event_group.id))
                elif Request.check(group,'deleted'):
                    if not Request.check(group,'new'):
                        event_group = EventGroup.objects.get(id=group_id, owning_organisation=self.org)
                        # Save the version before changing it.
                        event_group.save_version(request.user, request.POST['comment'])

                        event_group.is_deleted=True
                        event_group.save()
                        EventSeries.objects.filter(owning_group__owning_organisation=self.org,owning_group=group_id).update(is_deleted=True)
                        total_events_deleted = Event.objects.filter(owning_series__owning_group__owning_organisation=self.org,owning_series__owning_group=group_id).delete()
                        log.error("Deleted Event Group ID %s " % (group_id))
                        
                else:
                    # Group is old , update it
                    location, _ = Location.objects.get_or_create(name=group['default-location'])
                    event_group = EventGroup.objects.get(id=group_id, owning_organisation=self.org)

                    # Save the version before changing it.
                    event_group.save_version(request.user, request.POST['comment'])

                    event_group.name = group['title']
                    event_group.default_pattern=group['default-date-time-pattern']
                    event_group.default_location=location
                    event_group.event_type=EventType.objects.get(id=group['type'])
                    event_group.description = group['desc']

                    self._sync_group_usage(event_group, classifications)

                    toremove = []
                    # update all event series recording which ones are to be removed
                    for es in event_group.owned_series.all():
                        try:
                            series = group["series_"]["%s" % es.id]
                        except KeyError:
                            log.error(traceback.format_exc())
                            toremove.append(es.id)
                            continue
                        if Request.check(series,'series_deleted'):
                            toremove.append(es.id)
                            continue
                        if Request.check(series,'series_new'):
                            continue
                        added, deleted = self._update_series(group, series, event_group, es, location, can_borrow)
                        total_events = total_events + added
                        total_events_deleted = total_events_deleted + deleted

                    # Remove everything that was deleted.
                    if len(toremove) > 0:
                        # Event.objects.filter(owning_series__in=toremove).delete()
                        # EventSeries.objects.filter(id__in=toremove).delete()
                        log.error("Deleted Event Series %s " % (toremove))

                    # Create any new event series records
                    for series_id, series in group['series_'].iteritems():
                        if Request.check(series,'series_new') and not Request.check(series,'series_deleted'):
                            added = self._create_series(group, series, event_group, location, can_borrow)
                            total_events = total_events + added

                    event_group.save()
                    log.error("Updated Event Group ID %s " % (event_group.id))


        target_url = "%s?%s" % (reverse("admin events", args=(self.year.url_id(), str(self.org.url_id()) )), 
                                urlencode({ "s": "lastupdate", 
                                            "page" :"1", 
                                            "o" : "d", 
                                            "fb_u"  : total_events,
                                            "fb_d" : total_events_deleted }))
        return self.prepare_response(request, target_url=target_url, updated=total_events)

