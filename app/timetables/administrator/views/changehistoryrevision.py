'''
Created on Aug 16, 2012

@author: ieb
'''
from timetables.administrator.views import AdminBaseView
from timetables.model.models import Revision, EventGroupVersion
from django.shortcuts import render


class ChangeHistoryRevision(AdminBaseView):
    '''
    Show a single revisiong in the change history.
    '''

    def js_main_module(self):
        return "admin/changehistory-revision"
    
    
    def context(self, request, revision_id, based_on=None):
        '''
        Configure the context for viewing a single revision.
        :param request:
        :param revision_id:
        :param based_on:
        '''
        context = super(ChangeHistoryRevision, self).context(request, based_on=based_on)

        context["page_title"] = "Revision %s" % revision_id


        context['revision'] = Revision.objects.get(id=revision_id,organisation=self.org)
        context['group_versions'] = EventGroupVersion.objects.filter(revision__id=revision_id)


        return context

    def get(self, request, year_start, org_code, revision_id):
        self.locate_organisation(request, year_start, org_code)
        context = self.context(request, revision_id)

        return render(request, "administrator/3.1-Revision.html", context)
