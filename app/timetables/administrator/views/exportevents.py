'''
Created on Aug 1, 2012

@author: ieb
'''
from django.http import HttpResponseBadRequest, HttpResponse
from timetables.model.models import Event
from icalendar import Calendar, Event as iCalEvent
from timetables.administrator.views.allevents import AllEvents
from django.utils.decorators import method_decorator
from django.views.decorators.http import condition
import StringIO
import csv

import logging
log = logging.getLogger(__name__)
del logging

# If GZip Middleware or conditional get middleware is used, this might not work.
class CsvExporter(object):
    '''
    Export data in CSV form.
    '''
    def export(self, view, events):
        def generate():
            csvfile = StringIO.StringIO()
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow([
                    "id",
                    "Title",
                    "Group Title",
                    "Lecturer(s)",
                    "Location",
                    "Timetable",
                    "Start",
                    "End",
                    "Last update"
                    ])
            yield csvfile.getvalue()
            for e in events:
                csvfile = StringIO.StringIO()
                csvwriter = csv.writer(csvfile)
                date = view.get_date(e)
                csvwriter.writerow([
                        e.id,
                        view.get_title(e),
                        view.get_owning_group(e),
                        ",".join(view.get_lecturers(e)),
                        view.get_location(e),
                        date.start_time,
                        date.end_time,
                        view.get_last_update(e)
                        ])
                yield csvfile.getvalue()
           
        response = HttpResponse(generate(),content_type="text/csv; charset=utf-8")
        response['Content-Disposition'] = "attachment; filename=events.csv"
        response.streaming = True
        return response
 
class ICalExporter(object):
    '''
    An iCal Exporter.
    '''
    def export(self, view, events):
        def generate():
            yield "BEGIN:VCALENDAR\r\n"\
                "PRODID:-//University of Cambridge Timetables//timetables.caret.cam.ac.uk//\r\n"\
                "VERSION:2.0\r\n"
            for e in events:
                date = view.get_date(e)
                event = iCalEvent()
                event.add('summary', '%s %s' % (view.get_owning_group(e), view.get_title(e)))
                event.add('dtstart', date.start_time)
                event.add('dtend', date.end_time)
                event.add('last-mod', view.get_last_update(e))
                event.add('location', view.get_location(e))
                for l in view.get_lecturers(e):
                    event.add('attendee',l)
                event['uid'] = '%s@timetables.caret.cam.ac.uk' % e.id
                event.add('priority', 5)
                yield event.as_string()
            yield "END:VCALENDAR\r\n"
        response = HttpResponse(generate(),content_type="text/calendar; charset=utf-8")
        response['Content-Disposition'] = "attachment; filename=events.ics"
        response.streaming = True
        return response

class ExportEvents(AllEvents):
    '''
    Export all events in either csv or ical form.
    '''
    
    EXPORTERS = {
         'csv' : CsvExporter(),
         'ical' : ICalExporter()
            }
    def js_main_module(self):
        return None

    @method_decorator(condition(etag_func=None))
    def get(self, request, year_start, org_code):
        # GET contains selected-events a comma separated list of EventIds
        # fmt is the format
        
        self.locate_organisation(request, year_start, org_code)
        
        if request.GET['fmt'] in ExportEvents.EXPORTERS:
            if "all-events" in request.GET and request.GET['all-events'] == "1":
                events, _ = self.list_events(request,with_sort=False)
            else:
                events = Event.objects.filter(id__in=request.GET['selected-events'].split(","),
                                          owning_series__owning_group__owning_organisation=self.org)
            return ExportEvents.EXPORTERS[request.GET['fmt']].export(self, events)
        return HttpResponseBadRequest("Not implemented pending resolution of model/design problem")
    
    
