from django.shortcuts import render, redirect, get_object_or_404
from timetables.views import BaseView
from timetables.administrator import testdata
from timetables.model import models
from timetables.utils import NavigationTree, TimetablesPaginator
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, user_passes_test
from timetables.model.models import User, Organisation, EventGroup, GroupUsage,\
    Permission
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponseRedirect, HttpResponseBadRequest,\
    HttpResponse

import logging
import traceback
import json
from timetables.utils.xact import xact
from timetables.utils.http import HttpParameters
from timetables.model.backend import PermissionSubject
from django.contrib.auth import get_backends
from timetables.utils.Json import JSON_INDENT, JSON_CONTENT_TYPE
log = logging.getLogger(__name__)
del logging


navtree = NavigationTree.from_json(
    { "name": "Overview", "view_name": "admin overview", "children":[
        { "name": "Event Timetables", "view_name": "admin events",
            "children": [
                { "name":  "Add New Events", "view_name": "admin add events", "requires" : Permission.WRITE },
                { "name":  "Import Events", "view_name": "admin import events", "requires" : Permission.WRITE }
            ]
        },
        { "name": "Classifications", "view_name": "admin classifications",
            "children": [
                { "name": "Add New Classification",
                        "view_name": "admin add classifications", "requires" : Permission.WRITE_CLASSIFICATIONS }
            ]
        },
        { "name": "Change History", "view_name": "admin change history" },
        { "name": "Administrators", "view_name": "admin administrators",
            "children": [
                { "name": "Add New Administrator",
                        "view_name": "admin add administrator", "requires" : Permission.WRITE_ADMINISTRATORS }
            ]
        }
    ]}
)





def check_using_method(function=None):
    '''
    This decoration is a class method decoration, not a function decoration.
    It needs the instance to work, which is why the standard @method_decorator can't
    be used.
    It manually chains to the user_passes_test decoration usign a dummy function and if thats successful
    it invokes the wrapped function.
    Its ugly, but the only way of doing this without reproducing the user_passes_test functionality.
    '''
    def decorator(instance, *args, **kwargs):
        def check(user):
            '''
            This gets access to the instance due to scope
            :param user:
            '''
            return instance.check_user(*args, **kwargs)
        def dummy(*args, **kwargs):
            '''
            This is just used to give the user_passes_test annotation something to call.
            '''
        user_passes_test(check)(dummy)(*args, **kwargs)
        return function(instance, *args, **kwargs)
    return decorator

class BadRequestData(Exception):
    '''
    This is used by views to indicate a problem with the request data that needs to result in something at the client.
    In general this should never happen as the form should pre-validate everything or respond with a page that tells the user what to
    do. If this is raised inside a xact wrapper, the transaction will be rolled back ensuring that the DB remains consistent.
    '''
    def __init__(self, code, message):
        if ( code < 400 ):
            log.error("Bad Request Data should not be used with status codes < 400 please correct %s " % traceback.format_exc(2))
        self.code = code
        self.message = message

class AdminBaseView(BaseView):
    "The base class of all Administration views."

    def __init__(self):
        super(AdminBaseView, self).__init__(navtree)

    @method_decorator(login_required)
    @check_using_method
    def dispatch(self, *args, **kwargs):
        try:
            return super(AdminBaseView, self).dispatch(*args, **kwargs)
        except BadRequestData as e:
            return HttpResponse(content=e.message, status=e.code)


    def check_user(self, *args, **kwargs):
        '''
        Check that the user has admin access for the operation. If access is
        denied a PermissionDenied exception will be raised. This method covers
        the basic case, if something more sophisticated is required the individual
        class should be used.

        NB: The signature on this method must match all the method signatures, and all the
        method signatures must be identical.
        :param request:
        :param year_start:
        :param org_code:
        '''
        # Expected to see request, year_start, org_code as formal parameters from the url
        assert len(args) > 2

        request = args[0]
        year_start = args[1]
        org_code = args[2]
        if not hasattr(self, "year") or not hasattr(self, "year"):
            self.locate_organisation(request, year_start, org_code)

        # This will use the authentication backend and cache the permissions set.
        perm = Permission.READ
        if not request.user.has_perm(perm, PermissionSubject(self.org)):
            raise PermissionDenied
        return True

    def navigation(self):
        pass

    def locate_organisation(self, request, year_start, org_code):
        """Gets a tuple of (AcademicYear, Organisation) for the provided args. 
        
        Raises:
            django.http.Http404: If no organisation exists for the provided year
                and code.
        """
        if hasattr(self, "year") and hasattr(self, "org"):
            # already done by the permissions check, no point in doing this twice.
            # If you want to do it twice del year and org first.
            return
        try:
            org = Organisation.objects.organisations_for_user(request.user).select_related("year").get(code=org_code, year__starting_year=year_start)
        except Organisation.DoesNotExist:
            raise Http404("Administrative access to the organisation you entered does not exist for you. That does not mean the organisation exists.")
        self.year = org.year
        self.org = org

    def context(self, request, based_on={}):
        '''
        Build context for the page.
        :param request: the current request
        :param based_on: a dict that the request is based on. This is only normally used in the early stages of development.
        '''
        if "_testdata" in request.REQUEST:
            context = (based_on or {}).copy()
        else:
            context = {}
            
            
        perms = request.user.get_all_permissions(PermissionSubject(self.org))
        self.navtree.apply_permissions(perms)
        context.update(super(AdminBaseView, self).context(request))

        if User.is_superadmin(request.user):
            context["can_write"] = True
            context["can_classify"] = True
            context["can_publish"] = True
            context["can_admin"] = True
        else:
            context["can_write"] = Permission.WRITE in perms
            context["can_classify"] = Permission.WRITE_CLASSIFICATIONS in perms
            context["can_publish"] = Permission.PUBLISH in perms
            context["can_admin"] = Permission.WRITE_ADMINISTRATORS in perms
        try:
            if self.year and self.org:
                context["page_year"] = self.year
                context["page_organisation"] = self.org
                context["years"] = models.AcademicYear.objects.active_years(user=request.user)
                context["organisations"] = Organisation.objects.organisations_for_user(request.user).filter(year=self.year).order_by()
            elif self.year:
                context["page_year"] = self.year
                context["years"] = models.AcademicYear.objects.active_years(user=request.user)
                context["organisations"] =  Organisation.objects.organisations_for_user(request.user).filter(year=self.year).order_by()
            elif self.org:
                context["page_year"] = self.org.year_set[0]
                context["page_organisation"] = self.org
                context["years"] = models.AcademicYear.objects.active_years(user=request.user)
                context["organisations"] =  Organisation.objects.organisations_for_user(request.user).filter(year=self.org.year_set[0]).order_by()
        except:
            log.error(traceback.format_exc())

        #context['recent_changes'] = {}
        #context['page_title'] = ""

        log.debug("Test Data is %s " % json.dumps(based_on, indent=2))
        log.debug("Live Data is %s " % context)

        return context
    
    
    def prepare_response(self,request, target_url=None, updated=0):
        '''
        Create a response from the admin views noting if the request was an Ajax request or a browser request.
        Ajax requests generate json responses. Browser request generate redirects.
        :param request:
        :param target_url: target of the redirect.
        :param updated: number of items updated
        '''
        if request.is_ajax():
            # Send a json response
            return HttpResponse(json.dumps({
                                            'response' : 'Ok',
                                            'updated' : updated
                                            }, indent=JSON_INDENT),content_type=JSON_CONTENT_TYPE )
        elif target_url is not None:
            return HttpResponseRedirect(target_url)
        else:
            # go back to where we came from
            return HttpResponseRedirect(request.META["HTTP_REFERER"])

class Home(AdminBaseView):
    """Responsible for redirecting users to an actual organisation home when
    they enter a partial URL such as:
    a: /administration/
    b: /administration/2012/
    
    Path a redirects to the first organisation in the current year, b redirects
    to the first organisation in the specified year.
    """

    def default_organisation(self, user, year_start=None):
        """Gets the default year and organisation for the current administrator, taking into 
        account the year requested and the situation where the user is a superadmin.
        """
        if hasattr(self, "org"):
            return
        org = None
        if year_start is not None:
            try:
                org =  Organisation.objects.organisations_for_user(user).filter(year__starting_year=year_start).order_by("name")[0]
            except:
                log.error(traceback.format_exc())
                pass
        if org is None:
            try:
                org = Organisation.objects.organisations_for_user(user).order_by("year", "name")[0]
            except:
                log.error(traceback.format_exc())
                raise Http404("You don't have permission to administer any organisations.")
        self.org = org
        self.year = org.year

    def get(self, request, year=None):
        self.default_organisation(request.user, year_start=year)
        return redirect("admin overview", str(self.org.year.starting_year), self.org.code)

    def check_user(self, request, year=None):
        self.default_organisation(request.user, year)
        if User.is_superadmin(request.user) or \
                User.is_admin(request.user, self.org.year, self.org):
            return True
        raise PermissionDenied





