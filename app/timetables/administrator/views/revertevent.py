'''
Created on Sep 10, 2012

@author: ieb
'''
from django.http import HttpResponseForbidden, HttpResponseBadRequest, HttpResponseNotFound
from django.utils.decorators import method_decorator
from timetables.administrator.views import AdminBaseView
from timetables.model.backend import PermissionSubject
from timetables.model.models import Permission, \
    EventGroupVersion
from timetables.utils.v1 import publish
from timetables.utils.xact import xact
import logging
from django.core.urlresolvers import reverse

log = logging.getLogger(__name__)
del logging

class RevertEvent(AdminBaseView):
    '''
    Revert an event.
    '''



    def js_main_module(self):
        return None




    @method_decorator(xact)
    def post(self, request, year_start, org_code):
        # the post contains a event group version id to revert
        # the 
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE, PermissionSubject(self.org)):
            return HttpResponseForbidden()
        

        event_group_version_id = request.POST['versionid'] or None
        if event_group_version_id is None:
            return HttpResponseBadRequest()

        try:
            event_group_version = EventGroupVersion.objects.get(id=int(event_group_version_id), owning_organisation=self.org)
            event_group_version.revert(request.user)
            event_group = event_group_version.current_version
            for event_series in event_group.owned_series.all():
                n_intervals, n_events, n_deleted = publish(event_series)
                log.error("Updated Event Series %s New TimeIntervals:%s New Events:%s " % (event_series.id, n_intervals, n_events))

        except EventGroupVersion.DoesNotExist:
            return HttpResponseNotFound()
        


        target_url = reverse("admin change history", args=(self.year.url_id(), str(self.org.url_id())))
        
        
        return self.prepare_response(request, target_url=target_url)

