'''
Created on Aug 8, 2012

@author: ieb
'''
from timetables.administrator.views import AdminBaseView
from django.utils.decorators import method_decorator
from timetables.utils.xact import xact
from django.http import HttpResponseBadRequest, HttpResponseNotFound,\
    HttpResponseForbidden
from timetables.model.models import Classifier, User, Permission,\
    OrganisationRole
from timetables.utils.http import Request
from timetables.model.backend import PermissionSubject
from django.core.urlresolvers import reverse

class SaveAdministrators(AdminBaseView):
    '''
    Save administrators after an update.
    '''


    

    def js_main_module(self):
        return None




    # TODO add permissions checks
    @method_decorator(xact)
    def post(self, request, year_start, org_code):
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE_ADMINISTRATORS, PermissionSubject(self.org)):
            return HttpResponseForbidden()

        
        # Process the post.
        user = None
        admin_permissions = ''
        try:
            crsid = request.POST['crsid']
            user, _ = User.objects.get_or_create(username=crsid)
            admin_type = request.POST['admin_type'] or 'faculty'
            admin_permissions = request.POST['permissions'] or 'r'
            disable = request.POST['disable'] or '0'
            can_add_admins = Request.check(request.POST, "add_admin_admins")
            levels = Classifier.objects.filter(id__in=request.POST.getlist("org_levels"),group__family='L',group__organisation=self.org)
            subjects = Classifier.objects.filter(id__in=request.POST.getlist("org_subjects"),group__family='S',group__organisation=self.org)
        except KeyError:
            return HttpResponseBadRequest()
        except User.DoesNotExist:
            return HttpResponseNotFound()
 
        target_url = reverse("admin administrators", args=(self.year.url_id(), str(self.org.url_id())))

        if disable == "1" and (user.username != request.user.username):
            OrganisationRole.objects.filter(organisation=self.org,user=user).delete()
        else:    
            # Get a list of permission names
            perms = []
            if 'r' in admin_permissions :
                perms.append(Permission.READ)
            if 'w' in admin_permissions:
                perms.append(Permission.WRITE)
            if 'p' in admin_permissions:
                perms.append(Permission.PUBLISH)
            if 'c' in admin_permissions:
                perms.append(Permission.WRITE_CLASSIFICATIONS)
            if admin_type == 'key':
                perms.append(Permission.KEY_ADMIN)
                perms.append(Permission.WRITE_CLASSIFICATIONS)
                perms.append(Permission.WRITE_ADMINISTRATORS)
            if can_add_admins:
                perms.append(Permission.WRITE_ADMINISTRATORS)
                
            # build a query that contains all the permission names (dont execute it)
            permissions = Permission.objects.filter(name__in=perms)
    
    
            # TODO: extract this from managers to avoid excessive SQL and hidden side effects.
            #       With everything embedded its not possible to build a simple update and 
            #       Whoever is working on this code will have to go and look at make_admin
            #       to work out exacly what is happening to be sure its safe to use. 
            # Associate the admin with the organisation, granting all permissions
            # from the permissions set. 
            org_role = User.make_admin(user, self.org, permissions)
            
            # sync the classiers up with level and subjects
            for c in org_role.classifiers.all():
                if c in levels:
                    continue
                if c in subjects:
                    continue
                org_role.classifiers.remove(c)
            for c in levels:
                org_role.classifiers.add(c)
            for c in subjects:
                org_role.classifiers.add(c)

        return self.prepare_response(request, target_url=target_url)

