'''
Created on Aug 9, 2012

@author: ieb
'''
from django.views.generic.base import View
from django.http import HttpResponse, HttpResponseBadRequest,\
    HttpResponseNotFound
from timetables.utils.v1 import pparser
from timetables.model.models import Classification, User, Term
import logging
from timetables.utils.v1.year import Year
from timetables.utils.Json import JSON_INDENT, JSON_CONTENT_TYPE
from django.utils import simplejson as json
from timetables.utils.v1.grouptemplate import GroupTemplate

log = logging.getLogger(__name__)

class ValidateDatePatternView(View):
    '''
    Validate a date pattern to check it will be accepted by the date parser.
    '''
    
    
    def get(self, request, pattern):
        '''
        Check the pattern in the string which may be a contain ; seperated patterns. 
        The caller may also speficy the default term code to use in the get paramter t
        :param request:
        :param pattern:
        '''
        if 't' in request.GET:
            term = request.GET['t']
        else:
            term = 'Mi'
        if 'p' in request.GET:
            template  = GroupTemplate(request.GET['p'])
        else:
            template = GroupTemplate('MTuWThF12-14')
            
        if pattern.strip() == "":
            return HttpResponseBadRequest("Invalid Pattern, can't be empty")

        parts = pattern.split(";");
        for p in parts:
            try:
                pattern = pparser.fullparse(p, template)
            except:
                try:
                    pattern = pparser.fullparse(term+p, template)
                except:
                    return HttpResponseBadRequest("Invalid Pattern")
        terms = [ term.start for term in Term.objects.filter(term_info__day=False,term_info__term_type='S',academic_year__starting_year=2012).order_by("start")]
        year = Year(terms)
        terms = []
        for start, end in year.atoms_to_isos(pattern.patterns()):
            terms.append({ "start" : start, "end" : end })
        return HttpResponse(json.dumps({
                                            'response' : 'Ok',
                                            'terms' : terms
                                            }, indent=JSON_INDENT),content_type=JSON_CONTENT_TYPE )


class ValidateClassificationNameView(View):
    '''
    # For editing, this validator checks whether a specified classification_name is
    # non-empty and, if so, whether a classification for the given
    # classification_name already exists. If empty or already exists, then it
    # returns an error response unless the name that already exists is the one that
    # was being edited, otherwise it returns success.
    
    # For addition, this validator checks whether a specified classification_name
    # is non-empty and, if so, whether a classification for the given
    # classification_name already exists. If empty or already exists, then it
    # returns an error response, otherwise it returns success.
    
    '''

    
    def get(self, request, classification_name, classification_name_orig=None):
        if ( classification_name_orig ) :
            # we are dealing with an edit:
            # we can't edit the name to become empty:
            if ( classification_name == '' ) :
                return HttpResponseBadRequest("Classification name to edit empty!")
            # we can edit an existing name if it's the record we are working on:
            if ( classification_name == classification_name_orig ) :
                return HttpResponse("ok to edit existing classification", content_type="text/plain")
            # we can edit the name to be one that does not currently exist:
            try:
                Classification.objects.get(name=classification_name)
                return HttpResponseBadRequest("Classification to edit already exists!")
            except:
                return HttpResponse("ok to edit into new classification", content_type="text/plain")
        else :
            # we are dealing with an add:
            # we can't add a classification with an empty name:
            if ( classification_name == '' ) :
                return HttpResponseBadRequest("Classification name to add empty!")
            # we can't add a classification with a name that is already used:
            try:
                Classification.objects.get(name=classification_name)
                return HttpResponseBadRequest("Classification to add already exists!")
            except:
                return HttpResponse("ok to add", content_type="text/plain")



class ValidateCRSIDView(View):
    '''
    Validate a CRSID by looking it up in LDAP (if available).
    '''
    
    
    def get(self, request, crsid):
        try:
            # TODO: This needs to use LDAP eventually
            if len(crsid) == 0:
                return HttpResponseNotFound()
            _ = User.objects.get(username=crsid)
            return HttpResponse("OK", content_type="text/plain")
        except:
            return HttpResponseNotFound()


