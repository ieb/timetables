'''
Created on Aug 1, 2012

@author: ieb
'''
from timetables.administrator.views import AdminBaseView
from django.utils.decorators import method_decorator
from timetables.utils.xact import xact
from django.http import  HttpResponseForbidden
from timetables.model.models import Event, Location, Permission
from timetables.model.backend import PermissionSubject

class BulkEditLocation(AdminBaseView):
    '''
    Bulk edit many events locations.
    '''
    
    def js_main_module(self):
        return None

    @method_decorator(xact)
    def post(self, request, year_start, org_code):
        '''
        POST contains selected-events a comma separated list of EventIds
        location containing comma separated names of locations
        This should clear the list of locations on all the events listed and 
        add a new set.
        
        :param request:
        :param year_start:
        :param org_code:
        '''
        
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE, PermissionSubject(self.org)):
            return HttpResponseForbidden()
        
        location, _ = Location.objects.get_or_create(name=request.POST['location'])
        
        if "all-events" in request.POST and request.POST['all-events'] == "1":
            events = self.list_events(request,with_sort=False)
        else:
            events = Event.objects.filter(id__in=request.POST['selected-events'].split(","),
                                      owning_series__owning_group__owning_organisation=self.org)

        updated = 0
        for e in events:
            e.overridden_location = location 
            e.save()
            updated = updated + 1
            
        return self.prepare_response(request, updated=updated)
