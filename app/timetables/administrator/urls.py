from django.conf.urls.defaults import patterns, url
from timetables.administrator import views
from timetables.administrator.views.allevents import AllEvents
from timetables.administrator.views.publishtimetables import PublishTimetables
from timetables.administrator.views.bulkeditclassifications import BulkEditClassifications
from timetables.administrator.views.bulkeditlecture import BulkEditLecture
from timetables.administrator.views.bulkeditlocation import BulkEditLocation
from timetables.administrator.views.exportevents import ExportEvents
from timetables.administrator.views.quickeditevent import QuickEditEvent
from timetables.administrator.views.classifications import Classifications
from timetables.administrator.views.addclassifications import AddClassifications
from timetables.administrator.views.editclassifications import EditClassifications
from timetables.administrator.views.saveclassifications import SaveClassification
from timetables.administrator.views.addevents import AddEvents,\
    AddEventGroupFragment, AddEventSeriesFragment, EditEvents,\
    EditEventGroupFragment, EditEventSeriesFragment
from timetables.administrator.views.saveevent import SaveEvent
from timetables.administrator.views.validators import ValidateDatePatternView,\
    ValidateClassificationNameView, ValidateCRSIDView
from timetables.administrator.views.changehistory import ChangeHistory
from timetables.administrator.views.changehistoryrevision import ChangeHistoryRevision
from timetables.administrator.views.changehistoryrevisiondetails import ChangeHistoryRevisionDetails
from timetables.administrator.views.administrators import Administrators
from timetables.administrator.views.addadministrator import AddAdministrators
from timetables.administrator.views.saveadministrators import SaveAdministrators
from timetables.administrator.views.saveexternalurl import SaveExternalUrlView
from timetables.administrator.views.overview import Overview
from timetables.administrator.views.vieweventseries import ViewSeriesEventsFragment
from timetables.administrator.views.editadministrator import EditAdministrators
from timetables.administrator.views.revertevent import RevertEvent
from timetables.administrator.views.duplicatetimetables import DuplicateTimetables
from timetables.administrator.views.importevents import ImportEvents
from timetables.administrator.views.gototimetable import GoToTimetable

# The url pattern prefix for all admin urls acting on a specific Organisation
SLUG_REGEX = r"[a-zA-Z0-9_\-]+"
ORG_YEAR = r"^(\d{4})/(" + SLUG_REGEX + ")/"

urlpatterns = patterns('',
    url(r'^$',
            views.Home.as_view(),
            name="admin home"),
    url(r'^(?P<year>\d{4})/$',
            views.Home.as_view(),
            name="admin home year"),

    url(ORG_YEAR + r'$',
            Overview.as_view(),
            name="admin overview"),

    url(ORG_YEAR + r'events/add/f/group-editor/(.*)/$',
            AddEventGroupFragment.as_view(),
            name="admin add event fragment editor group"),
    url(ORG_YEAR + r'events/add/f/series-editor/(.*?)/(.*)/$',
            AddEventSeriesFragment.as_view(),
            name="admin add event fragment editor series"),
    url(ORG_YEAR + r'events/add/f/events/(.*)/$',
            ViewSeriesEventsFragment.as_view(),
            name="admin add event fragment view series"),
    url(ORG_YEAR + r'events/add/$',
            AddEvents.as_view(),
            name="admin add events"),
    url(ORG_YEAR + r'events/$',
            AllEvents.as_view(),
            name="admin events"),
    url(ORG_YEAR + r'timetable/$',
            GoToTimetable.as_view(),
            name="admin timetable"),


    url(ORG_YEAR + r'events/save/$',
            SaveEvent.as_view(),
            name="admin save events"),



    url(ORG_YEAR + r'events/edit/f/group-editor/(.*)/$',
            EditEventGroupFragment.as_view(),
            name="admin edit event fragment editor group"),
    url(ORG_YEAR + r'events/edit/f/series-editor/(.*?)/(.*)/$',
            EditEventSeriesFragment.as_view(),
            name="admin edit event fragment editor series"),


    url(ORG_YEAR + r'events/edit/f/events/(.*)/$',
            ViewSeriesEventsFragment.as_view(),
            name="admin edit event fragment view series"),

    url(ORG_YEAR + r'events/edit/(.*?)$',
            EditEvents.as_view(),
            name="admin edit event"),


    url(ORG_YEAR + r'events/quickedit/$',
            QuickEditEvent.as_view(),
            name="admin quick edit event"),

    url(ORG_YEAR + r'events/import/$',
            ImportEvents.as_view(),
            name="admin import events"),

    # FIXME: implement the correct end point.
    url(ORG_YEAR + r'events/export/$',
            ExportEvents.as_view(),
            name="admin export events"),


    url(ORG_YEAR + r'publish/$',
            PublishTimetables.as_view(),
            name="admin publish timetables"),

    url(ORG_YEAR + r'duplicate/$',
            DuplicateTimetables.as_view(),
            name="admin duplicate timetables"),


    url(ORG_YEAR + r'classifications/$',
            Classifications.as_view(),
            name="admin classifications"),
    url(ORG_YEAR + r'classifications/add/$',
            AddClassifications.as_view(),
            name="admin add classifications"),
    url(ORG_YEAR + r'classifications/edit/(.*?)$',
            EditClassifications.as_view(),
            name="admin edit classifications"),

    url(ORG_YEAR + r'classifications/save/$',
            SaveClassification.as_view(),
            name="admin save classifications"),


    # Not presently used, pending model/design resolution.
    url(ORG_YEAR + r'event/bulkedit/classification/$',
            BulkEditClassifications.as_view(),
            name="admin bulk edit classification"),

    url(ORG_YEAR + r'event/bulkedit/lecture/$',
            BulkEditLecture.as_view(),
            name="admin bulk edit lectures"),

    url(ORG_YEAR + r'event/bulkedit/location/$',
            BulkEditLocation.as_view(),
            name="admin bulk edit location"),

    url(ORG_YEAR + r'event/externalurl/save$',
            SaveExternalUrlView.as_view(),
            name="admin edit external url"),



    url(ORG_YEAR + r'changehistory/$',
            ChangeHistory.as_view(),
            name="admin change history"),
    url(ORG_YEAR + r'changehistory/revision/(.*?)/details/$',
            ChangeHistoryRevisionDetails.as_view(),
            name="admin change history revision details"),
    url(ORG_YEAR + r'changehistory/revision/(.*?)/$',
            ChangeHistoryRevision.as_view(),
            name="admin change history revision"),

    url(ORG_YEAR + r'changehistory/revert/$',
            RevertEvent.as_view(),
            name="admin change history revert"),


    url(ORG_YEAR + r'administrators/$',
            Administrators.as_view(),
            name="admin administrators"),
    url(ORG_YEAR + r'administrators/add/$',
            AddAdministrators.as_view(),
            name="admin add administrator"),

    url(ORG_YEAR + r'administrators/edit/(.*?)$',
            EditAdministrators.as_view(),
            name="admin edit administrator"),

    url(ORG_YEAR + r'administrators/save/$',
            SaveAdministrators.as_view(),
            name="admin save administrator"),




    url(r'^validate/date_pattern/(.*)$',
            ValidateDatePatternView.as_view(),
            name="admin validate date pattern"),

    url(r'^validate/crsid/(.*)$',
            ValidateCRSIDView.as_view(),
            name="admin validate crsid"),

    # validation url for classification names when adding or editing
    # classifications:
    url(r'^validate/classification_name/([^/]*)(?:/([^/]*))?/?$',
            ValidateClassificationNameView.as_view(),
            name="admin validate classification name"),

)
