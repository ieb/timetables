# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'UserProfile.feed_key'
        db.add_column('model_userprofile', 'feed_key',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=12),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'UserProfile.feed_key'
        db.delete_column('model_userprofile', 'feed_key')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'model.academicyear': {
            'Meta': {'object_name': 'AcademicYear'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_archived': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'starting_year': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'model.classification': {
            'Meta': {'object_name': 'Classification'},
            'classifier': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'classification'", 'unique': 'True', 'null': 'True', 'to': "orm['model.Classifier']"}),
            'contact_person': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '1024'})
        },
        'model.classifier': {
            'Meta': {'object_name': 'Classifier'},
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.ClassifierGroup']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'children'", 'null': 'True', 'to': "orm['model.Classifier']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'model.classifiergroup': {
            'Meta': {'object_name': 'ClassifierGroup'},
            'family': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"})
        },
        'model.collisionevents': {
            'Meta': {'object_name': 'CollisionEvents'},
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Event']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventSeries']"}),
            'timeslot': ('django.db.models.fields.IntegerField', [], {})
        },
        'model.event': {
            'Meta': {'object_name': 'Event'},
            'combination_set_id': ('django.db.models.fields.IntegerField', [], {}),
            'default_timeslot': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'null': 'True', 'to': "orm['model.TimeInterval']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_staff_overridden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2000, 1, 1, 0, 0)'}),
            'overridden_location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Location']", 'null': 'True'}),
            'overridden_staff': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.User']", 'null': 'True', 'symmetrical': 'False'}),
            'overridden_title': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '512', 'null': 'True'}),
            'owning_series': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventSeries']", 'db_column': "'e_s'"}),
            'position': ('django.db.models.fields.IntegerField', [], {})
        },
        'model.eventgroup': {
            'Meta': {'object_name': 'EventGroup'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'default_location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Location']"}),
            'default_pattern': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'embargo_date': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'owning_organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"}),
            'publication_status': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'p_status'"}),
            'term': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Term']"}),
            'usages': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'used_groups'", 'symmetrical': 'False', 'through': "orm['model.GroupUsage']", 'to': "orm['model.Organisation']"})
        },
        'model.eventgroupversion': {
            'Meta': {'object_name': 'EventGroupVersion'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'current_version': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'versions'", 'db_column': "'c_v'", 'to': "orm['model.EventGroup']"}),
            'default_location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Location']"}),
            'default_pattern': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'embargo_date': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'owning_organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"}),
            'publication_status': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'p_status'"}),
            'revision': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Revision']"}),
            'term': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Term']"}),
            'usages': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'versioned_used_groups'", 'symmetrical': 'False', 'through': "orm['model.GroupUsageVersion']", 'to': "orm['model.Organisation']"})
        },
        'model.eventseries': {
            'Meta': {'object_name': 'EventSeries'},
            'associated_staff': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.User']", 'symmetrical': 'False'}),
            'can_borrow': ('django.db.models.fields.BigIntegerField', [], {'default': 'True'}),
            'date_time_pattern': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_column': "'t_s'"}),
            'external_url': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Location']"}),
            'owning_group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'owned_series'", 'to': "orm['model.EventGroup']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'usages': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['model.EventGroup']", 'db_column': "'e_g'", 'symmetrical': 'False'})
        },
        'model.eventseriesversion': {
            'Meta': {'object_name': 'EventSeriesVersion'},
            'associated_staff': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.User']", 'symmetrical': 'False'}),
            'can_borrow': ('django.db.models.fields.BigIntegerField', [], {'default': 'True'}),
            'current_version': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'versions'", 'db_column': "'c_v'", 'to': "orm['model.EventSeries']"}),
            'date_time_pattern': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_column': "'t_s'"}),
            'external_url': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Location']"}),
            'owning_group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'versioned_owned_series'", 'to': "orm['model.EventGroupVersion']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'usages': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['model.EventGroup']", 'db_column': "'e_g'", 'symmetrical': 'False'})
        },
        'model.eventtype': {
            'Meta': {'object_name': 'EventType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        },
        'model.eventversion': {
            'Meta': {'object_name': 'EventVersion'},
            'combination_set_id': ('django.db.models.fields.IntegerField', [], {}),
            'current_version': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'versions'", 'to': "orm['model.Event']"}),
            'default_timeslot': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'null': 'True', 'to': "orm['model.TimeInterval']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_staff_overridden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2000, 1, 1, 0, 0)'}),
            'overridden_location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Location']", 'null': 'True'}),
            'overridden_staff': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.User']", 'null': 'True', 'symmetrical': 'False'}),
            'overridden_title': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '512', 'null': 'True'}),
            'owning_series': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventSeriesVersion']", 'db_column': "'e_s'"}),
            'position': ('django.db.models.fields.IntegerField', [], {})
        },
        'model.grouprevision': {
            'Meta': {'object_name': 'GroupRevision'},
            'enclosing_revision': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Revision']"}),
            'group_version': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventGroupVersion']", 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'usage_version': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.GroupUsageVersion']"})
        },
        'model.groupusage': {
            'Meta': {'object_name': 'GroupUsage'},
            'classifiers': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['model.Classifier']", 'symmetrical': 'False'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventGroup']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"})
        },
        'model.groupusageversion': {
            'Meta': {'object_name': 'GroupUsageVersion'},
            'classifiers': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['model.Classifier']", 'symmetrical': 'False'}),
            'current_version': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'versions'", 'to': "orm['model.GroupUsage']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventGroupVersion']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"})
        },
        'model.location': {
            'Meta': {'object_name': 'Location'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '512'})
        },
        'model.organisation': {
            'Meta': {'unique_together': "(('name', 'year'), ('code', 'year'))", 'object_name': 'Organisation'},
            'code': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'year': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.AcademicYear']"})
        },
        'model.organisationrole': {
            'Meta': {'object_name': 'OrganisationRole'},
            'classifiers': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['model.Classifier']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"}),
            'role': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Role']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'model.permission': {
            'Meta': {'object_name': 'Permission'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'})
        },
        'model.personaltimetable': {
            'Meta': {'object_name': 'PersonalTimetable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'year': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.AcademicYear']"})
        },
        'model.personaltimetableusage': {
            'Meta': {'unique_together': "(('timetable', 'group', 'series'),)", 'object_name': 'PersonalTimetableUsage'},
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.GroupUsage']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventSeries']"}),
            'timetable': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.PersonalTimetable']"})
        },
        'model.revision': {
            'Meta': {'object_name': 'Revision'},
            'comment': ('django.db.models.fields.TextField', [], {}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'version': ('django.db.models.fields.IntegerField', [], {})
        },
        'model.role': {
            'Meta': {'object_name': 'Role'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['model.Permission']", 'through': "orm['model.RolePermission']", 'symmetrical': 'False'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'r_t'"})
        },
        'model.rolepermission': {
            'Meta': {'object_name': 'RolePermission'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'permission': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Permission']"}),
            'role': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Role']"})
        },
        'model.term': {
            'Meta': {'object_name': 'Term'},
            'academic_year': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'terms'", 'db_column': "'a_y'", 'to': "orm['model.AcademicYear']"}),
            'end': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start': ('django.db.models.fields.DateField', [], {}),
            'term_info': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.TermInfo']"})
        },
        'model.terminfo': {
            'Meta': {'object_name': 'TermInfo'},
            'day': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16'}),
            'term_type': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        'model.timeinterval': {
            'Meta': {'object_name': 'TimeInterval'},
            'end_time': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {})
        },
        'model.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'feed_key': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '12'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_notified': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'notification_email': ('django.db.models.fields.CharField', [], {'max_length': '75'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        }
    }

    complete_apps = ['model']