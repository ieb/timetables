from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Q, Count
from django.contrib.auth.models import User as DjangoUser
from django.contrib.auth.models import UserManager as DjangoUserManager
from django.contrib.auth.models import Permission as DjangoPermission
from django.core.exceptions import ValidationError
from django.dispatch import receiver
from django.db.models.query import QuerySet

from datetime import datetime, date
import math

from timetables.templatetags.templatetags import timetables_tags
from timetables.utils import ints
from operator import xor
from itertools import groupby


import logging
from django.utils.decorators import method_decorator
from timetables.utils.xact import xact
from django.utils.datetime_safe import date
from django.db.models.aggregates import Max
import traceback
from django.utils.crypto import salted_hmac, get_random_string,\
    constant_time_compare
from django.utils.functional import lazy
log = logging.getLogger(__name__)
del logging
import time

__considerations = """
- We need to be able to isolate one year's data from another.
    Because we archive content at the end of a year, we can't have changes
    made for a new academic # year change the data of an archived year.
    Care must be taken not to leak references between years.
- Need to support borrowing groups from different organisations
- Faculties can have arbitrarily many classification groups (level, subject,
    sub-subject, foo, bar)
- Subjects and Levels need to be defined on a per-organisation basis, as
    different organisations are free to add levels, and to name the "subjects"
    classifier whatever they want. Additionally I'd assume that different
    organisations don't see the values added by other organisations.
- The design does not have any concept of departments nested under faculties,
    it just has the abstract term "Faculty" which can be taken to be any
    organisation (including departments). Somewhat abstract names (like
    organisation) should probably be used in order to minimise unwanted
    assumptions as to the role of an entity based on its name. These terms
    be given clear definitions, here and or at:
    https://docs.google.com/a/caret.cam.ac.uk/spreadsheet/ccc?key=0AodHvow9JoMhdGdGRE1PaFc2YW5UaG9rYTh1YVNPc1E

"""
class NotEditable(Exception):
    """Raised when an attempt is made to modify a read only model, i.e.
    changing an archived year."""
    pass


class QuerySetManager(models.Manager):
    """
    A Manager which allows models to provide a customised queryset.
    
    See: http://djangosnippets.org/snippets/734/ 
    """
    def get_query_set(self):
            return self.model.QuerySet(self.model)


class UserManager(DjangoUserManager):

    def all_admins(self):
        superadmin = Q(user_permissions=User.superadmin_permission())
        normal_admin = Q(organisationrole__isnull=False)
        return (self.filter(superadmin | normal_admin).distinct()
                .order_by("username"))

    def superadmins(self):
        "Gets a queryset of all users who have Super Administrator status."
        return self.filter(user_permissions=User.superadmin_permission())

    def admins(self, year=None, organisation=None, permissions=None):
        """Filters the set of all users to those which are administrators of at
        least one Organisation. Optionally may take
        year as integer ID,
        organisation as integer ID,
        permissions as list of Permission objects
        """
        queryset = self.filter(organisationrole__isnull=False)
        if year:
            queryset = queryset.filter(
                    organisationrole__organisation__year__exact=year)
        if organisation:
            queryset = queryset.filter(
                    organisationrole__organisation=organisation)
        if permissions:
            for permission in permissions:
                queryset = queryset.filter(
                    organisationrole__role__permissions=permission)
        return queryset

class User(DjangoUser):
    """A proxy for the Django User model in order to provide timetables specific
    methods.
    """

    SUPERADMIN_PERM_NAME = "Has super administrator status."
    SUPERADMIN_PERM_CODENAME = "superadministrator"
    SUPERADMIN_PERM = "auth.superadministrator"

    ADMINISTRATOR_DISABLED_PERM_NAME = "Administrator status has been disabled."
    ADMINISTRATOR_DISABLED_PERM_CODENAME = "administrator_disabled"
    ADMINISTRATOR_DISABLED_PERM = "auth.administrator_disabled"

    objects = UserManager()

    class Meta:
        proxy = True

    @staticmethod
    def _get_or_make_permission(name, codename):
        dj_user_type = ContentType.objects.get(app_label="auth", model="user")

        # Declaring permissions on proxy classes (such as this) doesn't seem to
        # work, so instead we'll create permissions dynamically on the django
        # User model.
        perm, _ = DjangoPermission.objects.get_or_create(
                content_type=dj_user_type,
                codename=codename,
                defaults=dict(name=name))
        return perm

    @staticmethod
    def superadmin_permission():
        return User._get_or_make_permission(User.SUPERADMIN_PERM_NAME,
                User.SUPERADMIN_PERM_CODENAME)

    @staticmethod
    def administrator_disabled_permission():
        return User._get_or_make_permission(
                User.ADMINISTRATOR_DISABLED_PERM_NAME,
                User.ADMINISTRATOR_DISABLED_PERM_CODENAME)

    @staticmethod
    def is_disabled(user):
        # can't disable django superusers
        if user.is_superuser:
            return False
        if not hasattr(user, "__is_disabled"):
            user.__is_disabled = user.has_perm(User.ADMINISTRATOR_DISABLED_PERM)
        return user.__is_disabled


    @staticmethod
    def has_disabled_perm(user):
        """Checks the user's permission list for the disabled permission.
        
        This is not the same as is_disabled() as that ignores superusers. This
        is probably slower and should not be used to check for authentication.
        """
        return User.ADMINISTRATOR_DISABLED_PERM in user.get_all_permissions()

    @staticmethod
    def disable_admin(user):
        """Marks a timetables admin as being disabled. Disabled admins can no
        longer access any administration or superadministration functions.
        
        Note that disabling a Django superuser will have no effect."""
        user.user_permissions.add(User.administrator_disabled_permission())

    @staticmethod
    def enable_admin(user):
        user.user_permissions.remove(User.administrator_disabled_permission())


    @staticmethod
    def make_superadmin(user):
        "Grants a user Timetables Super Administrator status."
        user.user_permissions.add(User.superadmin_permission())

    @staticmethod
    def strip_superadmin(user):
        """Revokes a user's superadmin status."""
        user.user_permissions.remove(User.superadmin_permission())
        del(user.__is_super_admin)

    @staticmethod
    def is_superadmin(user):
        "Returns: True if this user is a Timetables Super Administrator."
        # this is written this way to minimize the number of resolutions
        # for most users.
        if user.is_superuser:
            return True
        if not hasattr(user, "__is_super_admin"):
            user.__is_super_admin = (user.has_perm(User.SUPERADMIN_PERM)
                    and not user.has_perm(User.ADMINISTRATOR_DISABLED_PERM))
        return user.__is_super_admin

    @staticmethod
    def is_admin(user, year=None, organisation=None):
        if User.is_disabled(user):
            return False

        if User.is_superadmin(user):
            return True

        queryset = OrganisationRole.objects.filter(user=user)
        if year:
            queryset = queryset.filter(organisation__year__exact=year)
        if organisation:
            queryset = queryset.filter(organisation=organisation)
        return queryset.exists()

    @staticmethod
    def make_admin(user, organisation, permissions):
        """Makes this user an administrator of the specified organisation with
        the provided permissions.

        Returns: The OrganisationRole object representing this user's
            relationship with the organisation.
        """
        assert organisation
        assert permissions is not None
        if organisation.year.is_archived:
            raise NotEditable("Year is archived: %s" % organisation.year)

        # Get the Role with the correct permissions
        role = Role.objects.get_role_or_create(permissions)

        # Fetch the OrganisationRole representing our relationship
        organisation_role, _ = OrganisationRole.objects.get_or_create(
                user=user, organisation=organisation, defaults=dict(role=role))

        if organisation_role.role != role:
            organisation_role.role = role
            organisation_role.save()
        return organisation_role

    def organisations(self):
        return Organisation.objects.filter(organisationrole__user=self)

    @staticmethod
    def as_context(user):
        """ Sets some useful properties for a logged in user in the context.

        If the user is not logged in all that is set is: is_logged_in : False.
        Otherwise a number of useful user attributes can be directly entered
        in the page templates.
        """
        if user is None:
            return { 'is_logged_in' : False }
        if user.is_anonymous():
            return { 'is_logged_in' : False }

        # Pull the user notification settings out of the user profile,
        # if one exists, else use defaults:
        is_notified = False
        notification_email = user.email
        try:
            # use values from profile if profile exists:
            user_profile = UserProfile.objects.get(user=user)
            is_notified = user_profile.is_notified
            notification_email = user_profile.notification_email
        except UserProfile.DoesNotExist:
            # stick with the defaults if no profile has been created:
            pass            

        return {
            'is_logged_in' : True,
            'name': "%s %s" % (user.first_name, user.last_name),
            'username': user.username,
            'is_notified' : is_notified,
            'notification_email' : notification_email,
            'isAdmin' : User.is_admin(user),
            'isSuperAdmin' : User.is_superadmin(user)
        }

@receiver(models.signals.pre_save,
    sender=User)
def lookup_sync_on_save(instance=None, **kwargs):
    """
    A pre-save signal receiver which updates the user's name/email from 
    lookup.cam.
    """
    assert instance, "A user instance must be provided."

    # We need to import lookup_synchronisation here rather than at the top level
    # to avoid a dependency loop.
    try:
        import lookup_synchronisation
        lookup_synchronisation.synchronise_user(instance, save_user=False)
    except:
        log.error("Failed to perform lookup, using defaults")
        if instance.username is not None:
            if instance.last_name is None:
                instance.last_name = ("%s (Last Name)" % instance.username)[:30]
            if instance.email is None:
                instance.email = "%s@cam.ac.uk" % instance.username
            if instance.first_name is None:
                instance.first_name = "%s" % instance.username[0]

class UserProfile(models.Model):
    '''
    Contains attributes that can be set for a given user.
    '''
    is_notified = models.BooleanField("Whether the user should be notified of "
            "changes made to the events they have in their timetable.", default=False)
    notification_email = models.CharField(max_length=128)
    
    feed_key = models.CharField("A random string which can be provided in "
            "personal timetable feed URLs in order to access the feed without "
            "logging in. e.g. to allow Google Calendar to access a private "
            "feed.", max_length=12, default="")
    
    user = models.ForeignKey(User)
      
    @staticmethod
    def for_user(user):
        profile, _ = UserProfile.objects.get_or_create(user=user)
        return profile
        
    
    def get_feed_key(self, create_if_missing=True):
        key = self.feed_key
        if key is not None and len(key) == 12:
            return key
        
        if create_if_missing is False:
            raise ValueError("No feed_key is set and create_if_missing is "
                    "False.")
        
        key = get_random_string()
        self.feed_key = key
        self.save()
        return key
    
    def feed_key_matches(self, provided_key):
        """
        Securely checks if the provided_key matches the feed_key of this
        UserProfile.
        
        Returns: True if they match, False otherwise.
        """
        key = self.feed_key
        if key is None or len(key) < 12:
            return False
        return constant_time_compare(self.feed_key, provided_key)

    def __unicode__(self):
        return u"user: %s[%s], notified: %s" % (self.user.get_full_name(),
                self.user.username, self.is_notified)

# Basic Structure to People, Organisation, Roles, Terms and Years

class  AcademicYearManager(models.Manager):
    def active_years(self, user=None):
        if user is None:
            return self.filter(is_archived=False)
        else:
            return self.filter(id__in=self.filter(
                        is_archived=False,
                        organisation__in=Organisation.objects.organisations_for_user(user))).order_by("starting_year")


class AcademicYear(models.Model):
    '''
    Define a year, may have more metadata attached in future
    '''
    starting_year = models.IntegerField("The year this academic year begins in"
            , unique=True)
    is_archived = models.BooleanField("Whether the year has been archived as a "
            "read only view on past events.", default=False)
    objects = AcademicYearManager()

    def url_id(self):
        return self.starting_year

    def name(self):
        return "%d/%d" % (self.starting_year, (self.starting_year + 1) % 100)

    def standard_terms(self):
        terms = Term.objects.filter(academic_year=self,
                term_info__term_type='S')
        standard_terms = list(TermInfo.objects.filter(term_type='S').values("name"))

        # FIXME: This code is hard to read, please add documentation to say what its supposed to do.
        # It will save others time later 
        def term_for_name(terms, name):
            filtered = filter(lambda t: t.term_name() == name, terms)
            return filtered[0] if filtered else None
        return dict([(term_name, term_for_name(terms, term_name))
                for term_name in standard_terms])

    def is_current(self, today=None):
        """True if this academic year is active for the current time.

        An AcademicYear is (arbitrarily) considered to be active from the 1st
        of June on its starting year, up to the last day of May in the next
        year.
        """
        today = today or date.today()
        return (today >= date(self.starting_year, 6, 1) and
                today < date(self.starting_year + 1, 6, 1))

    @staticmethod
    def get_current(today=None, as_int=False):
        """
        Gets the current academic year based on today's date.
        
        Args:
            today: The date of "today" to use. If not specified, today's actual
                date is used.
            as_int: If True then just the integer value of the starting year
                is returned rather than querying the db for an actual
                AcademicYear instance.
        Returns: The AcademicYear instance of the current academic year.
        """
        # work out starting year
        today = date.today()
        current_start_year = today.year
        if today.month < 6: # assumes that academic year starts on 1st June
            current_start_year = current_start_year - 1

        if as_int is True:
            return current_start_year
        return AcademicYear.objects.get(starting_year=current_start_year)

    def __cmp__(self, other):
        "Years are ordered by name."
        # This works as dates in string form (2012/13, 2013/14 etc) sort
        # as expected, but it seems like it would be sensible to store the start
        # year as an integer instead of the name, and calculate the name based
        # on the year. Another alternative would be to name based on the range
        # of a Year's terms.
        return cmp(self.starting_year, other.starting_year)

    def __unicode__(self):
        return self.name()

class RoleManager(models.Manager):
    def with_permissions(self, permissions):
        """Returns a queryset containing Roles which have exactly the specified
        permissions, no more, no less.
        """
        # Querying for many-to-many relationships which have exactly some values
        # seems to be somewhat tricky. You can't just do 
        # filter(permissions__in=[x,y,z]) because that would match roles with
        # any of x, y, z plus any other permissions. This would be particularly
        # bad for us as you might query for a role with read permissions, but
        # get one with read plus write permissions!
        #
        # The strategy I'm using here is to make a filter call for each 
        # specified permission, plus a check that the number of permissions
        # owned by a role is the same as that which we query for.
        # This may well not be the best way, feel free to change this if you can
        # do better.

        # ignore duplicates
        permissions = set(permissions)
        perm_count = len(permissions)

        # Restrict the results to those roles with the right number of 
        # permissions
        queryset = (self.annotate(permission_count=models.Count("permissions"))
                .filter(permission_count=perm_count))

        # successively pare down the queryset to match only the specified 
        # permissions
        for permission in permissions:
            queryset = queryset.filter(permissions=permission)

        return queryset

    @staticmethod
    def hash_string(string):
        return ints.hash_ints(ord(char) for char in string)

    @staticmethod
    def uniqueness_hash(permissions, base=36, max_hashlen=6):
        "Generates a unique ID for a set of permissions"
        assert isinstance(permissions, set)
        rawhash = abs(reduce(xor, map(RoleManager.hash_string, permissions)))\
                % (base ** max_hashlen)
        return ints.to_string(rawhash, base)

    def get_role_or_create(self, permissions):
        ""
        queryset = self.with_permissions(permissions)[:1]
        if queryset:
            return queryset[0]

        # Roles need a distinct name, and as we're generating them on the fly
        # we need to generate unique names for roles with distinct sets of
        # permissions.
        permset = set(p.name for p in permissions)
        uniqueness_hash = RoleManager.uniqueness_hash(permset)

        # Note: this is not atomic, so in theory we could create > 1 role with
        # the same set of permissions, but roles will be created so rarely that
        # it shouldn't really matter in practice. If this is a problem then
        # the simplest thing to do would to be pre-create the Roles with all 
        # required permutations of permissions.
        return self._createRole(uniqueness_hash, permissions, permset)

    @method_decorator(xact)
    def _createRole(self, uniqueness_hash, permissions, permset):
        role = Role(name="administrator-%s" % uniqueness_hash)
        role.save(force_insert=True)

        RolePermission.objects.bulk_create(
                [RolePermission(role=role, permission=perm)
                        for perm in permissions])
        assert set(p.name for p in role.permissions.all()) == permset
        return role

class Role(models.Model):
    '''
    Names the roles in the system
    '''
    ROLE_TYPE_CHOICE = (
                ('A', 'Administrative'),
                ('O', 'Other')
                )

    name = models.CharField(max_length=64, unique=True)
    type = models.CharField(max_length=1,
                            choices=ROLE_TYPE_CHOICE,
                            help_text='The Type of Role',
                            db_column="r_t")
    permissions = models.ManyToManyField("Permission", through="RolePermission",
            help_text="The permissions associated with this role.")

    objects = RoleManager()

    def __unicode__(self):
        return "Role(%s)" % (self.name)


class PermissionManager(models.Manager):
    # These are created in model/fixtures/initial_data.yaml
    def admin_read(self):
        return self.get(name=Permission.READ)
    def admin_write(self):
        return self.get(name=Permission.WRITE)
    def admin_publish(self):
        return self.get(name=Permission.PUBLISH)
    def admin_write_classifications(self):
        return self.get(name=Permission.WRITE_CLASSIFICATIONS)
    def admin_write_administrators(self):
        return self.get(name=Permission.WRITE_ADMINISTRATORS)
    def admin_key_admin(self):
        return self.get(name=Permission.KEY_ADMIN)

class Permission(models.Model):

    READ = "administration_read"
    WRITE = "administration_write"
    PUBLISH = "administration_publish"
    WRITE_CLASSIFICATIONS = "administration_write_classifications"
    WRITE_ADMINISTRATORS = "administration_write_administrators"
    KEY_ADMIN = "administration_key_admin"
    '''
    Names the permissions in the system
    '''
    name = models.CharField(max_length=50, unique=True)

    objects = PermissionManager()

    def __unicode__(self):
        return u"Permission(%s)" % (self.name)

class RolePermission(models.Model):
    '''
    Grants a permission to a role
    '''
    role = models.ForeignKey(Role)
    permission = models.ForeignKey(Permission)

    def __unicode__(self):
        return u"%s %s" % (self.role.name, self.permission.name)

class TermInfo(models.Model):
    '''
    Defines the aspects of terms which are the same across years. For example,
    every Michaelmas term will reference the same TermInfo instance to mark them
    all as being an instance of the Michaelmas term.
    '''
    NAME_MICHAELMAS = u"michaelmas"
    NAME_LENT = u"lent"
    NAME_EASTER = u"easter"
    NAME_EASTER_DAY = u"easter_day"
    STANDARD_TERM_NAMES = set([NAME_MICHAELMAS, NAME_LENT, NAME_EASTER])
    _TERM_ORDER = {NAME_MICHAELMAS: 0, NAME_LENT: 1, NAME_EASTER_DAY: 2,
            NAME_EASTER: 3}

    TERM_SHORT_NAMES = {
            NAME_MICHAELMAS : 'Mi',
            NAME_LENT: 'Le',
            NAME_EASTER: 'Ea',
            }

    STANDARD_TERM = "S"
    OTHER_TERM = "O"
    TERMINFO_TYPE_CHOICES = (
        (STANDARD_TERM, 'Standard Term'),
        (OTHER_TERM, 'Other Term')
    )
    name = models.CharField(max_length=64, unique=True)
    # Day defines if the term is a day or not. (easterday)
    day = models.BooleanField(default=False)
    term_type = models.CharField("Defines if the terminfo is standard or "
            "another type ",
            max_length=1,
            choices=TERMINFO_TYPE_CHOICES)

    def is_standard(self):
        return self.term_type == self.STANDARD_TERM
    is_standard.boolean = True

    def _name_order_value(self):
        "Get an ordering value to order terms by name."
        # If we're a standard term, order by special ordering value rather than
        # name. __cmp__ ensures that special order values can't be compared to
        # normal names.
        if self.term_type == self.STANDARD_TERM:
            return self._TERM_ORDER[self.name]
        # Otherwise order by name for other unknown terms
        return self.name

    def __cmp__(self, other):
        # order first on type (invert order so standard terms (S) come before 
        # 'other' (O))
        typecmp = cmp(self.term_type, other.term_type) * -1
        if typecmp != 0:
            return typecmp
        # Break ties with name, which orders standard terms as they appear in
        # the calendar (michaelmas < lent < easter_day < easter)
        return cmp(self._name_order_value(), other._name_order_value())

    def save(self, **kwargs):
        self.clean()
        models.Model.save(self, **kwargs)

    def clean(self):
        """
        Do some validation to ensure that terms marked as STANDARD_TERMs have
        one of the expected names, and that names are canonicalised to
        lowercase.
        """
        canonical_name = self.name.lower()
        if self.term_type == self.STANDARD_TERM:
            if self.day:
                if canonical_name != self.NAME_EASTER_DAY:
                    raise ValidationError("Standard TermInfos representing days"
                            " must be called easter_day. Got: %s" % self.name)
            elif canonical_name not in self.STANDARD_TERM_NAMES:
                raise ValueError("Standard terms must be named one of: %s, "
                        "but was: %s" % (self.STANDARD_TERM_NAMES, self.name))

        # Emit a warning if the name value is changed by canonicalising it 
        if canonical_name != self.name:
            log.warning("Lowercasing term name: %s" % self.name)
        self.name = canonical_name

    def __unicode__(self):
        return timetables_tags.humanise(self.name)

    class Meta(object):
        verbose_name = "Term Information"
        verbose_name_plural = verbose_name

class  TermManager(models.Manager):
    def default_active_terms(self):
        """Gets the 4 standard terms for all active year
        (Michaelmas, Lent & Easter, plus a special term for Easter day).
        """
        today = date.today()
        current_start_year = today.year
        if today.month < 6: # assumes that academic year starts on 1st June
            current_start_year = current_start_year - 1

        year_set = [x for x in range(current_start_year - 1, current_start_year + 4)]

        return self.filter(academic_year__is_archived=False, academic_year__starting_year__in=year_set,
                term_info__term_type="S").prefetch_related("academic_year", "term_info")


class Term(models.Model):
    '''
    A term is period of time with a start end end date associated with an academic year as terms
    have different start and end dates each year. If the start and end date is the same, the Term
    represents a day, like Easter Day. There may be more than 3 terms in a year, and terms may be
    special to organisations. eg Engineering Summer School.
    '''
    objects = TermManager()

    term_info = models.ForeignKey(TermInfo)
    academic_year = models.ForeignKey(AcademicYear, db_column="a_y",
            related_name="terms")
    start = models.DateField("start of term")
    end = models.DateField("end of term")

    def clean(self):
        if self.start > self.end:
            raise ValidationError("Start date must be before or equal to end "
                    "date.")

    def term_name(self):
        return self.term_info.name
    
    def contained_months(self):
        """
        Find the months contained in this term.
        
        Returns: A sequence of (year, month) tuples.
        """
        def next_month(date):
            if date.month == 12:
                return date.replace(year=date.year + 1, month=1)
            return date.replace(month=date.month+1)
        
        assert self.start <= self.end, "A term must start before it ends."
        start = date(self.start.year, self.start.month, 1)
        end = date(self.end.year, self.end.month, 1)
        
        months = [start]
        month = start
        while month < end:
            month = next_month(month)
            months.append(month)
        return [(m.year, m.month) for m in months]

    def __unicode__(self):
        return ("Term(name: %s, standard?: %s, start: %s, end: %s, "
                "academic_year: %s)") % (
                self.term_info.name,
                ("Yes" if self.term_info.term_type == TermInfo.STANDARD_TERM
                        else "No"),
                self.start.isoformat(),
                self.end.isoformat(),
                self.academic_year.name)

    def __cmp__(self, other):
        # TermInfo objects know how to compare themselves sensibly
        return cmp(
                (self.term_info, self.academic_year),
                (other.term_info, other.academic_year))



class OrganisationRole(models.Model):
    '''
    Defines the roles that users have within an organisation. The roles define what a user can do.
    '''
    user = models.ForeignKey(User)
    role = models.ForeignKey(Role)
    organisation = models.ForeignKey("Organisation")
    classifiers = models.ManyToManyField("Classifier", help_text="The "
            "classifiers under organisation that this role confers permissions "
            "for.")

    def clean(self):
        # Ensure all of our classifiers are owned by our organisation.
        organisation_classifiers = self.classifiers.filter(
                group__organisation=self.organisation).count()
        all_classifiers = self.classifiers.count()
        if organisation_classifiers != all_classifiers:
            raise ValidationError("Not all classifiers were owned by this "
                    "OrganisationRole's organisation. role: %d, expected: %d, "
                    "got: %d" % (self.id, all_classifiers,
                            organisation_classifiers))

    def __unicode__(self):
        return u"user: %s[%s], role: %s, org: %s" % (self.user.get_full_name(),
                self.user.username, self.role.name, self.organisation.name)

@receiver(models.signals.m2m_changed,
        sender=OrganisationRole.classifiers.through)
def prevent_alien_classifiers(sender, instance, action, reverse, model,
        pk_set, **kwargs):
    """Monitors the changed signal on the many to many rel between 
    OrganisationRoles and Classifiers in order to prevent Classifiers not
    associated with an organisation from being added to an OrganisationRole.
    
    ValidationErrors will only be raised from this function in the case of
    programmer error, not from user input. 
    """
    if action != "pre_add":
        return
    log.debug("prevent_alien_classifiers(%s %s %s %s %s %s)" % (sender,
            instance, action, reverse, model, pk_set))
    if model == OrganisationRole:
        classifier = instance
        # Check if any OrganisationRole objects with our set of keys exists
        # such that the orgrole's organisation is not equal to the organisation
        # of the classifier
        if (OrganisationRole.objects.filter(id__in=pk_set)
                .exclude(organisation=classifier.group.organisation)
                .exists()):
            raise ValidationError("One or more OrganisationRoles with pks: %s "
                    "did not share the the same Organisation as the "
                    "Classifier: %s" % (pk_set, classifier))
        return

    elif model == Classifier:
        orgrole = instance
        if (Classifier.objects.filter(id__in=pk_set)
                .exclude(group__organisation=orgrole.organisation)
                .exists()):
            raise ValidationError("One or more Classifiers with pks: %s "
                    "did not share the the same Organisation as the "
                    "OrganisationRole: %s" % (pk_set, orgrole))
        return

    assert False, "Unexpected event: %s %s %s %s %s %s" % (sender, instance,
            action, reverse, model, pk_set)

class Location(models.Model):
    '''
    A Location has a name.
    This list of types is shared across years, so existing rows should not
    normally be modified. If locations become obsolete then they should be
    marked as such with a new field, rather than deleting them.
    '''
    name = models.CharField(max_length=512, unique=True)

    def __unicode__(self):
        return self.name

class EventType(models.Model):
    """
    The available types of events, for example Lectures, Labs, Seminars etc etc.
    This list of types is shared across years, so it should be considered append
    only, existing values should not be changed/removed otherwise archived years
    would be modified.
    """
    name = models.CharField(max_length=128, unique=True)

    def __unicode__(self):
        return self.name

class OrganisationManager(models.Manager):

    def with_years(self):  # Used in superadmin
        """Gets all organisations with pre-fetched years (to avoid O(n) separate
        db queries when doing something with an org + year."""
        return self.select_related("year").all().order_by(
                "year__starting_year", "name")

    def grouped_into_years(self): # Used in superadmin
        """Gives an list of (yearname, [organisation]) tuples.

        Note that the nested lists of organisations are iterators.
        """
        # Eagerly evaluate the iterator of groupby into a list to allow
        # multiple iterations over years.
        return [(year, list(organisations)) for (year, organisations)
                in groupby(self.with_years(), lambda org: org.year)]

    def organisation_years_bootstrap(self): # Used in superadmin
        org_years = self.grouped_into_years()
        years = [{
                "start_year": year.starting_year,
                "name": year.name(),
                "organisations": [{"id": org.id, "code": org.code, "name": org.name}
                        for org in organisations]
                }
                for (year, organisations) in org_years]
        return years

    def organisation_bootstrap(self, org_id):  # TODO: Not used, remove is really the case
        org = (self.prefetch_related("classifiergroup_set__classifier_set")
                .get(id=org_id))

        org_json = {
            "id": org.id,
            "name": org.name,
            "code": org.code,
            "year": org.year.starting_year,
            "classifier_groups": [
                {
                    "id": classifier_group.id,
                    "name": classifier_group.name,
                    "type": classifier_group.get_family_display().lower(),
                    "classifiers": [
                        {
                            "id": classifier.id,
                            "value": classifier.value
                        }
                        for classifier in classifier_group.classifier_set.all()
                    ]
                }
                for classifier_group in org.classifiergroup_set.all()
            ]
        }

        return org_json

    def organisations_for_user(self, user):
        if User.is_superadmin(user):
            return Organisation.objects.all()
        return Organisation.objects.filter(organisationrole__user=user)

# FIXME: The x.500 spelling of Organisation is Organization, spelling with an s may cause problems.
class Organisation(models.Model):
    '''
    An Organisation is an entity which represents Departments, Faculties and
    any other groups publishing timetables.

    The organisations present in a given academic year are listed on the Student
    frontpage (see Student wireframe 1.0 LECTURE LIST - HOME).

    The term "Faculty" is used by HEAD throughout their design, but here the
    more general term Organisation is used to avoid the assumption that only
    Faculties can be (HEAD) Faculties.
    '''
    name = models.CharField(max_length=512, unique=False)
    # FIXME: code is not the slug. code is the code as in CamSIS code, and I dont think anyone wants that in a URL.
    code = models.SlugField(
            "The name to use to refer to this organisation in URLs.")
    # This means you have to create the organization every year.
    year = models.ForeignKey(AcademicYear,
            help_text="The year this organisation is defined for.")

    objects = OrganisationManager()

    def to_json(self):
        return {
            "id": self.id,
            "name": self.name,
            "code": self.code,
            "year": self.year.starting_year
        }

    def from_json(self, json):
        name = json.get("name")
        code = json.get("code")
        year = AcademicYear.objects.get(starting_year=json.get("year"))
        # Ignore any id present in the JSON and create a new object
        return Organisation(name=name, code=code, year=year)

    def url_id(self):
        return self.code

    class Meta:
        unique_together = (("name", "year"), ("code", "year"))

    def __unicode__(self):
        return "%s@%s" % (self.name, self.year.name())

class ClassifierGroupManager(models.Manager):

    def for_organisation(self, organisation):
        """
        Gets a list of the ClassifierGroups for organisation.
 
        The returned list is ordered primarily by family such that
        level < subject < custom. Groups are ordered secondarily by name.
 
        Args:
           organisation:
               An Organisation instance.
        Returns:
           An ordered list of ClassifierGroup instances.
        """
        # Fetch the classifier group QuerySet reverse ordered by family, then ordered by name:
        classifier_groups = self.filter(organisation_id=organisation, family__in=['L', 'S', 'C']).order_by('-family', 'name')
        # Convert the QuerySet to a list of classifier groups:
        listed = list(classifier_groups)
        # return level then subject first (then the custom sorted groups):
        (listed[0], listed[1]) = listed[1], listed[0]
        return listed


class ClassifierGroup(models.Model):
    """
    ClassifierGroups are used by Organisations to group a set of Classifiers
    into a named group.

    These are the objects shown at annotation 3 of the superadmin blueprint
    2.1 - ADD NEW FACULTY.
    """
    FAMILY_CHOICES = (("L", "Level"), ("S", "Subject"), ("C", "Custom"))
    name = models.CharField("A human readable name representing the members of "
            "the group.", max_length=128)
    organisation = models.ForeignKey(Organisation)
    family = models.CharField("Defines the role the type of classification this"
            " group represents. There are two default classification types: "
            "Levels (Part IA, Part IB etc) and Subjects (Computer Science, "
            "Chemistry etc). These two exist by default, but organisations are "
            "free to define their own extra custom classifications. Knowing "
            "that a ClassifierGroup represents a Level or subject allows "
            "distinct groups across different Organisations to be merged for "
            "querying purposes.",
            max_length=1, choices=FAMILY_CHOICES)

    objects = ClassifierGroupManager()

    def to_json(self):
        return {
            "id": self.id,
            "name": self.name,
            "type": self.get_family_display().lower()
        }

    def from_json(self, json):
        pass
        #return new ClassifierGroup(name=json.get("name"), type

    def __unicode__(self):
        return "%s | %s" % (self.name, self.organisation)

class Classifier(models.Model):
    """
    An individual value from a ClassifierGroup, for example "Part IA", or
    "Computer Science". Event groups are tagged with Classifications in order
    to mark them as being associated with a given (Tripos) part or subject.
    """
    parent = models.ForeignKey("Classifier", related_name="children", null=True)
    group = models.ForeignKey(ClassifierGroup)
    value = models.CharField("The value this classification holds.",
            max_length=256)

    def parent_id(self):
        try:
            return self.parent.id
        except:
            return None
        
    def contact_person(self):
        try:
            classification = Classification.objects.get(classifier=self)
            return classification.contact_person
        except:
            return None
        
    def website(self):
        try:
            classification = Classification.objects.get(classifier=self)
            return classification.website
        except:
            return None
    
    def description(self):
        try:
            classification = Classification.objects.get(classifier=self)
            return classification.description
        except:
            return None
    
    
    def __unicode__(self):
        return "%s | %s" % (self.value, self.group)


class Classification(models.Model):
    """
    A Classification associates metadata with a combination of Classifiers
    (i.e. Levels, Subjects, etc). Not necessarily every permutation of
    Classifiers will have a defined Classification, just those combinations
    which are deemed significant.

    These are the objects shown on admin blueprint 2.0 CLASSIFICATION LISTING.
    """
    name = models.CharField(max_length=512)
    website = models.CharField(max_length=1024)
    description = models.TextField()
    contact_person = models.CharField(max_length=1024)
    owner = models.ForeignKey(Organisation)
    classifier = models.OneToOneField(Classifier, related_name="classification", null=True) # The classifier that this 
    
    def __unicode__(self):
        return self.name



class CollisionEvents(models.Model):
    """
    Stores the timeslot for events - allows for collision detection.
    One row should be created for each timeslot in each event in the specified event series
    """
    series = models.ForeignKey( "EventSeries" ) # use for quick reference: CollisionEvents.object.filter( series__in [] )
    event = models.ForeignKey( "Event" ) # because we need to be able to indicate which event clashes with which other event
    timeslot = models.IntegerField()
    
    duration = 900 # duration of the timeslot (currently 15 minutes) in seconds
    
    def __unicode__(self):
        return "%s, %s" % ( self.event, self.timeslot )
    
    class Meta:
        verbose_name_plural = "Collision events" # avoids irritating behaviour of Django admin where an extra "s" is added to end of table name: "Collision Eventss". Meh.
    
    def save(self, *args, **kwargs):
        """
        Override default save - ensure that the EventSeries is consistent with the event.
        timeslot should be set by calling method 
        """
        self.series = self.event.owning_series
        super(CollisionEvents, self).save(*args, **kwargs)
    
    @staticmethod
    def get_timeslot(t):
        """
        Return the timeslot in which time t falls.
        Timeslot is integer value calculated as the number of durations since the epoch.
        t should be passed as a second value.
        """
        ts = int(math.floor(t / CollisionEvents.duration))
        return ts
    
    @staticmethod
    def get_timeslots(t_start, t_end):
        """
        Return the range of timeslots into which the time between t_start and t_end fall.
        t_start and t_end should be passed as second values.
        """
        ts_start = CollisionEvents.get_timeslot(t_start)
        ts_end = CollisionEvents.get_timeslot(t_end)
        return range( ts_start, ts_end+1 )
        

class AbstractGroupUsage(models.Model):
    """
    Represents the use of an EventGroup by an Organisation. This usage
    relationship either implies the group is owned by an Organisation, or that
    the group has been borrowed by an organisation.
    """
    organisation = models.ForeignKey(Organisation)
    classifiers = models.ManyToManyField(Classifier)

    def levels(self):
        return self.classifiers.filter(group__family="L")

    def subjects(self):
        return self.classifiers.filter(group__family="S")

    class Meta(object):
        abstract = True


class GroupUsage(AbstractGroupUsage):
    """
    The standard, live GroupUsage, representing the inclusion of an event
    group within a faculty under a set of classifiers.
    """
    group = models.ForeignKey("EventGroup")
    
    def __unicode__(self):
        return "%s | %s" % (self.organisation, self.group)



class GroupUsageVersion(AbstractGroupUsage):
    """
    A GroupUsage implementation which represents archived useage of event
    groups. It references (versioned) EventGroupVersions instead of (live)
    EventGroups.
    """
    group = models.ForeignKey("EventGroupVersion")
    current_version = models.ForeignKey(GroupUsage, related_name="versions")

# Max length used for series and event titles
MAX_TITLE_LENGTH = 512

class AbstractEventGroup(models.Model):
    '''
    Represents a group of associated EventSequences. A group is owned by a
    single Organisation and can be borrowed by arbitrarily many others.
    '''
    PUBLICATION_CHOICE = (
        ('P', 'Published'),
        ('U', 'UnPublished'),
    )
    name = models.CharField(max_length=MAX_TITLE_LENGTH)
    publication_status = models.CharField(max_length=1,
            choices=PUBLICATION_CHOICE,
            help_text='Publication Status',
            db_column="p_status")
    embargo_date = models.DateField("The Embargo date on publication", null=True)
    owning_organisation = models.ForeignKey(Organisation, help_text="A single "
            "organisation owns any given group.")
    code = models.CharField(max_length=64, help_text="code for the series, might be from Dan's coding system")
    term = models.ForeignKey(Term)
    description = models.TextField('Description of the EventGroup', null=True)

    default_pattern = models.CharField("The default date/time pattern to use "
            "when expanding child sequences.", max_length=256)
    default_location = models.ForeignKey(Location, help_text="The default "
            "location given to child sequences unless they provide their own.")

    # A series/event's type is defined at the group level, yet a series/event
    # can be included in potentially several groups which introduces a
    # contradiction. In order to resolve this, the type of an event/series is
    # defined by the owning group, not the group its borrowed into. This also
    # applies to terms.
    event_type = models.ForeignKey(EventType)

    is_deleted = models.BooleanField("Rather than actually removing rows which "
            "are deleted, they should be marked as deleted with this field. "
            "This is so that borrowers referencing a group do not have the "
            "record of their borrow removed by a cascading removal of objects "
            "referencing a deleted group. Borrowers of events can then also "
            "provide notifications to the user that a borrowed group has been "
            "removed, and the borrowed group can be restored if it is later "
            "resurrected.", db_index=True)

    def timetables(self):
        return self.usages.all();

    class Meta(object):
        abstract = True

class EventGroup(AbstractEventGroup):
    usages = models.ManyToManyField(Organisation, related_name="used_groups",
            through="GroupUsage")

    def getPublicationStatus(self):
        for c, v in self.PUBLICATION_CHOICE:
            if c == self.publication_status:
                return v
        return self.publication_status

    def __unicode__(self):
        return self.name

    def events(self):
        return Event.objects.under_group(self)

    def series(self):
        return EventSeries.objects.under_group(self)

    def lecturers(self):
        lecturers = []
        for series in self.series():
            lecturers_series = series.lecturers()
            for lecturer in lecturers_series:
                if lecturer not in lecturers:
                    lecturers.append( lecturer )
        return lecturers

    def save_version(self, user, comment):
        '''
        Create a new version from the current version.
        Returns the new version
        Also versions EventSeries objects but does not version Events.
        :param user: the user creating the version
        :param comment: a comment.
        '''
        # This is not great, it would be better to use the id of the revision.
        current_revision = Revision.objects.all().aggregate(Max('version'))['version__max']
        if current_revision is None:
            current_revision = 0
        else:
            current_revision = current_revision + 1
        revision = Revision.objects.create(
                    version=current_revision,
                    comment=comment,
                    user=User.objects.get(username=user.username),
                    organisation=self.owning_organisation)
        group_version = EventGroupVersion.objects.create(
                                current_version=self,
                                revision=revision,
                                name=self.name,
                                publication_status=self.publication_status,
                                embargo_date=self.embargo_date,
                                owning_organisation=self.owning_organisation,
                                code=self.code,
                                term=self.term,
                                description=self.description,
                                default_pattern=self.default_pattern,
                                default_location=self.default_location,
                                event_type=self.event_type,
                                is_deleted=self.is_deleted
                                )
        for group_usage in GroupUsage.objects.filter(group=self):
            group_usage_version = GroupUsageVersion.objects.create(
                     organisation=group_usage.organisation,
                     group=group_version,
                     current_version=group_usage
                    )
            for c in group_usage.classifiers.all():
                group_usage_version.classifiers.add(c)
            group_usage_version.save()

        for es in self.owned_series.all():
            es.save_version(group_version)

        GroupRevision.objects.create(
                    usage_version=group_usage_version,
                    enclosing_revision=revision,
                    group_version=group_version)
        return group_version


class EventGroupVersion(AbstractEventGroup):
    '''
    Holds previous versions of EventGroups
    '''
    usages = models.ManyToManyField(Organisation, through="GroupUsageVersion",
            help_text="", related_name="versioned_used_groups")
    current_version = models.ForeignKey(EventGroup,
            help_text="The current version of this event group, to enable "
            "queries of past versions", related_name="versions",
            db_column="c_v")
    revision = models.ForeignKey("Revision")

    def revert(self, user):
        self.current_version.save_version(user,"Reverting EventGroup %s Revision %s " % (self.name, self.revision.id))
        self.current_version.name = self.name
        self.current_version.publication_status = self.publication_status
        self.current_version.embargo_date = self.embargo_date
        self.current_version.owning_organisation = self.owning_organisation
        self.current_version.code = self.code
        self.current_version.term = self.term
        self.current_version.description = self.description
        self.current_version.default_pattern = self.default_pattern
        self.current_version.default_location = self.default_location
        self.current_version.event_type = self.event_type
        self.current_version.is_deleted = self.is_deleted
        # delete all usages
        GroupUsage.objects.filter(group=self.current_version).delete()

        for group_usage_version in GroupUsageVersion.objects.filter(group=self):
            group_usage = GroupUsage.objects.create(
                     organisation=group_usage_version.orgnanisation,
                     group=self.current_version
                    )
            for classifier in group_usage_version.classifiers.all():
                group_usage.classifiers.add(classifier)
            group_usage.save()

        # delete all events and event series
        Event.objects.filter(owning_series__owning_group=self.current_version).delete()
        EventSeries.objects.filter(owning_group=self.current_version).delete()

        # create new event series
        for event_series_version in EventSeriesVersion.objects.filter(owning_group=self):
            event_series_version.revert(self.current_version)

        self.current_version.save()
        # the caller should now republish all the events.



class AbstractEventSeries(models.Model):
    """"""
    usages = models.ManyToManyField(EventGroup, db_column="e_g",
            help_text="EventSeries are part of one or more EventGroup. "
            "(i.e. may be shared)")
    title = models.CharField("The title of the series",
            max_length=MAX_TITLE_LENGTH)
    date_time_pattern = models.CharField("Definition of the time of a lecture, "
            "eg Ea1-8 Sa 9, as per Dans model", max_length=256, db_column='t_s')
    location = models.ForeignKey(Location, help_text="The (default) location "
            "the events in this series take place in.")
    associated_staff = models.ManyToManyField(User, help_text="CRSIDs of people"
            " associated with the event, e.g. the lecturer(s) or teachers.")
    is_deleted = models.BooleanField("Setting to True marks the series as being"
            " deleted without physically removing the row.", db_index=True)
    
    can_borrow = models.BigIntegerField("Stetting to false prevents borrowing of this series", default=True)

    # This might not be the right place for this.
    external_url = models.CharField("External Url for timetable Serise ", max_length=2048, null=True)

    last_modified = models.DateTimeField(auto_now=True, auto_now_add=True)

    class Meta(object):
        abstract = True

class EventSeriesManager(models.Manager):
    def under_group(self, group):
        # We should be using eventseries_set.all() but event series are not
        # added to that at present
        return group.owned_series.all()

class EventSeries(AbstractEventSeries):
    '''
    An EventSeries represents a lecture series, or any other group of
    associated events.
    '''
    owning_group = models.ForeignKey(EventGroup, related_name="owned_series")

    objects = EventSeriesManager()

    def get_location(self):
        try:
            return self.location or self.owning_group.default_location
        except:
            return ""

    def get_date(self):
        try:
            return self.date_time_pattern or self.owning_group.default_pattern
        except:
            return ""

    def get_title(self):
        return self.title or ""


    def get_staff_as_string(self):
        try:
            return ";".join([u.username for u in  self.associated_staff.all()])
        except:
            log.error(traceback.format_exc())
            return ""

    def lecturers(self):
        """
        gets list of all staff, including those in events
        """
        lecturers = self.associated_staff.all()
        events = Event.objects.filter( owning_series = self )
        for event in events:
            lecturers_event = [] # to contain lecturers for individual event (if overridden) but not used publicly
            if event.is_staff_overridden:
                for lecturer in event.overridden_staff.all():
                    lecturers_event.append( lecturer )
                for lecturer in lecturers_event: # can also use set.union for one line of code, but you end up swapping between sets and lists a lot later
                    if lecturer not in lecturers:
                        lecturers.append(lecturer)
        return lecturers

    class Meta(object):
        verbose_name = "Event Series"
        verbose_name_plural = verbose_name # series is plural of series!

    def __unicode__(self):
        return self.title

    def save_version(self, group_version):
        '''
        Create a new version associating the new version with the supplied group_version
        return that new verion as an EventSeriesVersion.
        Does not version the events.
        :param group_version:
        '''
        event_series = EventSeriesVersion.objects.create(
                                owning_group=group_version,
                                current_version=self,
                                title=self.title,
                                date_time_pattern=self.date_time_pattern,
                                location=self.location,
                                is_deleted=self.is_deleted,
                                can_borrow=self.can_borrow,
                                external_url=self.external_url)
        for user in self.associated_staff.all():
            event_series.associated_staff.add(user)
        for eventgroup in self.usages.all():
            event_series.usages.add(eventgroup)
        event_series.save()
        return event_series



class EventSeriesVersion(AbstractEventSeries):
    '''
    Holds previous versions of Event Series
    '''
    owning_group = models.ForeignKey(EventGroupVersion, related_name="versioned_owned_series")
    current_version = models.ForeignKey(EventSeries,
            help_text="The current version of this event sequence, to enable queries of past versions",
            related_name="versions",
            db_column="c_v")

    def revert(self, group):
        event_series = EventSeries.objects.create(
                        owning_group=group,
                        title=self.title,
                        date_time_pattern=self.date_time_pattern,
                        location=self.location,
                        is_deleted=self.is_deleted,
                        external_url=self.external_url)
        for user in self.associated_staff.all():
            event_series.associated_staff.add(user)
        for eventgroup in self.usages.all():
            event_series.usages.add(eventgroup)
        event_series.save()
        return event_series


class TimeInterval(models.Model):
    """
    Represents a period of time that an event occurs over.
    """
    start_time = models.DateTimeField(help_text='Start of the event')
    end_time = models.DateTimeField(help_text='End of the event')

    def get_duration(self):
        """
        Gets a datetime.timedelta representing the length of this interval.
        """
        start, end = self.start_time, self.end_time
        return end - start
    
    def _get_total_seconds(self, td):
        '''
        get total seconds avoiding 2.7 only method.
        :param td:
        '''
        return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6

    def get_duration_hours(self, as_int=True):
        hours = self._get_total_seconds(self.get_duration()) / float(60 * 60)
        return int(hours) if as_int else hours

    def get_duration_minutes(self, as_int=True):
        remaining_secs = (abs(self._get_total_seconds(self.get_duration())) % (60 * 60))
        minutes = remaining_secs / 60.0
        minutes = math.copysign(minutes, self._get_total_seconds(self.get_duration()))
        return int(minutes) if as_int else minutes

    def clean(self):
        if self.start_time > self.end_time:
            raise ValidationError("Start time must be before or equal to end "
                    "time.")

    def __unicode__(self):
        return u"start: %s, duration: %02dh%02dm" % (self.start_time,
                self.get_duration_hours(), self.get_duration_minutes())


class AbstractEvent(models.Model):
    """
    An event is a constituent part of an event series. Events are individual
    intervals of time, with extra meta data attached, such as the title,
    location etc.
    """
    # Fields provided automatically via parent series:
    position = models.IntegerField("The 0 based index of the Event within its "
            "parent EventSequence.")

    # The timeslot could be null if say a count is given with no default pattern
    # The override field would then be used to provide a value
    default_timeslot = models.ForeignKey(TimeInterval, null=True,
            # Don't create back relation accessor on TimeInterval
            related_name="+")

    # Optional fields supplied by parent series if null, or by value if
    # provided.
    overridden_title = models.CharField(max_length=MAX_TITLE_LENGTH, null=True, default=None)
    # Need an extra flag to mark if the staff have been overridden, as we can't
    # really null a many to many field...
    is_staff_overridden = models.BooleanField("True if we're providing the list"
            " of staff here, False if the parent series's staff should be "
            "used.")
    overridden_staff = models.ManyToManyField(User, null=True)
    overridden_location = models.ForeignKey(Location, null=True)
    
    staff = None # Used for in memory generated events
    location = None
    title = None

    combination_set_id = models.IntegerField("In order to support HEAD's"
            "'Combine' feature we need to be able to associate events within a"
            " series with each other. Events with the same combination_set_id "
            "are taken to be 'combined'. I'm not sure why this even exists, as "
            "HEAD's design for the student UI seems to have a whole series as "
            "the smallest unit of addition to a student timetable. This can be "
            "given the same value as the position field to not combine events.")

    # very important that this is only updated when something overrides and not when 
    # the event is published.
    #last_modified = models.DateTimeField(auto_now=False, auto_now_add=False, default="2000-01-01 01:01:00Z")
    last_modified = models.DateTimeField(auto_now=False, auto_now_add=False, default=datetime(2000, 1, 1, 1, 1, 0))

    def position1(self):
        """
        Gets the 1-based index of this event in its series.
        """
        return self.position + 1

    def get_title(self):
        return self.title or self.overridden_title or self.owning_series.title

    def get_timeslot(self):
        return self.default_timeslot

    def get_staff(self):
        """
        Gets a queryset of the Users associated with this event.
        """
        try:
            return self.staff or (self.overridden_staff.all() if self.is_staff_overridden else
                self.owning_series.associated_staff.all())
        except:
            return self.staff

    def get_location(self):
        try:
            return self.location or self.overridden_location or self.owning_series.location
        except:
            return self.location

    def get_series_length(self):
        """
        Gets the number of events in the series this event is part of.
        
        By calling include_series_length() when querying for events, this count
        can be calculated in the initial query, rather than on a per-event
        basis. i.e:
        
            >>> events = Event.objects.all().include_series_length()[:5]
            >>> [e.get_series_length() for e in events]
            [96, 96, 96, 96, 96]
            >>> len(connection.queries)
            1
            >>> connection.queries = []
            >>> events = Event.objects.all()[:5]
            >>>  [e.get_series_length() for e in events]
            [96, 96, 96, 96, 96]
            >>> len(connection.queries)
            11
        """
        # If the include_series_length() method is used on the Event's queryset
        # there'll be a property on the returned events containing the number
        # of events in the owning series which we can use.
        if hasattr(self, "owning_series__event__count"):
            return self.owning_series__event__count
        # Otherwise default to querying for it
        return self.owning_series.event_set.count()

    class Meta(object):
        abstract = True

    def __unicode__(self):
        return (u"%s[%d](title: %s, start: %s, length: %02dh%02dm, "
                "location: %s, staff: [%s])" % (
                self.__class__.__name__,
                self.position,
                self.get_title(),
                self.get_timeslot().start_time,
                self.get_timeslot().get_duration_hours(),
                self.get_timeslot().get_duration_minutes(),
                self.get_location(),
                ", ".join(u.username for u in self.get_staff())))


class EventManager(QuerySetManager):
    def under_group(self, group):
        return self.filter(owning_series__in=
                EventSeries.objects.under_group(group))

class Event(AbstractEvent):
    '''
    Represents events which are live, rather than old versions.
    '''

    owning_series = models.ForeignKey(EventSeries,
            help_text='Link to the record that specifies this event',
            db_column="e_s")

    objects = EventManager()
    
    class QuerySet(QuerySet):
        def in_users_timetable(self, user, year=None):
            """
            Filter the queryset to contain events in the personal timetable of
            the specified user, optionally restricted to a specific academic
            year.
            
            Args:
                user: The username or User instance of the user whose timetable
                    the events should be in.
                year: The starting year of an Academic year. Only events from 
                    the specified year's personal timetable will be returned.
                    Events from all year's personal timetables will be returned
                    if not specified.
            """
            if isinstance(user, basestring):
                qs = self.filter(
                        owning_series__personaltimetableusage__timetable__owner__username=user)
            else:
                qs = self.filter(
                        owning_series__personaltimetableusage__timetable__owner=user)
            
            # Restrict to a year if one is specified. 
            if year is not None:
                qs = qs.filter(owning_series__personaltimetableusage__timetable__year__starting_year=year)
            
            return qs
        
        def in_range(self, start, end):
            """
            Filters the queryset to contain events intersecting the specified
            start and end dates.
            """
            if start > end:
                raise ValueError(
                        "start was > end. start: %s, end: %s" % (start, end))
            
            # Note: it's assumed that event's timeslots's start & end are
            # correctly sorted...
            ends_before_start= Q(default_timeslot__end_time__lte=start)
            starts_after_end= Q(default_timeslot__start_time__gte=end)
            
            outside_range = ends_before_start | starts_after_end
            inside_range = ~outside_range
            
            return self.filter(inside_range)
        
        def include_series_length(self):
            return self.annotate(Count("owning_series__event"))


class EventVersion(AbstractEvent):
    '''
    Holds previous versions of Events.
    '''
    owning_series = models.ForeignKey(EventSeriesVersion,
            help_text='Link to the record that specifies this event',
            db_column="e_s")
    current_version = models.ForeignKey(Event, related_name="versions",
            help_text="The corresponding live version of this versioned event.")


class Revision(models.Model):
    """Represents an entry in the admin Change History screen."""
    # FK on EventGroupVersion to link into revision
    # 
    """ The version number must be explicit and immutable to ensure that
    all pointers to it can be relied upon. Relying on an inferred version number
    will make the version number inaccessible to query sets that don't contain the
    correct data and are not ordered correctly and will be susceptible to deletions """
    # Note: Why not just use the ID of the record ?
    # Having duplicate IDs via GroupRevision will be almost impossible to enforce.
    version = models.IntegerField(help_text="Explicit revision number")
    created = models.DateTimeField(auto_now=True)
    comment = models.TextField(help_text="User supplied comment")
    user = models.ForeignKey(User, help_text="The user that created the version")
    organisation = models.ForeignKey(Organisation,
            help_text="The organisation that the change was made for. The "
            "contents of a revision should be restricted to groups which are "
            "owned by this organisation.")


class GroupRevision(models.Model):
    """A GroupRevision is a part of a Revision which effects only a single
    group. Revisions may represent bulk edits to multiple groups by being
    comprised of multiple GroupRevision instances to represent each group which
    was changed.

    Only organisations owning a group are able to edit the group itself, others
    are only able to borrow the group, which is achieved by associating the
    group to the organisation with a GroupUsage object. Edits to just these
    usage classifers are type U. Edits to the group itself, whether or not they
    update the usage at the same time are type G.
    """
    enclosing_revision = models.ForeignKey(Revision)
    # usage_version is never null, as group revisions always snapshot the usage
    # state. This is in order to allow the usage classifiers to be reverted
    # when reverting a group's state to an old version.
    usage_version = models.ForeignKey(GroupUsageVersion)
    # A group is not necessarily changed by a group revision. For example, 
    # borrowing organisations are not able to edit the groups they borrow, but
    # can edit the classifiers the group is borrowed under.
    group_version = models.ForeignKey(EventGroupVersion, null=True)

class PersonalTimetableManager(models.Manager):
    
    def for_user(self, user):
        return self.filter(user=user)
    
    def for_active_year(self):
        return self.filter(year=AcademicYear.get_current())

class PersonalTimetable(models.Model):
    """
    Represents a student's personal timetable for a given year.
    An entry is created when a student adds an event series to
    their custom timetable.
    """
    year = models.ForeignKey(AcademicYear,
            help_text="The year the timetable is in.")
    owner = models.ForeignKey(DjangoUser)
    
    objects = PersonalTimetableManager()
    
    def __unicode__(self):
        return u"%s | %s" % (self.owner, self.year)

class PersonalTimetableUsage(models.Model):
    """
    Represents items in a student's personal timetable.
    Entries must be unique across timetable / groupusage / event series.
    This means you can add the same event series from different group
    usages, which could happen with borrowing. There may be some implications
    for the UI with regard to dealing with such duplications:
    """
    timetable = models.ForeignKey(PersonalTimetable,
            help_text="The timetable containing the items.")
    group = models.ForeignKey(GroupUsage,
            help_text="The group usage for the event series added")
    series = models.ForeignKey(EventSeries,
            help_text="The event series added")
    class Meta:
        unique_together = ("timetable", "group", "series")

    def __unicode__(self):
        return u"%s | %s" % (self.timetable, self.group)

    # TODO: Add an additional check here that ensures the series
    # being added is actually a member of the given group. (See "clean"
    # methods)
        
