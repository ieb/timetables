from django.test import TestCase
from timetables.model.models import Permission, RoleManager, TimeInterval
from itertools import combinations

class RoleGenerationTest(TestCase):

    def test_permission_permutations_hash_to_distinct_names(self):
        permission_names = set(p.name for p in Permission.objects.all())
        self.assertTrue(permission_names, "Some permissions should be present")

        combo_names = []
        for count in range(1, len(permission_names)):
            for combo in combinations(permission_names, count):
                combo_names.append(RoleManager.uniqueness_hash(set(combo)))
        self.assertTrue(combo_names, "Some names should be present")
        self.assertEqual(len(combo_names), len(set(combo_names)),
                "Expected %d distinct names, but got: %d. Values: %s" % (
                        len(combo_names), len(set(combo_names)), combo_names))

class TimeIntervalTest(TestCase):

    def test_duration(self):
        ti = TimeInterval(
                start_time="2012-01-01T12:00",
                end_time="2012-01-01T13:35")
        ti.full_clean()

        self.assertEqual(ti.get_duration_hours(), 1)
        self.assertEqual(ti.get_duration_hours(as_int=False),
                (60 * 60 + 60 * 35) / (60 * 60.0))

        self.assertEqual(ti.get_duration_minutes(), 35)
        self.assertEqual(ti.get_duration_minutes(as_int=False),
                35.0)

    def test_duration_2(self):
        # 30 seconds
        ti = TimeInterval(
                start_time="2012-01-01T12:00",
                end_time="2012-01-01T12:00:30")
        ti.full_clean()

        self.assertEqual(ti.get_duration_minutes(), 0)
        self.assertEqual(ti.get_duration_minutes(as_int=False), 0.5)
