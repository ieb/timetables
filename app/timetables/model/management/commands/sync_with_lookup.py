import sys

from django.core.management.base import BaseCommand

from timetables.model.models import User
from timetables.model import lookup_synchronisation

# Why doesn't Django use argparse? :(
from optparse import make_option

class Command(BaseCommand):

    help = """Synchronise timetables users with lookup.cam.ac.uk.

Unless -o/--only or -d/--defer are used, all users existing in the timetables
database are synchronised. --batch-size can be used to control the number of
users to fetch from the db & lookup in one go.
"""
    option_list = BaseCommand.option_list + (
        make_option("-o", "--only", action="append", dest="only",
                metavar="crsid", help="Sync only the user specified. Can be "
                "provided multiple times to specify multiple users to sync."),
        make_option("-d", "--defer", action="append", dest="defer",
                metavar="crsid", help="Don't sync the specified user. Can be "
                "provided multiple times to skip multiple users."),
        make_option("--batch-size", type="int", dest="batch_size",
                metavar="count", help="The number of users to fetch from the "
                "database and lookup.cam at once. default: 256"),

    )

    DEFAULT_BATCH_SIZE = 256

    def msg(self, msg, v=1, dest=sys.stderr):
        """
        Prints a message conditionally based on verbosity level.
        """
        if self.verbosity_level >= v:
            # Quick hack for lazily evaluated messages by prefixing with lambda:
            if callable(msg):
                msg = msg()
            print >> dest, msg

    def on_user_synched(self, django_user=None, lookup_user=None, **kwargs):
        self.msg('synched: %s, old email: "%s", new email: "%s", old name: '
                '"%s", new name: "%s"' % (django_user.username,
                        django_user.email, lookup_user.email,
                        django_user.last_name, lookup_user.name), v=2)
        self.sync_count += 1

    def handle(self, *args, **options):
        self.verbosity_level = int(options["verbosity"])

        only = options["only"]
        defer = options["defer"]
        batch_size = options["batch_size"] or self.DEFAULT_BATCH_SIZE

        self.msg("using batch size: %d" % batch_size, v=2)

        qs = self.get_user_qs(only, defer)
        self.msg(lambda: "%d users to synchronise" % qs.count(), v=2)
        self.msg(lambda: "users: %s" % ", ".join(u.username for u in qs), v=3)

        self.sync_count = 0
        # Register a function to be called when a user is synched. We'll use
        # this to log a message for each sync and keep a counter
        lookup_synchronisation.pre_user_lookup_sync.connect(
                self.on_user_synched)

        lookup_synchronisation.bulk_synchronise_users(qs, batch_size)
        self.msg(lambda: "finished; %d of %d users updated." %
                (self.sync_count, qs.count()), v=1)

    def get_user_qs(self, only, defer):
        if only:
            qs = User.objects.filter(username__in=only)
        else:
            qs = User.objects.all()

        if defer:
            qs = qs.exclude(username__in=defer)

        # Only fetch required fields
        qs = qs.only("id", "username", "email", "last_name")
        return qs
