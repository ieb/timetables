'''
Created on May 23, 2012

@author: ieb
'''
from django.core.management.base import BaseCommand
from timetables.model.models import EventSeries
import logging
from timetables.utils.v1 import publish

log = logging.getLogger(__name__)
del logging

class Command(BaseCommand):
    args = 'None'
    help = 'Clears all event data and re populates it based on the EventSeriese that are currently loaded.'
    'Does not take any account of versions or of events that have overridden data in the Event Series'


    def handle(self, *args, **options):
        
        n_event_series = 0
        n_events = 0
        n_new_intervals = 0
        for event_series in EventSeries.objects.all():
            n_event_series = n_event_series + 1
            ni, ne = publish(event_series)
            n_new_intervals = n_new_intervals + ni
            n_events = n_events + ne
            log.info("Processing %s %s produced %s events" % (event_series.title, event_series.date_time_pattern, ne))
        log.info("Processing of %s series complete generated %s events. New time intervals %s " % (n_event_series, n_events, n_new_intervals))



