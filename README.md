# Timetables


This code base is the timetables code base.

At present license has not been determined and code should be assumed to be 
proprietary.

# Source folders

This code base is structured for deployment and may not follow an absolutely standard DJango layout.

        app - contains the DJango Timetables application code, test classes and client classes.
        app/templates - contains the templates for the application.
        apache - contains Apache HTTPD configuration files
        app-data - is where the application writes user data
        htdocs - the wsgi script loaded by Apache HTTPD
        static-data - additional static data including a checkout of the OAE UI code
        README.md - This file.

# To develop

Ensure that you have Python 2.x >= 2.6 installed. If you are on a OSX, then the default Python is Ok.

If you are on ubuntu, then you will likely need to do the following:

        $ sudo apt-get install libsasl2-dev python2.7-dev libldap2-dev postgresql-server-dev-9.1

Install project dependencies using pip and the requirements.txt file in the project root:

        $ sudo pip install -r requirements.txt
        
Having done that, create a database

        cd app/timetables
        python manage.py syncdb
        
Answer a few questions about the default super user.

Configure a suitable environment for development work:

        create app/timetables/local_settings.py containing:

        # Disable the Query Cache to dev work
        DISABLE_QUERYSET_CACHE = True
        # Enable default data on Add forms to make it quicker to do Developer QA.
        TEST_DEFAULT_DATA = True

Load in sample data:

        python manage.py load_caldata ../../../timetables-2012datafolder list
        python manage.py load_caldata ../../../timetables-2012datafolder "Natural Sciences Tripos::IA"
        
Start the Server

        cd app/timetables
        python manage.py runserver
        open http://localhost:8000


# To deploy

Use apache mod_wsgi in any mode, however it will perform better running Apache HTTPD in event mode, with multiple threads
per mod_wsgi process. DJango is thread safe and can be run like this.

The application is assumed to be in /var/www/timetables.localhost. If it was not checked out there a symbolic link can be used.

There is a configuration file in apache/timetables.conf that can be included in an Apache HTTPD configuration file.

        Include /var/www/timetables.localhost/apache/timetables.conf

Collect the static resources, into static-data/staticroot so that Apache HTTPD can serve the files directly.

        cd app/timetables
        python manage.py collectstatic
        

Further Deployment instructions at https://docs.djangoproject.com/en/dev/howto/deployment/wsgi/modwsgi/


# File system permissions

The application will need to write to app-data/. It may need to write .pyc
files to the app/ folder. It will need to read from other folders. An operator
on the command line may need to write to static-data to collect static files.


