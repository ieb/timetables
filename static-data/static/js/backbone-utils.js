define(["backbone"], function(Backbone) {
    
    /**
     * Gets the superclass of a Backbone style class.
     * 
     * This can be used to call a static method on a superclass without hard 
     * coding the superclass, for example to call through to the superclass 
     * constructor:
     * 
     *     superclass(CurrentClass).apply(this, arguments);
     */
    function superclass(Class) {
        return Object.getPrototypeOf(Class.prototype).constructor;
    }
    
    /**
     * Gets the prototype object of the superclass of a Backbone style class.
     * 
     * This can be used to call the superclass's implementation of an instance
     * method. For example, to call the superclass implementation of initialize
     * you can do:
     * 
     *     superproto(CurrentClass).initialize.call(this);
     */
    function superproto(Class) {
        return superclass(Class).prototype;
    }
    
    return {
        superclass: superclass,
        superproto: superproto
    };
});