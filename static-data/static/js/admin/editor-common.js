define(["jquery", 
        "admin/validation",
        "admin/datepattern",
        "autocomplete",
        "domReady", 
        "admin/classification-selectors",
        "jquery-ui/dialog", 
        "jquery-ui/sortable"], 
        function($,validation, datepattern, autocomplete){
	"use strict";
	// console.log("editor-common run");
	
	// add a validation type for empty date patterns.
	validation.add(".validation_date_pattern_message_empty_ok",function(element, feedback, pattern_ok_func, pattern_bad_func) {
		var val = element.val().trim();
        if ( val.length === 0 ) {
        	pattern_ok_func()
        } else {
        	validation.chain(".validation_date_pattern_message", element, feedback, pattern_ok_func, pattern_bad_func);
        }
	});
	
	// add a validation type for empty date pattern if default present.
	validation.add(".validation_date_pattern_message_if_no_default", function(element, feedback, pattern_ok_func, pattern_bad_func) {
		// find the scope, then find the named default and check its not empty, if its not empty, Ok, otherwise validate this element.
		var scope = $(element).parents("."+$(feedback).attr("default-scope"));
		var defaultValue = $(scope).find("[name="+$(feedback).attr("default-name")+"]").val().trim();
        var val = element.val().trim();
		if (defaultValue.length > 0 ) {
			if ( val.length > 0 ) {
				validation.chain(".validation_date_pattern_message", element, feedback, pattern_ok_func, pattern_bad_func);
			} else {
				// default is full and value is empty, ok
				pattern_ok_func();
			}
		} else {
			validation.chain(".validation_date_pattern_message", element, feedback, pattern_ok_func, pattern_bad_func);
		}
	});
	
	// add a validation pattern for not emptry if no default.
	validation.add(".validation_not_empty_if_no_default", function(element, feedback, pattern_ok_func, pattern_bad_func){
		// find the scope, then find the named default and check its not empty, if its not empty, Ok, otherwise validate this element.
		var scope = $(element).parents("."+$(feedback).attr("default-scope"));
		var defaultValue = $(scope).find("[name="+$(feedback).attr("default-name")+"]").val();
		if (defaultValue && defaultValue.length > 0 ) {
			pattern_ok_func();
		} else {
			validation.chain(".validation_not_empty", element, feedback, pattern_ok_func, pattern_bad_func);
		}
	});
	
    $( "#date-dialog" ).dialog({
        autoOpen: false,
        width: 620,
        modal: true
    });

    $('#date-dialog .close-dialog').click(function() {
        $("#date-dialog").dialog("close");
        return false;
    });

    // enable switching between tabs
    $('.admin-tab-group-events input[type="radio"]').change(function(event){
        switch($(this).attr('id')){
            case "open-add-events-tab":
                $('#add-events-tab').show();
                $('#external-link-tab').hide();
                $(this).parent().addClass('selected');
                $('#open-add-external-link-tab').parent().removeClass('selected');
            break;

            case "open-add-external-link-tab":
                $('#add-events-tab').hide();
                $('#external-link-tab').show();
                $(this).parent().addClass('selected');
                $('#open-add-events-tab').parent().removeClass('selected');
            break;

        }
    });
    
    var showDateEditor = function(header) {
    	// make the dateEditor show just after the target fragment using the target fragment
    	// as the source for the date specification
    	var dateFragment = null;
    	if ($(header).hasClass("date-dialog-fragment") ) {
    		dateFragment = header;
    	} else {
        	dateFragment = $(header).parents(".date-dialog-fragment");    		
    	}
    	if ( dateFragment.length !== 1 ) 
    		throw "Should have got a single date fragment from "+header+" got "+dateFragment.length;
    	var editorFragment = $("#date-dialog .date-dialog-fragment-editor");
    	if ( editorFragment.length !== 1 ) 
    		throw "Should have got a single editor fragment";
    	
    	$(dateFragment).after(editorFragment);
    	$("#date-dialog .date-dialog-fragment-header .dates-edit a").show();
    	$(dateFragment).find(".date-dialog-fragment-header .dates-edit a").hide();
    	// populate the form
    	var datePattern = $(dateFragment).find(".date-editor-pattern").text();
    	var dateSpec = datepattern.parse_date_spec(datePattern);
    	// console.log("Loading pattern "+datePattern+" as "+dateSpec);
    	datepattern.update_form(editorFragment, dateSpec[0]);
    	
    	
    };
    
    var showTab = function(rb) {
        var targetContent = "." + $(rb).attr('tab-content');
        $(rb).parents(".radio-btn-tab-group").find(targetContent).show();    	    	
    };
    
    var tabbingController = function() {
        $('.radio-btn-tab-content').hide();
        showTab(this);
    };
    
    // Setup the Select Date Range radio-button-tabs...
    var rangeTabsRadios = $(".radio-btn-tab input:radio");
    // Hide non-active tabs when the radio input changes
    rangeTabsRadios.change(tabbingController);
    
    // Hide all but the first by default (the changed event doesnt fire correctly in Chrome);
    // dont do this, let the template do it. rangeTabsRadios.first().prop('checked', true);
    // showTab(rangeTabsRadios.first());

    var addAnnotherDate = function(dateSpec) {
    	// locate the date dialog fragment
    	// append it,
    	var newFragment = $("#date-dialog .date-dialog-fragment:first").clone()
    	$("#date-dialog .date-dialog-fragment:last").after(newFragment);
    	// add a count
    	$(newFragment).find(".date-editor-n").text($("#date-dialog .date-dialog-fragment").length+". ");
    	$(newFragment).find(".date-editor-pattern").text("");
    	// console.log("Cleared date pattern");
    	if ( dateSpec !== null ) {
    		var dateFormat = datepattern.format_date_spec(dateSpec);
        	$(newFragment).find(".date-editor-pattern").text(dateFormat);
        	// console.log("Setting new date pattern to ["+dateFormat+"]");
    	}
    	// show the header
    	$("#date-dialog .date-dialog-title").show();
    	

    	// move the editor to this possition and initialise
    	
    	// hide all editors
    	// show all headers
    	$("#date-dialog .date-dialog-fragment-header").show();
    	// show all edit buttons
    	$("#date-dialog .date-dialog-fragment-header .dates-edit a").show();
    	$(newFragment).find(".date-dialog-fragment-header .dates-edit a").hide();
    	// bind to the edit link for this fragment
    	$(newFragment).find(".date-dialog-fragment-header .dates-edit a").click(function() {
    		showDateEditor(this);
    	});
    	// show this fragment
    	showDateEditor(newFragment);

   
    };

    // Attache to the edit button..
	$("#date-dialog .date-dialog-fragment-header .dates-edit a").click(function() {
		showDateEditor(this);
	});
    // Attache add another date to the button.
    $('#date-dialog .add-dialog').click(function() {
    	addAnnotherDate(null);
    });
    
    // Attach the change handler for the date editor
    $('#date-dialog .date-dialog-fragment-editor').change(function() {
    	var datespec = datepattern.create_date_spec($(this));
    	var newDatePattern = datepattern.format_date_spec(datespec);
    	$(this).prev().find(".date-editor-pattern").text(newDatePattern);
    });


    var openDateDialog = function(target) {
    	/*
    	 * There is one date dialog, that may contain many date dialog fragments
    	 * Each fragment represents one date element.
    	 * When the form comes up we remove all but the first fragment editor
    	 * Then we set the value of the first fragment.
    	 * And the contents of the form
    	 * The user can trigger adding new fragments which are blank initially, and are then
    	 * loaded with the pattern that is supplied in the text span.
    	 * So we have 1 form serving multiple specifications
    	 */
    	$("#date-dialog .date-dialog-fragment:first").addClass("_first");
    	$("#date-dialog .date-dialog-fragment").each(function() {
    		if ( !$(this).hasClass("_first") ) {
    			$(this).remove();
    		}
    	});
    	$("#date-dialog .date-dialog-fragment").removeClass("_first");

    	$("#date-dialog .date-dialog-title").hide();
    	$("#date-dialog .date-dialog-fragment-header").hide();
    	$("#date-dialog .date-dialog-fragment").show();
    	$("#date-dialog .date-dialog-fragment-editor").show();
    	
    	$("#date-dialog").find("form").each(function() {
    		this.reset();
    	});
    	var group = $(this).parents('.add-events-form');
    	var groupid = $(group).find('[name=group]').val();
    	var termid = $(group).find('[name=term_'+groupid+']').val();
    	$("#date-dialog").find('[name=term]').val(termid);
    	$('#date-dialog .error-message').hide();
    	var datespec_val = $(target).prev("input").val();
    	
    	var datespec = datepattern.parse_date_spec($(target).prev("input").val());
		$("#date-dialog .date-dialog-fragment:first .date-editor-n ").text("1. ");
		$("#date-dialog .date-dialog-fragment:first .date-editor-pattern ").text("");
		// console.log("Preparing form with "+datespec.length+" entries from ["+datespec_val+"]");
    	for ( var i = 0; i < datespec.length; i++ ) {
    		if ( i > 0 ) {
        		addAnnotherDate(datespec[i]);    			
    		} else {
        		var dateFormat = datepattern.format_date_spec(datespec[i]);
    			// console.log("Setting first date to  "+dateFormat);
        		$("#date-dialog .date-dialog-fragment:first .date-editor-n ").text((i+1)+". ");
        		$("#date-dialog .date-dialog-fragment:first .date-editor-pattern ").text(dateFormat);
    		}
    	}
		// console.log("Done Preparing form with "+datespec.length+" entries");
    	showDateEditor($("#date-dialog .date-dialog-fragment:first"));
    	
        $("#date-dialog").dialog("open");
        $('#date-dialog .save-dialog').click(function() {
            // build the string and save the value
        	// TODO: make this work for multiple patterns.
        	var datePattern = new Array();
        	$("#date-dialog .date-editor-pattern ").each(function() {
        		datePattern.push($(this).text());
        	});
        	$(target).prev("input").val(datePattern.join(';'));
            $("#date-dialog").dialog("close");
            $('#date-dialog .save-dialog').unbind('click');
            // remove the click handler when we are done to avoid scope issues.
            return false;
        });
    }


    var bindDateDialog = function(target) {
    	target.find("a.ic-import-date").each(function(){
    		if ( ! $(this).hasClass("dialog_is_bound") ) {
        		$(this).click(function() {
    	    		openDateDialog(this);
    	            // We need to return false to stop the browser following the href of the <a>
    	            return false;
        		});
    			$(this).addClass("dialog_is_bound");
    		}
        });

    }

    // lazy loads events based on nearest containing event series.
    // the source must be inside a tr.series and have a tr.event following it.
    var update_events = function(source) {
    	var series_container = $(source).parents(".series");
    	var group_container = $(source).parents(".group_container_marker");
    	// get the series id from the series group container, the input field should be annotated
    	// with the class .series_id_marker, if its not, then the series is new.
    	var series_id = $(series_container).find(".series_id_marker");
    	if ( series_id.length > 0 ) {
    		series_id = $(series_id).val();
    	} else {
    		series_id = "new";
    	}

    	var qparams = {}
    	// get the pattern first trying the series, then trying the group if nothing has been defined in the series
    	var series_pattern = $(series_container).find("[name^=series_date]");
    	if ( series_pattern.length > 0 ) {
    		qparams["p"] = $(series_pattern).val();
        } else {
        	qparams["p"] = "";
    	}
    	var title = $(series_container).find("[name^=series_title]");
    	if ( title.length > 0 ) {
    		qparams["t"] = $(title).val();
        } else {
        	qparams["t"] = "";
    	}
    	var lecture = $(series_container).find("[name^=series_lecturer]");
    	if ( lecture.length > 0 ) {
    		qparams["s"] = $(lecture).val();
        } else {
        	qparams["s"] = "";
    	}
    	var location = $(series_container).find("[name^=series_location]");
    	if ( location.length > 0 ) {
    		qparams["l"] = $(location).val();
        } else {
        	qparams["l"] = "";
    	}
    	var group_pattern = $(group_container).find("[name^=default-date-time-pattern]");
    	if ( group_pattern.length > 0 ) {
    		qparams["g"] = $(group_pattern).val();
    	} else {
    		qparams["g"] = "none";
    	}

    	var target = $(source).parents("tr.series");
    	// show an ajax spinner
    	$(target).after($('#events_pending_load').html());
    	var spinner = $(target).next();
    	$(spinner).addClass("events_container");
    	$(spinner).show();

    	// go get the events table for this events series taking into account the pattern the user is using.
    	$.get("f/events/"+encodeURIComponent(series_id)+"/",qparams,
    			function(data) {
    		var n = $(target).next();
    		if ( $(n).hasClass("events_container")) {
    			$(n).remove();
    		}
    		$(target).after(data);
    		$(target).next().addClass("events_container");
    	}).error(function(data) {
    		var n = $(target).next();
    		if ( $(n).hasClass("events_container")) {
    			$(n).remove();
    		}
    		$(target).after('<div class="events_container">Failed to retrieve events for the series</div>');
    		$(target).next().addClass("events_container");

    	});

    };


    $("td.action-cel a.btn-view").die().live("click", function() {


    	// Get the events from the server and let it decide how many should be shown.
        if($(this).text() === "View"){
        	// find the parent containers for series and group
        	update_events(this);
            $(this).text("Hide");
        }else {
    		var n = $(this).parents("tr.series").next();
    		if ( $(n).hasClass("events_container")) {
    			$(n).remove();
    		}
            $(this).text("View");
        }
            
        return false;
        

    });
    
    
    // Borrow dialog
    var searchButton = $('#borrow-find-button');
    
    $( "#borrow-dialog" ).dialog({
        autoOpen: false,
        width: 700,
        modal: true
    });
    
    $('.open-borrow-dialog').click(function() {
        // reset dialog
        $('#faculty-input').val('');
        searchButton.text('Find events');
        $('#borrow-search-results').hide();
        
        $("#borrow-dialog").dialog("open");
    });
    
    searchButton.click(function() {
        // Perform a search
        
        // Show search results
        $('#borrow-search-results').show();
        $(this).text('Borrow');
    });
    
    $('#borrow-dialog .close-dialog').click(function() {
        $("#borrow-dialog").dialog("close");
    });
    
    // Hide specific content => this needs to be improved, probably with Django specific code
    /*
     * Removed as it doesnt work
     *if($('.admin-title').first().text().trim() == "Add New Events"){
        $('.btn-delete').hide(); // this may not be quite right, as .btn-delete was an ID that had to be
        						// converted to a class, so a better selector might
        						// be required
    }else {
        // settings for the edit mode
        $('#add-new-group-button').hide();
    }
    */



    // Setup a container for functions that have forward references
    var setup_forms = {};
    
    // setup the seriese form, changing the add button into a remove button and 
    // setting the current one to a remove button. (Crazy but thats what the spec says)
    var setup_new_series = function(new_series, end_of_series, new_group) {
    	// Make the add button add new rows
        // Set the icons on the input table
    	new_group.each(function(index, element) {
            $('.action-cel .ic-add',element).not(':last').removeClass('ic-add').addClass('ic-delete-action');
         });

    	// bind the date dialog in (only once per element)
    	bindDateDialog(new_series);

        // bind the autocomplete functionality in to all the
        // series lecturer fields:
    	var autocomplete_fields = new_series.find("input[name^=series_lecturer]");
        autocomplete_fields.each(function(index,element) {
            autocomplete.crsidAutocomplete(element);
        });

    	var ab = new_series.find(".control-series-button");
    	ab.click(function() {
    		if ($(this).hasClass('ic-delete-action')) {
    			new_series.find('.deleted-series-marker').val("1");
    			new_series.hide();
    		} else {
    			var group_id = new_group.find('.group-id-marker').val();
    			var series_add_id = new_group.find(".series").length+1;
            	$.ajax({
        		  	url: "f/series-editor/"+encodeURIComponent(group_id)+"/"+encodeURIComponent(series_add_id)+"/",
        		  	context: document.body
        		}).done(function(data) {
        			end_of_series.before(data);
        			setup_new_series(end_of_series.prev(), end_of_series, new_group);
        		});    		    			
    		}
    	});
    	// Bind validation to this DOM subtree, see validation.js for the classes that are required.
    	validation.bind(new_group);

    }
    //
    // Setup the group form that we might have just loaded
    var setup_new_group = function(new_group) {
    	// remove the form when the delete button is clicked
    	new_group.find('.btn-delete').click(function() {
    		new_group.find('.deleted-group-marker').val("1");
    		new_group.hide();
    	});
    	
    	new_group.find('.show-group-description').click(function() {
    		if ($(this).hasClass('ic-delete-action')) {
        		new_group.find('textarea.group-description').hide();
        		$(this).removeClass('ic-delete-action').addClass('ic-add');
    		} else {
        		new_group.find('textarea.group-description').show();
        		$(this).removeClass('ic-add').addClass('ic-delete-action');
    		}

    	});
    	
    	bindDateDialog(new_group);

    	// setup the counting of the events on the event series table and make it sortable.
    	new_group.find(".sortable").sortable({ placeholder: 'ui-state-highlight', opacity: 0.6, tolerance: 'pointer',items: 'tr', distance: 15});


        var countEvents = function () {
            var totalevents = 0;
            $("tbody.event tr.series td span.dateHolder input").each(function () {
                totalevents += parseInt($(this).val(), 10);
                if (totalevents > 0) {
                	// TODO: improve this selector, its not right.
                    $(".totalevents").text("Total number of events: " + totalevents);
                }
            });
        };
        countEvents();


        new_group.find("tbody.event tr.series td span.dateHolder input").change(function () {
            countEvents();
        });
        
        new_group.find('.combine-help').click(function(){
            
        });

        // Set the icons on the input table so that the + and - buttons on event serise work
    	new_group.find('.editor-group').each(function(index, element) {
            $('.action-cel .ic-add',element).not(':last').removeClass('ic-add').addClass('ic-delete-action');
         });
    	
    	// setup the series which is the row marked with .series before the row marked with .end-of-series-editors
    	var end_of_series = new_group.find(".end-of-series-editors");
    	setup_new_series(end_of_series.prev(), end_of_series, new_group);

    	// Bind validation to this DOM subtree, see validation.js for the classes that are required.
    	validation.bind(new_group);

    }
    // prepare all pre-loaded new group forms
    $(".add-events-form").each(function() {
    	setup_new_group($(this));
    });
    
    // load a new group-edit form whtn the add button is clicked.
    $('#add-new-group-button').click(function() {
    	var group_id = "n_"+($('.group-id-marker').length+1);
    	$.ajax({
    		  	url: "f/group-editor/"+encodeURIComponent(group_id)+"/",
    		  	context: document.body
    		}).done(function(data) {
    			$("#end-of-group-editors").before(data);
    			setup_new_group($("#end-of-group-editors").prev());    			
    		});
    });
    


});
