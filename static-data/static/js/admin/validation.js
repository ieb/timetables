/*
 * This function is nothing like as sophisticated as jQueryValidators, however rather than binding to
 * the input class it binds to the error message which is in the template marked up with the validation class.
 * That enables us to a) progressively build the form and b) ensure that the error messages dont break the 
 * UI. I tried to use jqueryvalidator but gave up as it didn't give me sufficient control over where the markup
 * was.To extend, add a class to marker_classes and add a validation function to validators.
 *
 * To use
 * <div>
 *    <div>
 *       <input type="text" name="yourname" value="" />
 *    </div>
 * </div>
 * <div class="validation_not_empty errormessage">Please fill in your name</div>
 *
 * or
 * <input type="text" name="yourname" value="" />
 * <div class="validation_not_empty errormessage">Please fill in your name</div>
 *
 *
 * This wont work
 * <input type="text" name="yourname" value="" />
 * <span class="in_the_way" />
 * <div class="validation_not_empty errormessage">Please fill in your name</div>
 *
 *
 *
 * and call validation.bind($('#any_element_containing_the_items_to_validate'))
 *
 * To add additional validation classes
 * validation(".classname",function(element, feedback, okfunc, badfunc){
 * });
 *
 * If Ok call okfunc, if not call badfunc.
 *
 * Why no return ?
 *
 * So you can do Ajax things and call okfunc of badfunc.
 *
 *
 *
 */
define(["jquery",
        "domReady",
        "jquery-ui/dialog"
        ], 
        function($) {
    "use strict";

    
    // Checks not empty
    var validateNotEmpty = function(element, feedback, okfunc, badfunc ) {
    	if ( element.val().trim().length === 0 ) {
    		badfunc();
    	} else {
    		okfunc();
    	}
    }
    // Invokes a GET url that validates the pattern. On success the pattern is ok, on 400 the pattern is 
    // is not Ok.
    var validateDatePattern = function(element, feedback, pattern_ok_func, pattern_bad_func) {
        var val = element.val().trim();
        if ( val.length === 0 ) {
        	pattern_bad_func();
        } else {
        	$.get("/administration/validate/date_pattern/"+encodeURIComponent(element.val()),
        			pattern_ok_func).error(pattern_bad_func);
        }
    }
    
    // Invokes a GET url that validates the pattern. On success the pattern is ok, on 400 the pattern is 
    // is not Ok.
    var validateCrsid = function(element, feedback, pattern_ok_func, pattern_bad_func) {
        var val = element.val().trim();
        if ( val.length === 0 ) {
        	pattern_bad_func();
        } else {
            $.get("/administration/validate/crsid/"+encodeURIComponent(val),
      			   pattern_ok_func).error(pattern_bad_func);
        }
    }

    // checks for at least 1 check box checked
    // inputs must all be children of the previous element
    var checkFieldSet = function(element, feedback, okfunc, badfunc) {
    	if ( $(feedback).prev().find("input:checked").length >= 1 ) {
    		okfunc();
    	} else {
    		badfunc();
    	}

    }

    // list of css classes that have validators
    var marker_classes = [
                     '.validation_date_pattern_message',
                     '.validation_not_empty',
             		'.validation_select_one_fieldset',
             		'.validate_crsid'];

    // map of validators to css class
    var validators = {
    		'.validation_date_pattern_message' : validateDatePattern,
    		'.validation_not_empty' : validateNotEmpty,
    		'.validation_select_one_fieldset': checkFieldSet,
    		'.validate_crsid' : validateCrsid
    };


    var getTargetInput = function(errorelem) {
    	var target = $(errorelem).prev('input,select,textarea,fieldset');
    	if ( target.length == 0 ) {
    		// there was no previous input element so we need the last input element of the previous container
    		// the last input in the previous element
    		target = $(errorelem).prev().find('input,select,textarea,fieldset').last()
    	}
    	return target;
    }


    var validateForm;
    // bind to form and mark the form as bound
    var bindToForm = function(input) {
    	var form = $(input).parents('form:first');
    	if (!form.hasClass("submithooked")) {
        	form.submit(validateForm);
        	form.addClass("submithooked");
    	}
    	return form;
    }

    var bindCheck = function(input, feedback, validFunc, form) {
    	if ( ! input.hasClass("valid")) {
    		// add this function to the queue of validation functions to executed on the form
			console.log("Queuing check on "+$(input).attr('name')+" Form "+$(form).attr("id"));
    		$(form).queue(function(next) {
    			try {
	    			validFunc(input, feedback,
	    				function() {
	    					$(feedback).hide();
	    					$(input).addClass('valid');
	    					console.log("Checked Valid "+$(input).attr('name')+" Form "+$(form).attr("id"));
	    					next();
	    				},
	    				function() {
	    					$(input).removeClass('valid');
	    					$(feedback).show();
	    					console.log("Checked InValid"+$(input).attr('name')+" Form "+$(form).attr("id"));
	    					next();
	    				});
        		} catch (e) {
					console.log("Error InValid"+$(input).attr('name')+" Form "+$(form).attr("id"));
					console.log(e.stack);
    				input.removeClass("valid");
    				$(feedback).show();    			
					next();
        		}
    		});
    	} else {
			console.log("Already Valid "+$(input).attr('name')+" Form "+$(form).attr("id"));
    	}
    }

    var bindChange = function(input, feedback, validFunc, differed, form) {
    	input.change(function() {
    		try {
			validFunc($(this), feedback,
    				function() {
						input.addClass("valid");
    					$(feedback).hide();
    				},
    				function() {
    					input.removeClass("valid");
    					$(feedback).show();
    				});
    		} catch (e) {
				input.removeClass("valid");
				$(feedback).show();    			
    		}
    	});
    }


    var bindValidator = function(base, className, validFunc, operation, form) {
    	console.log("Binding validators for class "+className);
        $(base).find(className).each(function() {
        	var feedback = this
        	var input = getTargetInput(this);
        	if ( input.length == 0 ) {
        		console.log("Failed to bind validation to input element. Input element must be a previous sibling or a child of the previous element. ");
        		return;
        	}
        	var containingForm = bindToForm(input);
        	if ( form ) {
        		if ( $(form)[0] === containingForm[0] ) {
        			operation(input, feedback, validFunc, form);  
        		}
        	} else {
        		operation(input, feedback, validFunc, false);
        	}
    	});
    }

    // Any element marked with one of the validation classes is an
    // error Box. EnableValidation will locate those elements, find the nearest previous input element
    // and bind on change to the function related to that valiation class.
    var bindValidatorsOperation = function(base, operation, form) {
    	for ( var c in marker_classes ) {
    		var className = marker_classes[c]
    		var validFunc = validators[marker_classes[c]]
    		if ( !validFunc ) {
    			console.log("No Validation function for marker class "+marker_classes[c]);
    			continue;
    		}
    		bindValidator(base, className, validFunc, operation, form);
        }
    };

    var bindValidators = function(base) {
    	return bindValidatorsOperation(base, bindChange, false);
    };
    
    // Show the dialog, but only after 2s if we have a slow validation or post
    var show_slow_post_dialog = function() {
    	$("#long-post-dialog").delay(1000).queue(function() {
    		$("#long-post-dialog").dialog("open");
    	});
    }

    // cancel any pending event to show the dialog and hide it.
    var hide_slow_post_dialog = function() {
    	$("#long-post-dialog").clearQueue();
    	$("#long-post-dialog").dialog("close");
    }


    // form submit function
    validateForm = function() {
    	// check everything that hasnt been checked. We have to uses a queue on the form element
    	// as some of the validations are ajax calls. So we add validators and finally add the check function.
    	// once the queue is built we return false.
    	// The queue then runs and at the end it sets a css class on the form of form_is_valid, or not and removes
    	// the css class form_is_being_checked.
    	var form = this;
    	if ( $(form).hasClass("form_is_valid") ) {
    		  show_slow_post_dialog();
    		  return true;
    	}
    	if ( $(form).hasClass("form_is_being_checked") ) {
    		return false;
    	}
    	$(form).addClass("form_is_being_checked");
		show_slow_post_dialog();
    	var error_set = {};
    	console.log("Building Validation Queue on Form "+$(form).attr("id"));
    	var messages = bindValidatorsOperation(this, bindCheck, form);
    	console.log("Done Building Validation Queue on Form "+$(form).attr("id")+" Queue contains "+$(form).queue());
    	$(form).queue(function(next) {
    		try {
	    		console.log("Last Method in Validation QUeue on Form "+$(form).attr("id"));
		    	for (var c in marker_classes ) {
		        	$(this).find(marker_classes[c]).filter(':visible').each(function() {
		        		error_set[$(this).text()] = true;
		        	});
		    	}
		    	var errors = Array();
		    	for ( var e in error_set ) {
		    		errors.push(e);
		    	}
				var toperrormessage = $(this).find(".top_error_message");
		    	if (errors.length > 0) {
		    		if ( toperrormessage.length === 1 ) {
		    			var errorlist = $(toperrormessage).find("ul.errorlist");
		    			errorlist.html("");
		    			for (var e in errors) {
		    				errorlist.append("<li>"+errors[e]+"</li>");
		    			}
		    			toperrormessage.show();
		    		} else {
		        		$("#error-dialog-message").text("There were errors, please correct them.")
		                $("#error-dialog").dialog("open");    			
		    		}
		    		$(form).removeClass("form_is_valid");
		    		$(form).removeClass("form_is_being_checked");
		    		console.log("Form has been checked and has errors ");
		    		hide_slow_post_dialog();
	
		    	} else {
		    		if ( toperrormessage.length === 1) {
		    			toperrormessage.hide();
		    		}
		    		$(form).addClass("form_is_valid");
		    		$(form).removeClass("form_is_being_checked");
		    		console.log("Form has been checked and is being submitted, no errors ");
		    		// Retrigger the form submission we could hook here and prevent double submissions
		    		$(form).submit();
		    	}
		    	$(form).clearQueue();
		    	$(form).dequeue();
    		} catch (e) {
    			console.log(e.stack);
    			$(form).removeClass("form_is_valid");
	    		$(form).removeClass("form_is_being_checked");
		    	$(form).clearQueue();
		    	$(form).dequeue();
	    		hide_slow_post_dialog();
	    		return;
    		}
    	});
    	return false;
    }


    // add a new validator and function
    var addValidator = function(validatorClass, func) {
    	validators[validatorClass] = func;
    	marker_classes.push(validatorClass);
    }
    
    var chainToValidator = function(validator_class, element, feedback, pattern_ok_func, pattern_bad_func) {
    	validators[validator_class](element, feedback, pattern_ok_func, pattern_bad_func);
    };
    
    
    $("#long-post-dialog").dialog({
        autoOpen: false,
        width: 620,
        modal: true
    });

    $('#long-post-dialog .close-dialog').click(function() {
        $("#long-post-dialog").dialog("close");
        return false;
    });

    
    return {
    	// add a new validator with function
    	add : addValidator,
    	// enable all known validation
    	bind : bindValidators,
    	
    	chain : chainToValidator
    }
});
