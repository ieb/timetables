define(["jquery",
        "admin/views",
        "jquery-ui/dialog", 
        "domReady"], 
        function($, views){
    "use strict";

    
    /* Duplicate year dialog */
    $("#duplicate-dialog").dialog({
        autoOpen: false,
        width: 400,
        modal: true
    });
        
    $("#open-duplicate-dialog").click(function() {
        $("#duplicate-dialog").dialog("open");
        return false;
    });
    $('.close-dialog').click(function() {
        $("#duplicate-dialog").dialog("close");
    });
    
    
    views.enableTopNav(function( start_year, org_code ){
        return "/administration/" +
            encodeURIComponent(start_year) +
            "/" +
            encodeURIComponent(org_code);
    });
    
 
});