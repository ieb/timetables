define(["jquery",
        "admin/views",
        "admin/classification-selectors"
        ],
        function($, views){
    "use strict";
    
    views.enableTopNav(function( start_year, org_code ){
        return "/administration/" +
            encodeURIComponent(start_year) +
            "/" + encodeURIComponent(org_code) +
            "/changehistory/";
    });
    
    $(function() {
        $('.ic-import-date').hide();
        $('.ic-add').hide();
        $('.combine-help').hide();
        $('.action-cel').first().hide();
        $('.ic-delete').hide();
        
        $("input:text").attr("disabled", "disabled");
        $("input:checkbox").attr("disabled", "disabled");
        $("input:radio").attr("disabled", "disabled");
        $("select").attr("disabled", "disabled");
    });
    
    return {
    };
});