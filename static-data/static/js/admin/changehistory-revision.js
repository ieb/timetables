
define(["jquery",
        "admin/views"], function($, views){
    
    views.enableTopNav(function( start_year, org_code ){
        return "/administration/" +
            encodeURIComponent(start_year) +
            "/" + encodeURIComponent(org_code) +
            "/changehistory/";
    });
    
    
    return {
    };
});