define(["jquery", 
        "admin/views",
        "autocomplete",
        "domReady", 
        "jquery-ui/dialog"],
        function($, views, autocomplete){
	"use strict";
	
    // puts all the values in a set of checked checkboxes into a single target form field.
	var update_selected_ids = function(check_box_selector, target_id, target_count_id) {
		$(target_id).val("");
		$(target_id+"-all").val("");
		$(target_count_id).text("");
		if ($("#select_all_events").prop("checked")) {
			$(target_id+"-all").val("1");
			$(target_count_id).text("all this set of");
			return -1
		} else {
			var checked_ids = [];
			$(check_box_selector).each( function() {
				if ( this.checked ) {
					checked_ids.push($(this).val());
				}
			});
			$(target_id).val(checked_ids.join());
			$(target_count_id).text(checked_ids.length);
			return checked_ids.length
		}
	}

	// Function to setup all forms and populate them with the values the user has 
	// selected from in the check boxes. If the user select all events, that will be recorded
	// In general the forms do a browser HTTP post followed by a redirect and refresh.
	var setup_bulk_form = function(dialog_id, checkboxes, extra_setup) {
    	$(dialog_id+"-form").each(function(){
    	       this.reset();
       	});
    	if ( update_selected_ids(checkboxes,dialog_id+"-selected",dialog_id+"-selected-count") === 0 ) {
    		$("#error-dialog-message").text("No Events selected please select all or individual events")
            $("#error-dialog").dialog("open");
    	} else {
    		if ( extra_setup ) {
    			extra_setup();
    		}
                if ( dialog_id == "#edit-lecturers-dialog" ) {
                    // If we are doing a bulk edit of lecturers then
                    // add the crsid autocomplete functionality:
    	            var autocomplete_fields = $( "#edit-lecturers-dialog" ).find("input[name=lecturers]");
                    autocomplete_fields.each(function(index,element) {
                        autocomplete.crsidAutocomplete(element);
                    });
                }
                
                $(dialog_id).dialog("open");
    	}
	}


	// Enable the top navigation drop downs.
	//
    views.enableTopNav(function( start_year, org_code ){
        return "/administration/" +
            encodeURIComponent(start_year) +
            "/" + encodeURIComponent(org_code) +
            "/events/";
    });


    // Initialise the events on the page
    $(function(){
        /* ----------------------------------------------------------------------- */
        /* Table rollover to show links for Edit and Quick Edit */
        /* ----------------------------------------------------------------------- */
        $('#events table tr').not("first").hover(
            function (){
               $(this).css("background","#ebf2f3");
               $(this).find('.nav-quickedit').show();
           }, 
           function () {
               $(this).css("background","");
               $(this).find('.nav-quickedit').hide();
           }
       );
       
        /* ----------------------------------------------------------------------- */
        /* Quick Edit Dialog */
        /* ----------------------------------------------------------------------- */
       $('body').append($('#quick-edit-panel'));
       $('.quick-edit-data').click(function(){
    	   $('#quick-edit-panel-form').each(function(){
    		   this.reset();
    	   });

    	   // locate the event-id from the checkbox with a class events_selection in the same row
    	   // ancestor of type tr child of tyle input with class events_selection
    	   // set the event id in the form
    	   var row = $(this).closest("tr");
    	   $("#quick-edit-panel-form-event-id").val(row.find("input.events_selection").val());
    	   // populate the fields from the data in the rows.
    	   var strip_serise=new RegExp("(.*?)\\(was:.*\\)");
    	   // function that extracts the first group is there is a match
    	   var get_current_value = function(term) {
    		   var v = term.text().trim()
    		   var m = strip_serise.exec(v);
    		   if (m === null) {
    			   return v;
    		   } else {
    			   return m[1];
    		   }
    	   };
    	   var split_and_trim = function(term) {
    		   var terms = term.split(",");
    		   for (var i=0; i < terms.length;i++) {
    			   terms[i] = terms[i].trim();
    		   }
    		   return terms.join();
    	   }

    	   $("#quick-edit-panel-form-title").val(get_current_value(row.find(".events-list-title")));
    	   $("#quick-edit-panel-form-lecturers").val(split_and_trim(get_current_value(row.find(".events-list-lecturers"))));
    	   $("#quick-edit-panel-form-location").val(get_current_value(row.find(".events-list-location")));
    	   // show the form
    	   var popuplocation = $(this).offset();
    	   popuplocation.top = popuplocation.top - $('#quick-edit-panel').height() - 69;
    	   popuplocation.left = popuplocation.left - $('#quick-edit-panel').width() / 2;
           $('#quick-edit-panel').show();
           // to make popup locations work, you have to show first.
    	   $('#quick-edit-panel').offset(popuplocation);
       });
       
       $('#quick-edit-panel .close-dialog').click(function(){
           $('#quick-edit-panel').hide();
       });

       /* ----------------------------------------------------------------------- */
        /* Classification dialog */
       /* ----------------------------------------------------------------------- */
        $("#classification-dialog").dialog({
            autoOpen: false,
            width: 756,
            modal: true
        });
            
        $("#open-classification-dialog").click(function() {
            $("#classification-dialog").dialog("open");
            return false;
        });
        $('#classification-dialog .close-dialog').click(function() {
            $("#classification-dialog").dialog("close");
        });

        $('#classification-dialog .clear').click(function() {
            $("#classification-dialog-form").each(function(){
                this.reset();
         });
        });

        /* ----------------------------------------------------------------------- */
        /* Publish timetables dialog */
        /* ----------------------------------------------------------------------- */
        $("#publish-timetables-dialog").dialog({
            autoOpen: false,
            width: 756,
            modal: true
        });
    
        $("#publish-timetables-button").click(function() {
            $("#publish-timetables-dialog").dialog("open");
            return false;
        });
    
        $('#publish-timetables-dialog .close-dialog').click(function() {
            $("#publish-timetables-dialog").dialog("close");
        });

        $('#publish-timetables-dialog .clear').click(function() {
            $("#publish-timetables-dialog-form").each(function(){
                   this.reset();
            });
        });

    
        /* ----------------------------------------------------------------------- */
        /* Edit classification, lecturers, location dialogs */
        /* ----------------------------------------------------------------------- */
        $("#edit-classification-dialog").dialog({
            autoOpen: false,
            width: 756,
            modal: true
        });

        $("#edit-lecturers-dialog").dialog({
            autoOpen: false,
            width: 400,
            modal: true
        });
        
        $("#edit-location-dialog").dialog({
            autoOpen: false,
            width: 400,
            modal: true
        });
        
        $('#edit-classification-dialog .close-dialog').click(function() {
            $("#edit-classification-dialog").dialog("close");
        });

        $('#edit-location-dialog .close-dialog').click(function() {
            $("#edit-location-dialog").dialog("close");
        });

        $('#edit-lecturers-dialog .close-dialog').click(function() {
            $("#edit-lecturers-dialog").dialog("close");
        });

        /* ----------------------------------------------------------------------- */
        /* Select all behaviour */
        /* ----------------------------------------------------------------------- */
    	$('#select_all_events').change(function(){
    		var check_all = this.checked;
    		$(".events_selection").each(function() {
    			$(this).prop("checked",check_all);
    		});
    	});


        /* ----------------------------------------------------------------------- */
    	/* Edit Options Dialog */
        /* ----------------------------------------------------------------------- */
        $('#edit-options').change(function(event){
            switch($(this).prop("selectedIndex"))
            {
            // the order must match the lookup
                case 1:
                	setup_bulk_form("#edit-lecturers-dialog",".events_selection")
                break;
                
                case 2:
                	setup_bulk_form("#edit-location-dialog",".events_selection")
                break;
            }
            
            $(this).prop("selectedIndex", 0);
        });




        /* ----------------------------------------------------------------------- */
        /* Export timetables dialog */
        /* ----------------------------------------------------------------------- */
        $("#export-events-dialog").dialog({
            autoOpen: false,
            width: 400,
            modal: true
        });

        $('#export-events-dialog-form').submit(function() {
            $("#export-events-dialog").dialog("close");
            return true;
        });

        $('#export-events-dialog .close-dialog').click(function() {
            $("#export-events-dialog").dialog("close");
        });

        $("#export-options").change(function(){
        	var format = $(this).val();
        	setup_bulk_form("#export-events-dialog",".events_selection", function() {
            	$('#export-events-dialog-fmt-txt').text(format);
            	$('#export-events-dialog-fmt').val(format);
        	});
            $(this).prop("selectedIndex", 0);
        });
        

        /* ----------------------------------------------------------------------- */
        /* Tab switcher for external events */
        /* ----------------------------------------------------------------------- */

        $('.admin-tab-group a').click(function(event){
            switch($(this).attr('id')){
                case "open-events-tab":
                    $('#events').show();
                    $('#filterOptions').show();
                    $('#external-listing').hide();
                    $(this).parent().addClass('selected');
                    $('#open-external-link-tab').parent().removeClass('selected');
                break;

                case "open-external-link-tab":
                    $('#events').hide();
                    $('#filterOptions').hide();
                    $('#external-listing').show();
                    $(this).parent().addClass('selected');
                    $('#open-events-tab').parent().removeClass('selected');
                break;

            }
        });


    });
    
    return {
    };
});
