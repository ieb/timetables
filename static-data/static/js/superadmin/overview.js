define(["jquery",
        "underscore",
        "backbone",
        "views",
        "models",
        "bootstrap/organisationsByYear",
        "jquery-ui/tabs",
        "domReady"],
        function($,
                _,
                Backbone,
                 views,
                 models,
                 organisationsByYear){
    "use strict";
    
    // Setup models
    var yearsState = models.YearsState.fromJSON(organisationsByYear);

    // Setup views
    var SelectTimetableView = Backbone.View.extend({
        
        events: {
            "click input": "viewTimetable"
        },
        
        initialize: function() {
            this.yearsState = this.options.yearsState;
        },
        
        render: function() {
            var yearSelector = new views.YearSelectView({
                yearsState: this.yearsState,
                el: this.$("#year-select").get(0)
            }).render();
            
            var organisationSelector = new views.OrganisationSelectView({
                yearsState: this.yearsState,
                el: this.$("#organisation-select").get(0)
            }).render();
        },
        
        viewTimetable: function(e) {
            var year = this.yearsState.selectedYear();
            var org = this.yearsState.selectedOrganisation();
            console.log(year.toJSON(), org.toJSON());
        }
        
    });
    
    // Create a customised version of the SuperadminAppView specifically for
    // the overview page
    var SuperadminOverviewAppView = views.SuperadminAppView.extend({
        
        events: function() {
            return _.extend(
                    views.SuperadminAppView.prototype.events.call(this), {
                
            });
        },
        
        initialize: function() {
            views.SuperadminAppView.prototype.initialize.call(this);
        },
        
        render: function() {
            views.SuperadminAppView.prototype.render.call(this);
            
            // Create a SelectTimetableView to control the block holding the
            // two <select>s for faculty and year.
            new SelectTimetableView({
                yearsState: yearsState,
                el: this.$("#view-faculty-timetables").get(0)}).render();
        }
    });
    
    
    // TODO: Refactor
    
    $("#tabs").tabs();


    $("tr.faculty td span.float-right a.btn-view").die().live("click", function() {
        $(this).closest("tbody").next("tbody").toggle();
        if ($(this).text() === "▶ View") {
            $(this).text("▼ Hide");
            $(this).closest("tbody").css({
                'background': '#F0F0F0'
            });
        } else {
            $(this).closest("tbody").css({
                'background': '#FFFFFF'
            });
            $(this).text("▶ View");
        }
        return false;
    });
    
    return {
        ApplicationView: SuperadminOverviewAppView
    };
});