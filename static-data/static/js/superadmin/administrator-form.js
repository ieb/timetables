define(["jquery", "assert", "underscore", "domReady"],
        function($, assert, _) {
    
    function setupAdminForm(isAddPage) {
        // Check if the form was manually submitted or auto submitted.
        var $was_auto_submitted = $(
                "form input[name=meta_was_auto_submitted]");
        
        // Restore scroll offset
        var offset = parseInt($("form input[name=meta_scroll_offset]").val());
        if(offset && $was_auto_submitted.val()) {
            console.log("Restoring scroll offset to", offset);
            $("body").scrollTop(offset);
        } else {
            console.log("No scroll offset", offset);
        }
        $was_auto_submitted.val(false);
        
        // When the year or organisation selection changes we submit the form in 
        // order that the server can provide values for select form fields which are
        // dependent on selected year/org. E.g. we can't show a list of orgs until
        // we know the year to look under. Similarly we can't display a list of
        // classifiers until we know the organisation whose classifiers should be
        // shown.
        var $context = $(".django-form.admin-editor .django-formset-container");
        $($context).on("change", 
            "select[name$=year], select[name$=organisation]", function (){
                
                var $this = $(this);
                var $facultyForm = $this.closest(".faculty-affiliation");
        
                // Before submitting the form we need to clear out selected values
                // that are dependant on the year or organisation selection that
                // was changed.
                
                // If it was the year selector that changed, unselect the organisation
                var thisIsYear = Boolean($this.filter("select[name$=year]").length);
                if(thisIsYear) {            
                    $facultyForm.find("select[name$=organisation]").val(null);
                }
                
                // De-select all the classifiers
                $facultyForm.find(".section-admin-add-group input[type=checkbox]")
                    .attr("checked", false);
                
                // save the scroll offset of the page
                $("form input[name=meta_scroll_offset]").val($("body").scrollTop());
                // mark the form as being autosubmitted.
                $was_auto_submitted.val(true);
                
                $("form").submit();
        });
        $context.on("click", ".faculty-affiliation .btn-remove", function() {
            var $facultyAffiliation = $(this).closest(".faculty-affiliation");
            if(isAddPage) {
                $facultyAffiliation.remove();
            } else {
                $facultyAffiliation.find("[name$=DELETE]").val(true);
                $facultyAffiliation.addClass("soft-deleted");
            }
            $(".django-formset.faculty-affiliations > [name$=TOTAL_FORMS]")
                .val($context.children().length);
        });

        $("#add-faculty-affiliation-btn").on("click", function() {
            function success(data, status, jqxhr) {
                $(data).appendTo($context);
                $(".django-formset.faculty-affiliations > [name$=TOTAL_FORMS]")
                    .val($context.children().length);
            }

            var url = isAddPage ? "/forms/organisation-affiliation-add/" :
                "/forms/organisation-affiliation-edit/";
            var jqxhr = $.get(url, 
                {prefix: "form-" + $context.children().length});
            jqxhr.success(success);
        });
    }
    
    return {
        setupAdminForm: setupAdminForm
    };
});