define(["jquery",
        // jqui has to be loaded before backbone so that this.$.dialog works.
        "jquery-ui/dialog",
        "jquery-ui/position",
        "underscore", 
        "backbone",
        "superadmin/models",
        "views",
        "viewmodels",
        "superadmin/viewmodels",
        "assert",
        "backbone-utils",
        "normalise",
        "text!templates/facultyEdit.html",
        "text!templates/classifierGroupEdit.html",
        "text!templates/classifierEdit.html",
        "text!templates/classifierEditDialog.html",
        "text!templates/classifierGroupEditDialog.html",
        "text!templates/formField.html",
        "text!templates/uniqueTextFormField.html",
        "text!templates/uniquenessIndicator.html",
        "text!templates/formButtons.html",
        "text!templates/loadingIndicator.html",
        "text!templates/fieldEditorDialog.html"
        ],
        function($,
                __ignore1__, // jquery ui, will be undefined
                __ignore2__, // jquery ui, will be undefined
                _,
                Backbone, 
                superadminModels,
                views,
                viewmodels,
                superadminViewmodels,
                assert,
                backbone_utils,
                normalise,
                facultiesEditTemplate,
                classifierGroupEditTemplate,
                classifierEditTemplate,
                classifierEditDialogTemplate,
                classifierGroupEditDialogTemplate,
                formFieldTemplate,
                uniqueTextFormFieldTemplate,
                uniquenessIndicatorTemplate,
                formButtonsTemplate,
                loadingIndicatorTemplate,
                fieldEditorDialogTemplate) {
    "use strict";

    var superclass = backbone_utils.superclass;
    var superproto = backbone_utils.superproto;
    
    // View Classes
    
    var FieldAnnotationView = Backbone.View.extend({
        tagName: "li",
        
        initialize: function() {
            _.bindAll(this, "render");
            this.annotation = this.options.annotation;
            this.annotation.on("change", this.render);
        },
        
        render: function(initial) {
            if(!this.annotation.isVisible()) {
                this.$el.hide();
                return;
            }
            if(!this.annotation.isValid()) {
                this.$el.text(this.annotation.getMessage());
                this.$el.slideDown();
            }
            else {
                if(initial) {
                    this.$el.hide();
                } else {
                    this.$el.slideUp();
                }
            }
            return;
            
        }
    });
    
    var SelectView = Backbone.View.extend({
        tagName: "select",
        
        events: {
            "change": "changed"
        },
        
        initialize: function() {
            _.bindAll(this, "render");
            this.selectModel = this.options.selectModel;
            this.selectModel.onChoicesChanged(this.render);
        },
        
        render: function() {
            var choices = this.selectModel.choices();
            _.each(choices, function(option) {
                var $option = $("<option></option>");
                $option.val(option[0]);
                $option.text(option[1]);
                this.$el.append($option);
            }, this);
        },
        
        changed: function() {
            this.selectModel.onSelectionChanged(this.$el.val());
        }
    });
    
    var TextInputView = Backbone.View.extend({
        
        tagName: "input",
        
        attributes: function() {
            return {
                type: "text"
            };
        },
        
        events: {
            "propertychange": "onUserInput",
            "keyup":          "onUserInput",
            "input":          "onUserInput",
            "cut":            "onUserInput",
            "paste":          "onUserInput"
        },
        
        initialize: function() {
            _.bindAll(this, "onRemoteValueChanged", "render");
            this.modelAttribute = this.options.inputModel.valueAttribute();
            assert(this.modelAttribute);
            this.modelAttribute.on("change", this.onRemoteValueChanged);
            this.value = this.$el.val() || this.modelAttribute.get() || "";
        },
        
        onRemoteValueChanged: function(value) {
            if(value !== this.value) {
                this.value = value;
                this.render();
            }
        },
        
        onUserInput: function() {
            var value = this.$el.val();
            if(value !== this.value) {
                this.value = value;
                this.modelAttribute.set(value);
            }
        },
        
        render: function() {
            if(this.$el.val() !== this.value) {
                this.$el.val(this.value);
            }
        },
        
        focus: function() {
            this.$el.focus();
        }
    });
    
    /**
     * Responsible for showing a loading indicator when the annotations of a
     * FormFieldView are being calculated. 
     */
    var FormFieldAnnotationProgressView = Backbone.View.extend({
        template: _.template(loadingIndicatorTemplate),
        
        initialize: function() {
            _.bindAll(this, "render", "renderFade");
            this.field = this.options.fieldModel;
            assert(this.field);
            this.field.on("change:annotations_calculated", this.renderFade);
        },
        
        render: function() {
            this.$el.html(this.template());
            var isCalculated = this.field.get("annotations_calculated");
            if(isCalculated)
                this.$el.hide();
            else
                this.$el.show();
        },
        
        renderFade: function() {
            var isCalculated = this.field.get("annotations_calculated");
            if(isCalculated)
                this.$el.hide();
            else
                this.$el.show();
        }
    });
    
    var FormFieldView = Backbone.View.extend({
        
        template: _.template(formFieldTemplate),
        
        initialize: function() {
            _.bindAll(this, "enableAnnotations", "renderValidityChange");
            
            this.field = this.options.fieldModel;
            this.field.onValidityChange(this.renderValidityChange);
            assert(this.field);

            this.inputView = this.options.inputView;
            this.inputModel = this.field.get("inputViewModel");
            assert(this.inputView);
            assert(this.inputModel);
        },
        
        render: function() {
            this.$el.html(this.template(this.field.toJSON()));
            
            this.$(".input-wrapper").append(this.inputView.el);
            this.inputView.render();
            
            var loadingIndicator = new FormFieldAnnotationProgressView({
                el: this.$(".loading-indicator-wrapper").get(0),
                fieldModel: this.field
            });
            loadingIndicator.render();
            
            // Listen for any changes to the input and make the annotations 
            // visible when they happen.
            this.inputModel.valueAttribute().on(
                    "change", this.enableAnnotations);
            
            var $annotations = this.$("ul");
            _.each(this.getAnnotations(), function(annotation) {
                var view = new FieldAnnotationView({annotation: annotation});
                $annotations.append(view.el);
                view.render(true);
            }, this);
        },
        
        getInputView: function() {
            return this.inputView;
        },
        
        renderValidityChange: function() {
            if(this.field.areAnyAnnotationsInvalid()) {
                this.$el.addClass("invalid-content");
            } else {
                this.$el.removeClass("invalid-content");
            }
        },
        
        getAnnotations: function() {
            return this.field.get("annotations").models;
        },
        
        enableAnnotations: function() {
            this.field.get("annotations").each(function(annotation) {
                annotation.makeVisible();
            });
        }
    });
    
    /**
     * A view which controls a dialog which allows classification names to be
     * edited.
     */
    var DialogView = Backbone.View.extend({
        tagName: "div",
        
        attributes: { style: "display: none;" },
        
        events: function() {
            return {
                "click button.cancel": "onCancel",
                "click button.save": "onSave",
                "keypress": "onKeyPress"
            };
        },
        
        initialize: function() {
            _.bindAll(this, "render", "onCancel", "onSave",
                    "focusTriggeringElement");
            this.on("save cancel", this.focusTriggeringElement);
        },
        
        renderTemplate: function() {
            return this.template({
                model: this.model.toJSON()
            });
        },
        
        renderDialog: function() {
            this.$el.html(this.renderTemplate());
            $("body").append(this.el);
            this.$el.dialog({
                autoOpen: true,
                width: 350,
                modal: true
            });
            this.$el.parent().position(this.options.position);
            // Bind the dialog's close event to ours so that our view is
            // tidied up if jqui closes the dialog (e.g. esc key pressed).
            this.$el.bind("dialogbeforeclose", this.onCancel);
        },
        
        render: function() {
            this.renderDialog();
            return this;
        },
        
        onCancel: function() {
            this.trigger("cancel", this.model);
            this.remove();
        },
        
        onSave: function() {
            this.trigger("save", this.model);
            this.remove();
        },
        
        onKeyPress: function(e) {
            if(e.keyCode === $.ui.keyCode.ENTER) {
                this.onSave();
            }
        },
        
        focusTriggeringElement: function() {
            // FIXME: for some reason this is no longer actually focusing 
            // anything...
            $(this.options.triggeringElement).focus();
        }
    });
    
    var FieldEditorDialog = DialogView.extend({
        constructor: function FieldEditorDialog() {
            superclass(FieldEditorDialog).apply(this, arguments);
        },
        
        className: "classifier-edit-dialog admin-dialog",
        
        template: _.template(fieldEditorDialogTemplate),
        
        initialize: function() {
            superproto(FieldEditorDialog).initialize.apply(this, arguments);
            _.bindAll(this, "updateSaveButton");
            this.viewModel = this.options.viewModel;
            assert(this.viewModel instanceof 
                    superadminViewmodels.FieldEditorDialogVM);
        },
        
        renderTemplate: function() {
            return this.template(this.viewModel.toJSON());
        },
        
        render: function() {
            superproto(FieldEditorDialog).render.call(this);
            
            var formFieldVM = this.viewModel.get("formField");
            var inputVM = formFieldVM.get("inputViewModel")
            var fieldView = new FormFieldView({
                el: this.$("li").get(0),
                fieldModel: formFieldVM,
                inputView: new TextInputView({ inputModel: inputVM })
            });
            fieldView.render();
            fieldView.getInputView().focus();
            
            formFieldVM.on("change:synched", this.updateSaveButton);
            this.updateSaveButton();
        },
        
        isFieldValid: function() {
            var formFieldVM = this.viewModel.get("formField");
            return formFieldVM.isSynched();
        },
        
        updateSaveButton: function() {
            var $button = this.$("button.save");
            if(this.isFieldValid()) {
                $button.removeClass("btn-inactive");
            } else {
                $button.addClass("btn-inactive");
            }
        },
        
        onSave: function() {
            if(this.isFieldValid())
                superproto(FieldEditorDialog).onSave.call(this);
        },
    });
    
    /**
     * A view which controls a dialog which allows classification names to be
     * edited.
     */
    var ClassifierEditDialogView = DialogView.extend({
        className: "classifier-edit-dialog admin-dialog",
        template: _.template(classifierEditDialogTemplate),
        
        initialize: function() {
            DialogView.prototype.initialize.call(this);
            this.isCreating = this.options.isCreating;
        },
        
        renderTemplate: function() {
            return this.template({
                model: this.model.toJSON(),
                isCreating: this.isCreating
            });
        },
        
        render: function() {
            DialogView.prototype.render.call(this);
            //var posForX = 
            return this;
        },
        
        onSave: function() {
            this.model.set({"value": this.$("input").val()});
            DialogView.prototype.onSave.call(this);
        }
    });
    
    /**
     * A view which controls a dialog which allows classification groups to
     * be edited.
     */
    var ClassifierGroupEditDialogView = ClassifierEditDialogView.extend({
        
        className: "classifier-group-edit-dialog admin-dialog",
        
        template: _.template(classifierGroupEditDialogTemplate),
        
        onSave: function() {
            this.model.set({"name": this.$("input").val()});
            this.trigger("save", this.model);
            this.remove();
        }
    });
    
    var ClassifierView = Backbone.View.extend({
        
        tagName: "tr",
        className: "classifier",
        
        template: _.template(classifierEditTemplate),
        
        events: function() {
            return {
                "click": "onEdit",
                "click a.delete": "onDelete"
            };
        },
        
        initialize: function() {
            _.bindAll(this, "render");
            this.classifier = this.options.classifier;
            this.classifier.on("change", this.render);
        },
        
        render: function() {
            this.$el.html(this.template(this.classifier.toJSON()));
            return this;
        },
        
        onEdit: function(event) {
            // Because we've bound edit to the entire element, we need to ignore
            // edits triggered by clicking on delete!
            if($(event.target).hasClass("delete")) {
                return;
            }
            
            var dialogVM = this.classifier.editValueDialog();
            var dialog = new FieldEditorDialog({
                viewModel: dialogVM,
                triggeringElement: this.$(".edit"),
                position: {
                    my: "left top",
                    at: "left top",
                    of: this.$(".edit")
                }
            });
            dialog.render();
        },
        
        onDelete: function() {
            this.classifier.remove();
        }
        
    });
    
    var ClassifierGroupView = Backbone.View.extend({
        
        tagName: "div",
        className: "classification-group box-container",
        
        template: _.template(classifierGroupEditTemplate),
        
        events: function() {
            return {
                "click .classifier-group-controls .edit": "onEdit",
                "click .classifier-group-controls .delete": "onDelete",
                "click .add-new": "onAddNewClassifier"
            };
        },
        
        initialize: function() {
            _.bindAll(this, "render", "onClassifierCreated",
                    "onClassifierDialogClosed");
            this.classifierGroup = this.options.classifierGroup;
            this.classifierGroup.on("change", this.render);
            this.classifierGroup.classifiers().on("add remove", this.render);
        },
        
        render: function() {
            this.$el.html(this.template(this.classifierGroup.toJSON()));
            
            var $classifiers = this.$(".classifiers");
            this.classifierGroup.classifiers().each(function(classifier){
                var view = new ClassifierView({ classifier: classifier });
                $classifiers.append(view.render().el);
            }, this);
            
            if(this.classifierGroup.classifiers().length === 0) {
                this.$el.addClass("empty");
            } else {
                this.$el.removeClass("empty");
            }
            
            return this;
        },
        
        onDelete: function() {
            this.classifierGroup.remove();
        },
        
        onEdit: function() {
            var $editButton = this.$(".classifier-group-controls .edit");
            var dialogVM = this.classifierGroup.editTitleDialog();
            
            
            var dialog = new FieldEditorDialog({
                viewModel: dialogVM,
                triggeringElement: $editButton,
                position: {
                    my: "left bottom",
                    at: "left bottom",
                    of: $editButton
                }
            });
            dialog.render();
        },
        
        onAddNewClassifier: function() {
            var $triggerButton = this.$(".add-new");
            var dialogVM = this.classifierGroup.newClassifierDialog();
            
            var dialog = new FieldEditorDialog({
                viewModel: dialogVM,
                triggeringElement: $triggerButton
//                position: {
//                    my: "left top",
//                    at: "left top",
//                    of: $triggerButton
//                }
            });
            // If the dialog is saved add the classifier to our group
            dialog.on("save", this.onClassifierCreated);
            dialog.on("save cancel", this.onClassifierDialogClosed);
            dialog.render();
        },
        
        onClassifierCreated: function() {
            this.classifierGroup.onNewClassifierDialogClosed(true);
        },
        
        onClassifierDialogClosed: function() {
            this.classifierGroup.onNewClassifierDialogClosed(false);
        }
        
    });
    
    var FormButtonsView = Backbone.View.extend({
        constructor: function FormButtonsView() {
            superclass(FormButtonsView).apply(this, arguments);
        },
        
        template: _.template(formButtonsTemplate),
        
        events: function() {
            return {
                "click .cancel": "onCancel",
                "click .save": "onSave"
            };
        },
        
        initialize: function() {
            _.bindAll(this, "render");
            this.form = this.options.form;
            this.form.on("change", this.render);
        },
        
        onSave: function() {
            this.form.onSave();
        },
        
        onCancel: function() {
            this.form.onCancel();
        },
        
        render: function() {
            this.$el.html(this.template(this.form.toJSON()));
        }
    });
    
    var FacultyEditView = Backbone.View.extend({
        
        tagName: "section",
        id: "add-new-faculties",
        template: _.template(facultiesEditTemplate),
        
        events: function() {
            return {
                "click #add-classification-group-button": "addClassificationGroup"
            };
        },
        
        initialize: function() {
            _.bindAll(this, "render", "onClassifierGroupCreated", 
                    "onCancelClassifierGroupCreation", 
                    "onClassifierGroupCreated");
            this.form = this.options.formModel;
            
            this.form.get("classifierGroups").on("add remove", this.render);
        },
        
        render: function() {
            console.log("FacultyEditView.render()");
            this.$el.html(this.template(this.form.toJSON()));
            
            // Name field
            var nameFieldModel = this.form.get("name");
            var nameField = new FormFieldView({
                el: this.$("li[data-name='faculty_name']").get(0),
                fieldModel: nameFieldModel,
                inputView: new TextInputView({
                    inputModel: nameFieldModel.get("inputViewModel")
                })
            });
            nameField.render();

            // Code field
            var codeFieldModel = this.form.get("code");
            var codeField = new FormFieldView({
                el: this.$("li[data-name='faculty_code']").get(0),
                fieldModel: codeFieldModel,
                inputView: new TextInputView({
                    inputModel: codeFieldModel.get("inputViewModel")
                })
            });
            codeField.render();

            // Year field
            var yearFieldModel = this.form.get("year");
            var years = new SelectView({
                el: this.$("select.year-select").get(0),
                selectModel: yearFieldModel.get("inputViewModel")
            });
            
            var yearField = new FormFieldView({
                el: this.$("li[data-name='year']").get(0),
                fieldModel: yearFieldModel,
                inputView: years
            });
            yearField.render();
            
            this.form.get("classifierGroups").each(function(classifierGroup) {
                var view = new ClassifierGroupView({
                    classifierGroup: classifierGroup
                });
                this.$("#classifier-groups").append(view.render().el);
            }, this);
            
            var buttonsView = new FormButtonsView({
                form: this.form,
                el: this.$("ul.nav-horizontal.nav-right").get(0)
            });
            buttonsView.render();
            
            return this;
        },

        addClassificationGroup: function() {
            var fieldEditorDialogVM = this.form.createNewClassificationDialog();
            var dialog = new FieldEditorDialog({
                viewModel: fieldEditorDialogVM,
                triggeringElement: this.$("#add-classification-group-button"),
                position: {
                    my: "right bottom",
                    at: "right top",
                    of: $("#classifier-groups")
                }
            });
            dialog.on("save", this.onClassifierGroupCreated);
            dialog.on("cancel", this.onCancelClassifierGroupCreation);
            dialog.render();
        },
        
        onClassifierGroupCreated: function(group) {
            this.form.addPendingClassifierGroup();
        },
        
        onCancelClassifierGroupCreation: function() {
            this.form.cancelPendingClassifierGroup();
        }
    });
    
    return {
        FacultyEditView: FacultyEditView
    };
});