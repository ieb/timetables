define(["jquery", 
        "domReady",
        "jquery-ui/autocomplete"], 
        function () {

    // The crsidAutoComplete provides a means to
    // dynamically suggest CRSids as the user inputs
    // ids into a field. The CRSids appear as a drop
    // down below the input field. To enable the
    // autocomplete functionality, the input element
    // is passed an argument to the crsidAutocomplete
    // function. You can enable multiple input fields
    // on a page by doing something like:
    //
    // var autocomplete_fields = new_series.find("input[name^=fields_to_enable");
    // autocomplete_fields.each(function(index,element) {
    //     autocomplete.crsidAutocomplete(element);
    // });
    // 
    // The function implementation is essentially taken
    // from:
    // http://jqueryui.com/demos/autocomplete/#multiple-remote
    // and so supports multiple comma separated
    // completions.

    function crsidAutocomplete(element) {

        "use strict";

        function split( val ) {
            return val.split( /,\s*/ );
        }
        function extractLast( term ) {
            return split( term ).pop();
        }

        $( element )
            // don't navigate away from the field on tab when selecting an item
            .bind( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).data( "autocomplete" ).menu.active ) {
                        event.preventDefault();
                    }
                })
            .autocomplete({
                // The minimum length entry to start autocompletion on:
                minLength: 1,
                source: function( request, response ) {
                    $.getJSON( "/utils/autocomplete/crsid/" + extractLast( request.term ) + "/", {
                    }, response );
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
	        },
	        select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( ", " );
                    return false;
	        }
	    });
     }

     return { crsidAutocomplete: crsidAutocomplete };
});
