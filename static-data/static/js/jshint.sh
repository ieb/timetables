#!/bin/sh
cd "$( dirname "${BASH_SOURCE[0]}" )"
find . -path './libs' -prune -o -name "*.js" -exec jshint {} + | head
