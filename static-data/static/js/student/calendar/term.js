define([
        "jquery", 
        "underscore", 
        "student/calendar/all", 
        "assert", 
        "bootstrap_affix",
        "domReady",
        "jquery-ui/effects/drop"],
        function($, _, all, assert) {
    "use strict";

    all.bindCommonCalendarInteractions();

    function SeriesDaysHighlighter() {
        assert(this !== window, "This is a constructor, call with new.");

        this.$highlightedCells = $();

        // Bind methods which will be invoked as event handlers
        _.bindAll(this, "onSeriesEnterExit");
    }
    // Add methods to SeriesDaysHighlighter.
    _.extend(SeriesDaysHighlighter.prototype, {
        HIGHLIGHTED_CLASS: "calendar-month__day_highlighted",

        onSeriesEnterExit: function(event) {
            if(event.type === "mouseenter") {
                this.addHighlightsFor(event.currentTarget);
            } else {
                this.removeHighlights();
            }
        },

        addHighlightsFor: function (series) {
            // Ensure no cells are highlighted before starting.
            this.removeHighlights();
            
            assert(this.$highlightedCells.length === 0);

            // Get a list of the dates used by events the series
            var dates = this.getSeriesEventDates(series);
            // Find all calendar cells for the dates
            this.$highlightedCells = this.selectCalendarCells(dates);
            // Highlight the cells
            this.addHighlights();
        },

        /** Gets a list of the dates that the series has events on. */
        getSeriesEventDates: function(series) {
            var seriesId = parseInt($(series).attr("data-series-id"));
            
            // Should never get NaN...
            assert(seriesId === seriesId, "data-series-id attr was not an int");
            
            var $events = $("#year-events > .event[data-owning-series-id="
                    + seriesId + "]");

            return _.chain($events)
                .map(function(e) { return $(e).attr("data-date"); })
                .sortBy(_.identity)
                .uniq()
                .value();
        },

        selectCalendarCells: function(dates) {
            var dayCellSelector = _.map(dates, function(d) {
                return ".calendar-month__day[data-date=\"" + d + "\"]";
            }).join(",");

            return $(dayCellSelector);
        },

        addHighlights: function() {
            this.$highlightedCells.addClass(this.HIGHLIGHTED_CLASS);
        },

        removeHighlights: function() {
            if(this.$highlightedCells) {
                this.$highlightedCells.removeClass(this.HIGHLIGHTED_CLASS);
                // Forget about the previously higlighted cells once they're
                // no longer highlighted.
                this.$highlightedCells = $();
            }
        }
    });

    $(".term-timetable__series-list").on("mouseenter mouseleave", 
            ".event-series", new SeriesDaysHighlighter().onSeriesEnterExit);

    /** 
     * Uses the bootstrap-affix to keep the calendars in the window when 
     * scrolling.
     */
    function scrollCalendarWithPage(calendarContainer) {
        var $calendarContainer = $(calendarContainer);
        var $calendarList = $calendarContainer
                .find(".term-timetable__calendar-list");

        // The calendar is positioned 30px below the top of the window
        // when fixed, so we need to fix it 30 px before it hits the top of
        // the screen.
        var top = Math.floor($calendarContainer.offset().top - 30);

        // The bottom offset is the distance from the bottom of hte page that
        // the calendar's bottom will be when it stops being fixed to the 
        // window. For us, this is the distance from the bottom of the element
        // containing the left and right columns. That element defines the range
        // the calendar can slide inside.
        var bottom = Math.ceil($(document).height() - (
            $calendarContainer.offset().top + $calendarContainer.height()));

        $calendarList.affix({
            offset: {
                top: top,
                bottom: bottom
            }
        });
    }

    // Affix any calendar with events.
    _.chain($(".term-timetable__columns"))
        .filter(function(column){ 
            return $(column).find(".term-timetable__no-events").length === 0;
        })
        .each(scrollCalendarWithPage);


    // Show event info dialog when hovering over a calendar day cell. Note that
    // the dialog only pops up
    $(".term-timetable-list").on("mouseover mouseleave",
        ".calendar-month__day_status_occupied", function(event) {

        // mouseover rather than mouseenter is used as we need to know when
        // the info dialog is entered, and treat that as leaving the target.
        var $this = $(event.currentTarget);
        var $dayContainer = $this.children(".calendar-month__day-container");
        var $elUnderMouse = $(event.target);
        var $infoDialog = $this.find(".info-dialog");

        // The options to use when animating in/out the popup
        var dropOptions = {duration: 70};

        // Popup the info dialog this # ms after entering the target el if the
        // mouse has not left. This prevents dialogs popping up all over the
        // place when running the mouse across the calendar.
        var popupDelay = 250;

        var mouseEnteredTargetEl = event.type == "mouseover" &&
                /* Treat entering the dialog as leaving */
                !($infoDialog.is($elUnderMouse) ||
                    $infoDialog.has($elUnderMouse).length > 0);

        if(mouseEnteredTargetEl) {

            var popupInProgress = ($this.data("delayedOpen") ||
                $this.data("infoDialog"));

            if(!popupInProgress) {
                var delay = _.delayIfNotCancelled(function() {
                    var $calendarList = $this.closest(".term-timetable__calendar-list");

                    var $infoDialog = $(".info-dialog").first().clone();
                    $infoDialog.appendTo(
                        $dayContainer);
                    $this.data("infoDialog", $infoDialog);
                    $infoDialog.show("drop", dropOptions);

                    $this.on("mousemove", onCalendarDayMousemove);
                    // Update the info dialog to show the event the mouse is
                    // currently over.
                    updateInfoDialogContent(mouseX($this, event), 
                        $this.outerWidth(), $this, true);

                }, popupDelay);
                // Save the delay against this element so that it can be
                // cancelled in the else below if the mouse leaves before the
                // delay elapses.
                $this.data("delayedOpen", delay);
            }

        } else {
            // Abort the opening animation if one is scheduled
            var delay = $this.data("delayedOpen");
            if(delay) {
                delay.cancel();
                $this.removeData("delayedOpen");
            }

            // Remove any existing popup
            var $infoDialog = $this.data("infoDialog");
            if($infoDialog) {
                $this.removeData("infoDialog");
                $(event.currentTarget).find(".info-dialog").hide("drop", dropOptions,
                        function() {
                    $infoDialog.remove();
                });
            }
            $this.removeData("eventsToday");
            $this.removeData("eventIndex");
            $this.off("mousemove", onCalendarDayMousemove);
        }
    });

    function mouseX($elem, event) {
        var x = event.pageX - $elem.offset().left;
        return Math.max(0, Math.min($elem.outerWidth(), x));
    }

    function onCalendarDayMousemove(event) {
        var width = $(this).outerWidth();
        var x = mouseX($(this), event);
        updateInfoDialogContent(x, width, $(this));
    }

    function updateInfoDialogContent(x, width, $day, immediate) {
        immediate = immediate || false;

        var $dialog = $day.data("infoDialog");
        if(!$dialog)
            return;

        var events = $day.data("eventsToday");
        if(!events) {
            var date = $day.data("date");
            events = $("#year-events .event[data-date=" + date + "]");
            $day.data("eventsToday", events);
        }
        
        var offset = x / width;
        var index = Math.max(0, Math.min(events.length -1, 
            Math.floor(events.length * offset)));

        if(index != $day.data("eventIndex")) {
            setInfoDialogContent($dialog, $(events[index]), immediate);
            $day.data("eventIndex", index);
        }
    }

    function setInfoDialogContent($infoDialog, $event, immediate) {
        var $oldHead = $infoDialog.find(".info-dialog__header-content");
        var $newHead = $event.find(".info-dialog__header-content").clone();
        switchElements($oldHead, $newHead, immediate);

        var $oldBody = $infoDialog.find(".info-dialog__body");
        var $newBody = $event.find(".info-dialog__body").clone();
        if(!($oldBody.length && $newBody.length))
            debugger;
        switchElements($oldBody, $newBody, immediate);
    }

    function switchElements($old, $new, immediate) {
        if(!immediate) {
            $old.fadeOut("fast", function(){
                $old.replaceWith($new);
                $new.fadeIn("fast");
            });
        }
        else
            $old.replaceWith($new);
    }
});