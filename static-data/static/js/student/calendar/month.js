define([
	"jquery", 
	"student/calendar/all",
	"student/calendar/calendar",
	"fullcalendar",
	"domReady"], 
	function($, all, calendar){

	all.bindCommonCalendarInteractions();

    var monthCalendar = new calendar.Calendar(
        // Parent element
        $(".calendar-content .fullcalendar"));

    // Bind interactions with various page elements which effect the calendar.
    calendar.bindEvents(monthCalendar);
    
    // Insert the calendar into the page (into the parent element)
    monthCalendar.initialise();
});