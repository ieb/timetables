define(["underscore"], function(_){
    function items(obj) {
        return _.map(obj, function(value, key) {
            return [key, value];
        });
    }
    
    function toObject(keyValuePairs) {
        function reducer(obj, kv){
            var key = kv[0];
            var value = kv[1];
            obj[key] = value;
            return obj; 
        }
        return _.reduce(keyValuePairs, reducer, {});
    }
    
    function filterObject(obj, predicate) {
        return toObject(
            _.filter(items(obj), function(item) {
                return predicate(item[0], item[1]);
            })
        );
    }

    var CancelableDelay = function CancelableDelay(func, wait) {
        _.bindAll(this, "execute", "cancel");
        this.isCancelled = false;
        this.func = func;
        this.funcArgs = _.toArray(arguments).slice(2);
        setTimeout(this.execute, wait);
    }
    CancelableDelay.prototype.execute = function() {
        if(!this.isCancelled)
            this.func.apply(undefined, this.funcArgs);
    }
    CancelableDelay.prototype.cancel = function() {
        this.isCancelled = true;
    }

    /**
     * Same as _.delay(...), except the execution of the function can be
     * cancelled by calling the cancel() method of the object returned by
     * this function.
     *
     * Example:
     *   var delayed = _.delayIfNotCancelled(myfunc, 500);
     *   // < 500ms later
     *   delayed.cancel();
     *   // myfunc is no longer called once 500ms have elapsed. If no call to
     *   // delayed.cancel() is made, myfunc will be executed as normal after
     *   // 500ms.
     */
    function delayIfNotCancelled(/* func, wait, [*arguments] */) {
        // We're basically doing new CancelableDelay(...) here, except that we
        // have to provide dynamic arguments with CancelableDelay.apply.
        var cancelableDelay = Object.create(CancelableDelay.prototype);
        CancelableDelay.apply(cancelableDelay, arguments);
        return cancelableDelay;
    }

    return {
        items: items,
        toObject: toObject,
        filterObject: filterObject,
        delayIfNotCancelled: delayIfNotCancelled
    };
});